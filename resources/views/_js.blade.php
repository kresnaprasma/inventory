<script type="text/javascript">
	var begin = '{{ $begin->format('Y-m-d') }}';
  	var end = '{{ $end->format('Y-m-d') }}';

  	getChart()
  	getChartVisitor()
  	getChartAverageVisitor()
  	getPieMenuChart()
  	getPieCashChart()
  	getPieSalesCategoryChart()

  	$(".toggle-expand-btn").click(function (e) {
  		$(this).closest('.box.box-warning').toggleClass('panel-fullscreen');
	});

  	$('select[name=outlet_sort]').change(function(){
  		updateQueryStringParam('outlet',this.value);
  	});

  	$('#reportrange').daterangepicker({
		buttonClasses: ['btn', 'btn-sm'],
		applyClass: 'btn-red',
		cancelClass: 'btn-default',
		startDate: '{{ $begin->format('m/d/y') }}',
		endDate: '{{ $end->format('m/d/y') }}',
		locale: {
		applyLabel: 'Submit',
		cancelLabel: 'Cancel',
		fromLabel: 'From',
		toLabel: 'To',
		},
		ranges: {
		'Today': [moment(), moment()],
		'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		'This Month': [moment().startOf('month'), moment().endOf('month')],
		'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
		}, function(start, end, label){
		// console.log(start.toISOString(), end.toISOString(), label);

		$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		// window.location="{{  request()->url() }}?begin="+ start.format('Y-MM-DD') +"&end=" + end.format('Y-MM-DD');
		updateQueryStringParam('begin', start.format('Y-MM-DD'));
		updateQueryStringParam('end', end.format('Y-MM-DD'));
	});

	/* jQueryKnob */

	$(".knob").knob({
    	'min':0,
    	
    	'readOnly': true,
    	'thickness': 0.3,
    	'angleArc': 250,
    	'angleOffset': -125,
    	'width': 250,
    	'height': 250,
    	'fgColor': '#505255',
    	'bgColor': '#e74c3c',
    	'format': function(v){
    		return v.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
    	}
	});

	$('.knob-precentage').knob({
		'min': 0,
		'max': 100,
		'readOnly' : true,
		'format': function(v){
    		return v+'%';
    	}
	})
    /* END JQUERY KNOB */

  	function getChart(){
	  	var labelDate = [];
	  	var data = [];

	  	var dataChart = $.ajax({
	  		url: '/api/web/bill/chart/date?begin='+begin+'&end='+end+'&outlet='+outlet,
	  		type: 'GET',
	  		success: function(response){
	  			$.each(response, function(key, value){
	  				var date_value = moment(value.DATE).format('DD/MM')
	  				var data_value = value.total.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');


	  				labelDate.push(date_value)
	  				data.push(value.total)
	  			})

	  			var barChartData = {
					labels: labelDate,
					datasets: [{
							label: 'Bill Chart',
							fill: false,
							backgroundColor: '#c0392b',
							borderColor: '#c0392b',
							borderWidth: 1,
							data: data
	  						}]
	  			}
	  			var ctx = document.getElementById('barChart').getContext('2d');
	  			myBar = new Chart(ctx, 
	  					{
							type: 'bar',
							data: barChartData,
							options: {	
								responsive: true,	
								legend: {
									position: 'top',
								},
								title: {
									display: true,
									text: 'Sales Chart'
								},
								tooltips: {
										mode: 'label',
										label: 'mylabel',
										callbacks: {
											label: function(tooltipItem, data) {
												return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
											}, 
										},
								},
							}
						});
	  		}
	  	})
    }

    function getChartVisitor(){
	  	var labelDate = [];
	  	var data = [];

	  	var dataChart = $.ajax({
	  		url: '/api/web/bill/chart/visitor/date?begin='+begin+'&end='+end+'&outlet='+outlet,
	  		type: 'GET',
	  		success: function(response){
	  			$.each(response, function(key, value){
	  				var date_value = moment(value.DATE).format('DD/MM')
	  				var data_value = value.total_visitor.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');


	  				labelDate.push(date_value)
	  				data.push(value.total_visitor)
	  			})

	  			var barChartData = {
					labels: labelDate,
					datasets: [{
							label: 'Visitor Count',
							fill: false,
							backgroundColor: '#c0392b',
							borderColor: '#c0392b',
							borderWidth: 1,
							data: data
	  						}]
	  			}
	  			var ctx = document.getElementById('barChartVisitor').getContext('2d');
	  			myBar = new Chart(ctx, 
	  					{
							type: 'line',
							data: barChartData,
							options: {	
								responsive: true,	
								legend: {
									position: 'top',
								},
								title: {
									display: true,
									text: 'Visitor Chart'
								},
								tooltips: {
										mode: 'label',
										label: 'mylabel',
										callbacks: {
											label: function(tooltipItem, data) {
												return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
											}, 
										},
								},
							}
						});
	  		}
	  	})
    }
    function getChartAverageVisitor(){
	  	var labelDate = [];
	  	var data = [];

	  	var dataChart = $.ajax({
	  		url: '/api/web/bill/chart/visitor/date?begin='+begin+'&end='+end+'&outlet='+outlet,
	  		type: 'GET',
	  		success: function(response){
	  			$.each(response, function(key, value){
	  				var date_value = moment(value.DATE).format('DD/MM')
	  				var data_value = value.total_visitor.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
	  				var total_average = Math.round(value.total / value.total_visitor);
	  				if(total_average ==''){
	  					total_average = 0;
	  				}

	  				labelDate.push(date_value)
	  				data.push(total_average)
	  			})

	  			var barChartData = {
					labels: labelDate,
					datasets: [{
							label: 'Avg Sales',
							fill: false,
							backgroundColor: '#c0392b',
							borderColor: '#c0392b',
							borderWidth: 1,
							data: data
	  						}]
	  			}
	  			var ctx = document.getElementById('barChartAverageVisitor').getContext('2d');
	  			myBar = new Chart(ctx, 
	  					{
							type: 'line',
							data: barChartData,
							options: {	
								responsive: true,	
								legend: {
									position: 'top',
								},
								title: {
									display: true,
									text: 'Average Sales Chart'
								},
								tooltips: {
										mode: 'label',
										label: 'mylabel',
										callbacks: {
											label: function(tooltipItem, data) {
												return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
											}, 
										},
								},
							}
						});
	  		}
	  	})
    }
    function getPieMenuChart() {
    	var label = [];
	  	var data = [];

	  	var dataChart = $.ajax({
	  		url: '/api/web/bill/chart/menu?begin='+begin+'&end='+end+'&outlet='+outlet,
	  		type: 'GET',
	  		success: function(response){
	  			// console.log(response);
	  			$.each(response, function(key, value){
	  				var label_menu = value.menu.name;
	  				var data_menu = value.total_menu;

	  				label.push(label_menu)
	  				data.push(data_menu)
	  			})

	  			var config = {
					type: 'pie',
					data: {
						datasets: [{
							data: data,
							backgroundColor: [
								'#e67e22','#27ae60','#2980b9','#e74c3c','#9b59b6'
							],
							label: 'Menu Chart'
						}],
						labels: label
					},
					options: {
						responsive: true,
						legend: {
				            display: true,
				            position: 'bottom',
				        }
					}
				};
	  			var ctx = document.getElementById('pieMenuChart').getContext('2d');
	  			myBar = new Chart(ctx, config);
	  		},
	  		error: function(response){
	  			alert('no data');
	  		}
	  	})
    }

    function getPieCashChart() {
    	var label = [];
	  	var data = [];
	  	
	  	var dataChart = $.ajax({
	  		url: '/api/web/bill/chart/cash?begin='+begin+'&end='+end+'&outlet='+outlet,
	  		type: 'GET',
	  		success: function(response){
	  			$.each(response, function(key, value){
	  				var label_menu = value.name;
	  				var sum = 0;
	  				$.each(value.cash, function(key, value){
	  					sum = sum + value.cash_amount;
	  				});
	  				

	  				var data_menu = sum
	  				
	  				label.push(label_menu)
	  				data.push(data_menu)
	  			})
	  			// data purchasing
	  			label.push('Purchasing')
	  			data.push({{ $total_purchasing }})
	  			
	  			var config = {
					type: 'pie',
					data: {
						datasets: [{
							data: data,
							backgroundColor: [
								'#e67e22','#27ae60','#2980b9','#e74c3c','#9b59b6'
							],
							label: 'Menu Chart'
						}],
						labels: label
					},
					options: {
						responsive: true,
						legend: {
				            display: true,
				            position: 'bottom',
				        }
					}
				};
	  			var ctx = document.getElementById('pieCashChart').getContext('2d');
	  			myBar = new Chart(ctx, config);
	  		},
	  		error: function(response){
	  			alert('no data');
	  		}
	  	})
    }

    function getPieSalesCategoryChart() {
    	var label = [];
	  	var data = [];
	  	var theHelp = Chart.helpers;
	  	var dataChart = $.ajax({
	  		url: '/api/web/bill/chart/bill-category?begin='+begin+'&end='+end+'&outlet='+outlet,
	  		type: 'GET',
	  		success: function(response){
	  			var total_sales = {{ $total_sales }};

	  			$.each(response, function(key, value){
	  				var label_menu = value.name;
	  				var sum = 0;
	  				$.each(value.bill, function(key, value){
	  					sum = sum + value.total;
	  				});
	  				

	  				var data_menu = Math.round((sum / total_sales) * 100);
	  				
	  				label.push(label_menu)
	  				data.push(data_menu)
	  			})
	  			
	  			var config = {
					type: 'pie',
					data: {
						datasets: [{
							data: data,
							backgroundColor: [
								'#e67e22','#27ae60','#2980b9','#e74c3c','#9b59b6'
							],
							label: 'Menu Chart'
						}],
						labels: label
					},
					options: {
						responsive: true,
						title: {
							display: true,
							text: 'Orders Chart',
							position: 'bottom'
						},
						legend: {
									display: true,

									// generateLabels changes from chart to chart,  check the source, 
									// this one is from the doughut :
									// https://github.com/chartjs/Chart.js/blob/master/src/controllers/controller.doughnut.js#L42
									labels: {
										generateLabels: function(chart) {
											var data = chart.data;
											if (data.labels.length && data.datasets.length) {
												return data.labels.map(function(label, i) {
													var meta = chart.getDatasetMeta(0);
													var ds = data.datasets[0];
													var arc = meta.data[i];
													var custom = arc && arc.custom || {};
													var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
													var arcOpts = chart.options.elements.arc;
													var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
													var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
													var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
													return {
														// And finally : 
														text: label+ ": " +ds.data[i]+'%',
														fillStyle: fill,
														strokeStyle: stroke,
														lineWidth: bw,
														hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
														index: i
													};
												});
											}
											return [];
										}
									}
						},

						tooltips: {
							callbacks: {
							    label: function(tooltipItem, data) {
							      //get the concerned dataset
							      var dataset = data.datasets[tooltipItem.datasetIndex];
							      //calculate the total of this data set
							      var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
							        return previousValue + currentValue;
							      });
							      //get the current items value
							      var currentValue = dataset.data[tooltipItem.index];
							      //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
							      var precentage = Math.floor(((currentValue/total) * 100)+0.5);

							      return precentage + "%";
							    }
						  	}
						} 
					}
				};
	  			var ctx = document.getElementById('pieSalesCategoryChart').getContext('2d');
	  			myBar = new Chart(ctx, config);

				
	  		},
	  		error: function(response){
	  			alert('no data');
	  		}
	  	})
    }

    function drawSegmentValues()
	{
	    for(var i=0; i<myPieChart.segments.length; i++) 
	    {
	        ctx.fillStyle="white";
	        var textSize = canvas.width/10;
	        ctx.font= textSize+"px Verdana";
	        // Get needed variables
	        var value = myPieChart.segments[i].value;
	        var startAngle = myPieChart.segments[i].startAngle;
	        var endAngle = myPieChart.segments[i].endAngle;
	        var middleAngle = startAngle + ((endAngle - startAngle)/2);

	        // Compute text location
	        var posX = (radius/2) * Math.cos(middleAngle) + midX;
	        var posY = (radius/2) * Math.sin(middleAngle) + midY;

	        // Text offside by middle
	        var w_offset = ctx.measureText(value).width/2;
	        var h_offset = textSize/4;

	        ctx.fillText(value, posX - w_offset, posY + h_offset);
	    }
	}
</script>