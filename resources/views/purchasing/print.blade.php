<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TB. Aneka Bangunan | Print</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/admin-lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/admin-lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/admin-lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/admin-lte/dist/css/AdminLTE.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
    table.table{
      font-size: 8px;
    }
    table.table-bordered{
      border:1px solid black;
      margin-top:20px;
    } 
    table.table-bordered > thead > tr > th{
      border:1px solid black;
    }
    table.table-bordered > tbody > tr > td{
      border:1px solid black;
    }
    .pagebreak { page-break-before: always; }
    @media print{
      .table thead th,
      .table tbody tr td{
        border-width: 1px !important;
        border-style: solid !important;
        border-color: black !important;
        font-size: 8px !important;
        background-color: red;
        padding:4px;
        -webkit-print-color-adjust:exact ;
      }
    }
  </style>
</head>
<body>
  <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>  Aneka Bangunan
            {{-- <small class="pull-right">Date: {{ date('d/M/Y', strtotime($purchasing->purchasing_date)) }}</small> --}}
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <b>Invoice {{ $purchasing->purchasing_no }}</b><br>
          <br>
          <b>Payment Date:</b> {{ date('d/M/Y', strtotime($purchasing->purchasing_date)) }}<br>
          <b>Due Date:</b>
          @if (!empty($purchasing->purchasing_due_date))
            {{ date('d/M/Y', strtotime($purchasing->purchasing_due_date)) }}<br>
          @else
            -
          @endif
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          @if ($purchasing->supplier_no)
            From:

            <address>
              <strong>{{ $purchasing->supplier_no }} | {{ $purchasing->supplier->name }}</strong><br>
              {{ $purchasing->supplier->address }}<br>
              Phone: {{ $purchasing->supplier->phone }}<br>
              Email: {{ $purchasing->supplier->email }}
            </address>
          @endif
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          @if ($purchasing->payment->sum('payment_amount') >= $purchasing->purchasing_total)
            <h2>Paid</h2>
          @else
            <h2>Not Paid</h2>
          @endif
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Inventory No.</th>
              <th>Qty</th>
              <th>Unit</th>
              <th style="text-align: right;">Price</th>
              <th style="text-align: right;">Total</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($purchasing->details as $pd)
                <tr>
                  <td>{{ $pd->inventory_code }} - {{ App\Inventory::where('inventory_code', $pd->inventory_code)->first()->inventory_name }}</td>
                  <td>
                    @if (empty($pd->retur_qty))
                      {{ number_format($pd->detail_qty) }}
                    @else 
                      @if ($pd->detail_unit != $pd->retur_unit)
                        @php $to_convert_size = 0;@endphp
                        @foreach ($pd->inventory->converts as $conv)
                          @if ($conv->convert_type == '<')
                            @php 
                              $to_convert_size = $pd->detail_qty * $conv->size; 
                              $to_retur_size = $to_convert_size - $pd->retur_qty;
                              $to_origin_size = $to_retur_size / $conv->size;
                            @endphp
                            {{ $to_origin_size }}
                          @else

                          @endif
                        @endforeach
                      @else 
                        {{ number_format($pd->detail_qty - $pd->retur_qty) }}
                      @endif
                    @endif
                  </td>
                  <td>{{ $pd->detail_unit }}</td>
                  <td style="text-align: right">{{ number_format($pd->detail_amount) }}</td>
                  <td style="text-align: right;">{{ number_format($pd->detail_total) }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Notes:</p>
          @if ($purchasing->purchasing_note)          
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              {{$purchasing->purchasing_note}}
            </p>
          @endif
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table" style="text-align: right;">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>{{ number_format($purchasing->purchasing_sub_total) }}</td>
              </tr>
              <tr>
                <th>Discount</th>
                <td>({{ number_format($purchasing->purchasing_discount) }})</td>
              </tr>
              <tr>
                <th>PPN/VAT (10%)</th>
                <td>{{ number_format($purchasing->purchasing_tax) }}</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>{{ number_format($purchasing->purchasing_total) }}</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{ route('purchasing.print', $purchasing->id) }}" target="_blank" class="btnprn btn btn-default"><i class="fa fa-print"></i> Print</a>

          <a href="{{ route('purchasing.payment.create',$purchasing->id) }}" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Payment
          </a>
          @if ($purchasing->payment->sum('payment_amount') >= $purchasing->purchasing_total)
            
          @else
            
              <a href="{{ route('purchasing.retur.create',$purchasing->id) }}" class="btn btn-primary pull-right" style="margin-right: 5px;"">
                <i class="fa fa-mail-reply-all "></i> Retur
              </a>
              <a href="{{ route('purchasing.edit',$purchasing->id) }}" class="btn btn-primary pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Revise
              </a>
          @endif
        </div>
      </div>

      @if ($purchasing->details->sum('retur_qty')>0)
      <hr>
      <h5><b>Retur:</b></h5>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Inventory No.</th>
              <th>Qty</th>
              <th>Unit</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($purchasing->details as $pd)
                <tr>
                  <td>{{ $pd->inventory_code }} - {{ App\Inventory::where('inventory_code', $pd->inventory_code)->first()->inventory_name }}</td>
                  <td>{{ $pd->retur_qty }}</td>
                  <td>{{ $pd->retur_unit }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @endif
    </section>
  <!-- /.content -->
</body>
</html>