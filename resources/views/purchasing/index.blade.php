@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Purchasing
        <small>Purchasing Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('purchasing.index') }}">Purhasing</a></li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray-dark">
            <div class="inner">
              <h3>{{ number_format($purchasing->where('status','completed')->sum('purchasing_total')) }}</h3>

              <p>Total Purchasing</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
        	<!-- small box -->
        	<div class="small-box bg-red">
        		<div class="inner">
        			<h3>{{ number_format($purchasing->where('status','outstanding')->sum('purchasing_total')) }}</h3>
        			<p>Total Outstanding</p>
        		</div>
        		<div class="icon">
              		<i class="ion ion-stats-bars"></i>
            	</div>
        	</div>
        </div>
    </div>

	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Purchasing List
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
		         	<a href="{{ route('purchasing.create') }}" class="btn btn-primary">
		            	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
		          	</a>
		          	{{-- <button type="button" class="btn btn-danger" onclick="DeletePurchasing()">
		            	<i class="fa fa-trash" aria-hidden="true"></i> Delete
		          	</button> --}}
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Sort By:
		          	</span>
		          	{!! Form::select('status',[''=>'--Status--','completed'=>'Completed','outstanding'=>'Outstanding'], null,['class'=>'btn btn-primary']) !!}
		          	<button type="button" class="btn btn-primary" id="reportrange">
			            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			            <span>
			            	@if ($begin)
			                	{{ $begin->format('M d, Y') }}
			                	-
			                	{{  $end->format('M d, Y') }}
			              	@endif
			            </span>
			            <b class="caret"></b>
			         </button>
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['id'=>'formStock']) !!}
				<table id="tableStock" class="table table-striped table-color">
					<thead>
						<tr>
							<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>No.</th>
							<th>Date</th>
							<th>Due Date</th>
							<th>Discount</th>
							<th>PPN</th>
							<th>Total</th>
							<th>Status</th>
	            			<th>Action</th>
						</tr>
					</thead>
					<tbody>
	          			@foreach ($purchasing as $p)
						<tr>
							<td>
								<input type="checkbox" id="idTablePurchasing" value="{{ $p->id }}" name="id[]" class="checkin">
							</td>
							<td>{{ $p->purchasing_no }}</td>
							<td>{{ date('d F y', strtotime($p->purchasing_date)) }}</td>
							<td>
								@if (!empty($p->purchasing_due_date))
									@if (date('Y-m-d H:i:s') >= $p->purchasing_due_date && $p->status != 'completed')
										<span class="label label-danger" style="font-size: 12px">
											<i class="fa fa-warning"></i> 
											{{ date('d F y', strtotime($p->purchasing_due_date)) }}
										</span>
									@else 
										{{ date('d F y', strtotime($p->purchasing_due_date)) }}
									@endif
								@endif
							</td>
							<td>{{ number_format($p->purchasing_discount_percentage) }}%</td>
							<td>{{ number_format($p->purchasing_tax) }}</td>
							<td>{{ number_format($p->purchasing_total) }}</td>
							<td>
								@if ($p->status == 'completed')
									<span class="label label-success" style="font-size: 12px">
										<i class="fa fa-check-circle-o"></i> {{ ucfirst($p->status) }}
									</span>
								@else
									<span class="label label-warning" style="font-size: 12px">
										<i class="fa fa-warning"></i> {{ ucfirst($p->status) }}
									</span>
								@endif
							</td>
							<td>
								<a href="{{ route('purchasing.show', $p->id) }}" class="btn btn-primary btn-xs">
									<i class="fa fa-pencil-square"></i> Show
								</a>
							</td>
						</tr>
						@endforeach
	        		</tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
  <script type="text/javascript">
    var tableStock = $("#tableStock").DataTable({
    	"dom": "rtip",
      	"pageLength": 10,
      	"retrieve": true,
      	"stateSave": true,
    });

    $("#searchDtbox").keyup(function() {
          tableStock.search($(this).val()).draw() ;
    });
      
    $("select[name=outlet_code]").on('change',function(){
    	updateQueryStringParam('outlet', this.value);
    });
    $("select[name=status]").on('change',function(){
    	updateQueryStringParam('status', this.value);
    });

    $('#reportrange').daterangepicker({
      buttonClasses: ['btn', 'btn-sm'],
      applyClass: 'btn-red',
      cancelClass: 'btn-default',
      startDate: '{{ $begin->format('m/d/y') }}',
      endDate: '{{ $end->format('m/d/y') }}',
      locale: {
        applyLabel: 'Submit',
        cancelLabel: 'Cancel',
        fromLabel: 'From',
        toLabel: 'To',
      },
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, function(start, end, label){
      console.log(start.toISOString(), end.toISOString(), label);

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      updateQueryStringParam('begin', start.format('Y-MM-DD'));
      updateQueryStringParam('end', end.format('Y-MM-DD'));

    });
    function DeleteUser() {
      $('#formStock').submit();
    }
  </script>
@stop