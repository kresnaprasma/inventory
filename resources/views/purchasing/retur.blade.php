@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Purchasing Manajemen</small>
    </h1>
    <ol class="breadcrumb">
      <li>Purchasing</li>
      <li><a href="{{ route('purchasing.index') }}">index</a></li>
      <li class="active">Create</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($purchasing, ['route' => ['purchasing.update', $purchasing->id],'id'=>'formCreatePurchasing','method'=>'PATCH']) !!}
      @include('purchasing._form',['edit'=>true])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('purchasing._js')
@stop