@extends('layouts.admin.admin')

@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Purchasing Order</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#{{-- {{ route('purchasing.index') }} --}}">Purchasing</a></li>
      <li><a href="{{ route('purchasing.order.index') }}">Order</a></li>
      <li class="active">Create</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($purchasing = new \App\PurchasingOrder, ['route' => 'purchasing.order.store','id'=>'formCreatePurchasing']) !!}
      @include('purchasing.order._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('purchasing.order._js')
@stop