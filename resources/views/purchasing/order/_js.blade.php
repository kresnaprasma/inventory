<script type="text/javascript">
	total_price();
	$('.inventory_code').change(function() {
		var inventory_code = $(this).val();
		var detail_unit = $(this).closest('tr').find('td .detail_unit').attr('id');

		getInventoryCode(inventory_code, detail_unit);
	});

	$('#purchasing_date').datepicker({
    	autoclose: true
  	});

  	$('#purchasing_due_date').datepicker({
    	autoclose: true
  	});

  	$(".del_rinc").click(function(){
		var rowCount = $('#tableDetailPurchasing tbody tr').length;
		var trFam = $(this).closest("tr");
		var id = trFam.find('.detail_id').attr('id');
		var id_val = $('#'+id).val();

	    if(rowCount > 1)
	    {
	    	$.ajax({
				url: '/api/web/purchasing/detail/'+id_val,
				type: 'DELETE',
				success: function(response){
					trFam.remove();
					total_price();
				},
				error: function(response){
					return false;
				}
			});
	    }
	    else{
	    	return false;
	    }
	});

	$(".del_rincian_create").click(function(){
		var rowCount = $('#tableDetailPurchasing tbody tr').length;
		var trFam = $(this).closest("tr");
		var id = trFam.find('.detail_id').attr('id');
		var id_val = $('#'+id).val();

	    if(rowCount > 1)
	    {
			trFam.remove();
			return false;
	    }
	    else{
	    	return false;
	    }
	});

	$("#btn-note").click(function() {
  		$("#purchasing_note").toggle("slow");
	});


	$('#purchasing_tax').change(function(){
		total_price();
	});

  	function addrow()
	{
    	var lastRow = $('#'+'tableDetailPurchasing' + " tbody tr:last");
    	var newRow = lastRow.clone(true);
	    //append row to this table
	    $('#tableDetailPurchasing').append(newRow);

	    // clear input
	    $("#tableDetailPurchasing tbody tr:last .detail-table").val('');
	   
	    var this_id = $("#tableDetailPurchasing tbody tr:last  .detail_qty").attr('id');
	    var this_id1 = $("#tableDetailPurchasing tbody tr:last .detail_unit").attr('id');
	    var this_id2 = $("#tableDetailPurchasing tbody tr:last  .detail_amount").attr('id');
	    var this_id3 = $("#tableDetailPurchasing tbody tr:last  .detail_discount").attr('id');
	    var this_id4 = $("#tableDetailPurchasing tbody tr:last  .detail_total").attr('id');
	    var this_id5 = $("#tableDetailPurchasing tbody tr:last  .detail_id").attr('id');
	    
	    $("#tableDetailPurchasing tbody tr:last td .detail_qty").attr('id', this_id + 1);
	    $("#tableDetailPurchasing tbody tr:last td .detail_unit").attr('id', this_id1 + 1);
	    $("#tableDetailPurchasing tbody tr:last td .detail_amount").attr('id', this_id2 + 1);
	    $("#tableDetailPurchasing tbody tr:last td .detail_discount").attr('id', this_id3 + 1);
	    $("#tableDetailPurchasing tbody tr:last td .detail_total").attr('id', this_id4 + 1);
	    $("#tableDetailPurchasing tbody tr:last td .detail_id").attr('id', this_id4 + 1);
	}

	function getInventoryCode(inventory_code, detail_unit) {
		$.ajax({
			url: '/api/web/inventory/'+ inventory_code,
			type: 'GET',
			success: function (response){
				$("#"+detail_unit).val(response.data.stock_unit);
			},
			error: function(response){
				console.log('inventory code', response);
			}
		})
	}

	
	function number(this_id){
		formatNumber(this_id);

		var thisTableId = $(this_id).parents("table tbody").attr("id");
		var id = $(this_id).closest('tr').find('.detail_qty').attr('id');
		var id2 = $(this_id).closest('tr').find('.detail_amount').attr('id');
		var id3 = $(this_id).closest('tr').find('.detail_total').attr('id');
		
		var id_val = $("#"+id).val();
		var id_num = parseFloat(id_val.replace(/[^0-9-.]/g, ''));

		var id_val2 = $("#"+id2).val();
		var id_num2 = parseFloat(id_val2.replace(/[^0-9-.]/g, ''));

		var cont = id_num * id_num2;

		var num = cont.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
		
		$("#"+id3).val(num);

		total_price();
		total_qty();
	}

	function numberDisc(this_id){
		total_price();
	}
	function total_price() {
		var sum = 0;
		$('#tableDetailPurchasing').find('.detail_total').each(function(){
		    var value = $(this).val();
		    var value_num = parseFloat(value.replace(/[^0-9-.]/g, ''));
		    if (!isNaN(value_num)) sum += value_num;
		});
		var subtotal = sum.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
		
		var discount = $('#purchasing_discount_percentage').val();
		var tax = $('#purchasing_tax').is(':checked');
		
		if(discount != ""){
			var sub_total_discount = sum - (sum * discount / 100);
		}else{
			var sub_total_discount = sum;
		}

		if(tax){
			var tax = sub_total_discount * 0.1;
			tax = tax.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
			var total = sub_total_discount + (sub_total_discount * 0.1);
			$('#purchasing_tax_amount').val(tax);
		}else{
			var total = sub_total_discount;
		}

		var num = total.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');

		$('#sub_total').val(subtotal);
		$('#purchasing_total').val(num);
    }

    function total_qty(){
    	var sum_qty = 0;
    	$('#tableDetailPurchasing').find('.detail_qty').each(function(){
		    var value = $(this).val();
		    var value_num = parseFloat(value.replace(/[^0-9-.]/g, ''));
		    if (!isNaN(value_num)) sum_qty += value_num;
		});

		var num = sum_qty.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
		
		if(num != 0){
			var total = $('#total_qty').val(num);
		}
    }

    function total_tax(){
    	var subtotal = parseFloat($('#sub_total').val().replace(/[^0-9-.]/g, ''));
    	var discount = $('#purchasing_discount_percentage').val();

    	var pretotal = (subtotal - (subtotal * discount /100)) * 0.1;
    	var total = subtotal - (subtotal * discount /100) + pretotal;

    	var num = total.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
    	var num2 = pretotal.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'')
    	
    	$('#purchasing_total').val(num);
    	$('#purchasing_tax_amount').val(num2);
    }

	function savePurchase(){
		$('#formCreatePurchasing').submit();
	}

	function deleteDetailPurchasing(id){
		$.ajax({
			url: '/api/web/purchasing/detail/'+id,
			type: 'DELETE',
			success: function(response){
				location.reload();
			}
		})
	}
</script>