@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Purchasing
        <small>Purchasing Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{-- {{ route('purchasing.index') }} --}}#">Purhasing</a></li>
        <li><a href="{{ route('purchasing.order.index') }}">Order</a></li>
      </ol>
    </section>
@stop

@section('content')
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Tables Stock
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
		         	<a href="{{ route('purchasing.order.create') }}" class="btn btn-primary">
		            	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
		          	</a>
		          	<button type="button" class="btn btn-danger" onclick="DeletePurchasing()">
		            	<i class="fa fa-trash" aria-hidden="true"></i> Delete
		          	</button>
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Filter
		          	</span>
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['id'=>'formOrder']) !!}
				<table id="tableOrder" class="table table-striped table-color">
					<thead>
						<tr>
							<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>No.</th>
							<th>Date</th>
							<th>Due Date</th>
							<th>Discount</th>
							<th>PPN</th>
							<th>Total</th>
							<th>Status</th>
	            			<th>Action</th>
						</tr>
					</thead>
					<tbody>
	          			@foreach ($purchasing as $p)
						<tr>
							<td>
								<input type="checkbox" id="idTablePurchasing" value="{{ $p->id }}" name="id[]" class="checkin">
							</td>
							<td>{{ $p->purchasing_no }}</td>
							<td>{{ date('d F y', strtotime($p->purchasing_date)) }}</td>
							<td>{{ date('d F y', strtotime($p->purchasing_due_date)) }}</td>
							<td>{{ number_format($p->purchasing_discount_percentage) }}%</td>
							<td>{{ number_format($p->purchasing_tax) }}</td>
							<td>{{ number_format($p->purchasing_total) }}</td>
							<td>{{ $p->status }}</td>
							<td>
								<a href="{{ route('purchasing.show', $p->id) }}" class="btn btn-default btn-xs">
									<i class="fa fa-pencil-square"></i> Edit
								</a>
							</td>
						</tr>
						@endforeach
	        		</tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
  <script type="text/javascript">
    var tableOrder = $("#tableOrder").DataTable({
    	"dom": "rtip",
      	"pageLength": 10,
      	"retrieve": true,
      	"stateSave": true,
    });

    $("#searchDtbox").keyup(function() {
          tableOrder.search($(this).val()).draw() ;
    });  

    function DeleteUser() {
      $('#formOrder').submit();
    }
  </script>
@stop