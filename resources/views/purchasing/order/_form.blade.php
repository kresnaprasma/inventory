<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Purchasing - <b>@if ($edit){{ $purchasing->purchasing_no }}@else {{ App\PurchasingOrder::OfMaxno() }}@endif</b>
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="form-group{{ $errors->has('purchasing_date') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('purchasing_date', "Date*", ['class'=>'control-label']) !!}
        <div class='input-group date' id='purchasing_date'>
          {!! Form::text('purchasing_date',old('purchasing_date'), ['class'=>'form-control']) !!}
          <span class="input-group-addon input-group-addon-silver">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </span>
        </div>
        @if ($errors->has('purchasing_date'))
            <span class="help-block">
                <strong>{{ $errors->first('purchasing_date') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('purchasing_due_date') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('purchasing_due_date', "Due Date*", ['class'=>'control-label']) !!}
        <div class='input-group date' id='purchasing_due_date'>
          {!! Form::text('purchasing_due_date',old('purchasing_due_date'), ['class'=>'form-control']) !!}
          <span class="input-group-addon input-group-addon-silver">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </span>
        </div>
        @if ($errors->has('purchasing_due_date'))
            <span class="help-block">
                <strong>{{ $errors->first('purchasing_due_date') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('purchasing_extno') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('purchasing_extno', "Ext No.", ['class'=>'control-label']) !!}
        {!! Form::text('purchasing_extno', old('purchasing_extno'), ['class'=>'form-control','required','autofocus']) !!}
        @if ($errors->has('purchasing_extno'))
          <span class="help-block">
            <strong>{{ $errors->first('purchasing_extno') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('supplier_code') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('supplier_code', "Supplier", ['class'=>'control-label']) !!}
        <select class="form-control" id="supplier_code" name="supplier_no">
          <option></option>
          @foreach ($supplier_list as $sp)
            @if ($purchasing->supplier_no == $sp->supplier_no)
              <option value="{{ $sp->supplier_no }}" selected="selected">{{ $sp->supplier_no }} - {{ $sp->name }}</option>
            @else
              <option value="{{ $sp->supplier_no }}">{{ $sp->supplier_no }} - {{ $sp->name }}</option>
            @endif
          @endforeach
        </select>
        @if ($errors->has('supplier_code'))
            <span class="help-block">
                <strong>{{ $errors->first('supplier_code') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <!-- /.row -->
    <hr>
    <div class="row" style="">
        <div class="col-md-12">
            @if ($purchasing->details->count() > 0)
              <table class="table table-striped table-color" id="tableDetailPurchasing">
                  <thead>
                    <th>Inventory No.</th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>&nbsp;</th>
                  </thead>
                  <tbody>
                    @php $i = 0; @endphp
                    @foreach ($purchasing->details as $pd)
                      @php $i = $i + 1; @endphp
                      <tr>
                        <td class="col-md-4">
                          <select name="inventory_code[]" class="form-control detail-purchasing inventory_code" id="inventory_code".$i>
                            <option value=""></option>
                            {{-- @foreach ($inventories as $i)
                              @if ($pd->inventory_code == $i->inventory_code)
                                <option value="{{ $i->inventory_code }}" selected="selected">{{ $i->inventory_code }} - {{ $i->inventory_name }}</option>
                              @else 
                                <option value="{{ $i->inventory_code }}">{{ $i->inventory_code }} - {{ $i->inventory_name }}</option>
                              @endif
                            @endforeach --}}
                          </select>
                        </td>
                        <td>
                          {!! Form::text('detail_qty[]',number_format($pd->detail_qty), ['class'=>'form-control detail_qty detail-table','id'=>'detail_qty'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                        <td>{!! Form::text('detail_unit[]', $pd->detail_unit, ['class'=>'form-control detail_unit detail-table','id'=>'detail_unit'.$i,'readonly']) !!}</td>
                        <td>{!! Form::text('detail_amount[]', number_format($pd->detail_amount), ['class'=>'form-control detail_amount detail-table','id'=>'detail_amount'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                        <td>{!! Form::text('detail_total[]', number_format($pd->detail_total), ['class'=>'form-control detail_total detail-table','id'=>'detail_total'.$i,'readonly']) !!}</td>
                        <td>
                            {!! Form::hidden('detail_id[]', $pd->id, ['id'=>'detail_id'.$i,'class'=>'detail_id detail-table']) !!}
                            <a href="javascript:void(0)" class="del_rinc btn btn-danger btn-xs">
                              <i class="fa fa-close" aria-hidden="true"></i>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td class="text-right"><b>Total Qty:</b></td>
                      <td>{{ Form::text('total_qty', null,['class'=>'form-control','id'=>'total_qty']) }}</td>
                      <td colspan="1" class="text-right"><b>Sub Total</b></td>
                      <td colspan="3">{!! Form::text('sub_total',null,['class'=>'form-control form-total','id'=>'sub_total','readonly']) !!}</td>
                    </tr>

                    <tr>
                      <td>
                        <button type="button" class="btn btn-primary" onclick="addrow()"><i class="fa fa-plus-circle"></i> Add Row</button>
                        <button type="button" class="btn btn-primary" id="btn-note"><i class="fa fa-plus-circle"></i> Notes</button>
                      </td>
                      <td colspan="2" class="text-right"><b>Discount %</b></td>
                      <td colspan="3">
                        {!! Form::number('purchasing_discount_percentage',null,['class'=>'form-control form-total','id'=>'purchasing_discount_percentage', 'max'=>'100','min'=>'1','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
                      </td>
                    </tr>
                    <tr>
                      <td>{{ Form::textarea('purchasing_note', null, ['class'=>'form-control', 'style'=>'display:none','id'=>'purchasing_note']) }}</td>
                      <td colspan="2" class="text-right">
                        <div class="checkbox">
                          <label>
                            @if ($purchasing->purchasing_tax >0)
                              <input type="checkbox" id="purchasing_tax" checked="checked"> 
                              <b>Tax </b>
                            @else
                              <input type="checkbox" id="purchasing_tax"> 
                              <b>Tax </b>
                            @endif
                          </label>
                        </div>
                      </td>
                      <td colspan="3">{!! Form::text('purchasing_tax',null,['class'=>'form-control form-total','id'=>'purchasing_tax_amount']) !!}</td>
                    </tr>
                    <tr>
                      <td colspan="3" class="text-right"><b>Total</b></td>
                      <td colspan="3">
                        {!! Form::text('purchasing_total',null,['class'=>'form-control form-total','id'=>'purchasing_total','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
                      </td>
                    </tr>
                  </tfoot>
              </table>
            @else
              @include('purchasing._tableCreateDetail')
            @endif
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">

        <button type="button" class="btn btn-flat btn-default">
          <i class="fa fa-pencil-square" aria-hidden="true"></i> Save as draft
        </button>
        
        <button type="button" class="btn btn-flat btn-danger pull-right">
          <i class="fa  fa-ban" aria-hidden="true"></i> Cancel
        </button>
        &nbsp;
        <button type="button" class="btn btn-flat btn-primary pull-right" onclick="savePurchase()">
          <i class="fa fa-check-square" aria-hidden="true"></i> Save
        </button>
      </div>
  </div>
</div>
<!-- /.box -->
