@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Purchasing Manajemen</small>
    </h1>
    <ol class="breadcrumb">
      <li>Purchasing</li>
      <li><a href="{{ route('purchasing.index') }}">index</a></li>
      <li><a href="#">Retur</a></li>
      <li class="active">Create</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($retur = new \App\PurchasingRetur, ['route' => 'purchasing.retur.store','id'=>'formCreatePurchasingRetur']) !!}
      @include('purchasing.retur._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('purchasing.retur._js')
@stop