<script type="text/javascript">
	
	$('.stock_code').each(function(){
		var inventory_code = $('#'+this.id+' :selected').val();
		var retur_unit = $('#'+this.id).closest('tr').find('td .retur_unit').attr('id');

		getStockCode(inventory_code, retur_unit);
	});
	
	function getStockCode(inventory_code, detail_unit) {
		$.ajax({
			url: '/api/web/inventory/'+ inventory_code,
			type: 'GET',
			success: function (response){
				console.log(response);
				$('#'+detail_unit).find('option').remove();
				$("#"+detail_unit).append($("<option></option>")
                    		.attr("value",response.data.stock_unit)
                    		.text(response.data.stock_unit));
				$.each(response.data.converts, function(key, value){
					$("#"+detail_unit).append($("<option></option>")
                    					.attr("value",value.unit)
                    					.text(value.unit));
				});
			},
			error: function(response){
				console.log('stock code', response);
			}
		})
	}

	function save(){
		$('#formCreatePurchasingRetur').submit();
	}
</script>