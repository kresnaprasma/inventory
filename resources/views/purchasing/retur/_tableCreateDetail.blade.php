<table class="table table-striped table-color" id="tableDetailPurchasing">
    <thead>
      <th>Stock No.</th>
      <th>Qty</th>
      <th>Unit</th>
      <th>Price</th>
      <th>Total</th>
      <th>&nbsp;</th>
    </thead>
    <tbody>
      @for ($i = 1; $i <= 4; $i++)
      <tr>
        <td class="col-md-4">
          <select name="stock_code[]" class="form-control detail-purchasing stock_code" id="stock_code".$i>
            <option value=""></option>
            @foreach ($stocks as $s)
              <option value="{{ $s->stock_code }}">{{ $s->stock_code }} - {{ $s->stock_name }}</option>
            @endforeach
          </select>
        </td>
        <td>
          {!! Form::text('detail_qty[]',null, ['class'=>'form-control detail_qty detail-table','id'=>'detail_qty'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
        <td>{!! Form::select('detail_unit[]',[], null, ['class'=>'form-control detail_unit detail-table','id'=>'detail_unit'.$i,'readonly']) !!}</td>
        <td>{!! Form::text('detail_amount[]', null, ['class'=>'form-control detail_amount detail-table','id'=>'detail_amount'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
        <td>{!! Form::text('detail_total[]', 0, ['class'=>'form-control detail_total detail-table','id'=>'detail_total'.$i,'readonly']) !!}</td>
        <td>
          {!! Form::hidden('detail_id[]', null, ['id'=>'detail_id'.$i,'class'=>'detail_id detail-table']) !!}
            <a href="javascript:void(0)" class="del_rincian_create btn btn-danger btn-xs">
              <i class="fa fa-close" aria-hidden="true"></i>
            </a>
        </td>
      </tr>
      @endfor
    </tbody>
    <tfoot>
      <tr>
        <td class="text-right"><b>Total Qty:</b></td>
        <td>{{ Form::text('total_qty', null,['class'=>'form-control','id'=>'total_qty']) }}</td>
        <td colspan="1" class="text-right"><b>Sub Total</b></td>
        <td colspan="3">{!! Form::text('sub_total',null,['class'=>'form-control form-total','id'=>'sub_total','readonly']) !!}</td>
      </tr>

      <tr>
        <td>
          <button type="button" class="btn btn-primary" onclick="addrow()"><i class="fa fa-plus-circle"></i> Add Row</button>
          <button type="button" class="btn btn-primary" id="btn-note"><i class="fa fa-plus-circle"></i> Notes</button>
        </td>
        <td colspan="2" class="text-right"><b>Discount %</b></td>
        <td colspan="3">
          {!! Form::number('purchasing_discount_percentage',null,['class'=>'form-control form-total','id'=>'purchasing_discount_percentage', 'max'=>'100','min'=>'1','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
        </td>
      </tr>
      <tr>
        <td>{{ Form::textarea('purchasing_note', null, ['class'=>'form-control', 'style'=>'display:none','id'=>'purchasing_note']) }}</td>
        <td colspan="2" class="text-right">
          <div class="checkbox">
            <label>
              <input type="checkbox" id="purchasing_tax"> <b>Tax</b>
            </label>
          </div>
        </td>
        <td colspan="3">{!! Form::text('purchasing_tax',null,['class'=>'form-control form-total','id'=>'purchasing_tax_amount']) !!}</td>
      </tr>
      <tr>
        <td colspan="3" class="text-right"><b>Total</b></td>
        <td colspan="3">
          {!! Form::text('purchasing_total',null,['class'=>'form-control form-total','id'=>'purchasing_total','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
        </td>
      </tr>
    </tfoot>
</table>