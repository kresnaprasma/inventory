<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Purchasing Retur - <b>@if ($edit){{ $purchasing->purchasing_no }}@else {{ App\PurchasingRetur::Maxno() }}@endif</b>
      | From: <b><a href="{{ route('purchasing.show',$purchasing->id) }}">{{ $purchasing->purchasing_no }}</a></b>
      {!! Form::hidden('purchasing_no',$purchasing->purchasing_no) !!}
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            @if ($purchasing->details->count() > 0)
              <table class="table table-striped table-color" id="tableDetailPurchasing">
                  <thead>
                    <th>Stock No.</th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Qty Retur</th>
                    <th>Unit Retur</th>
                  </thead>
                  <tbody>
                    @php $i = 0; @endphp
                    @foreach ($purchasing->details as $pd)
                      @php $i = $i + 1; @endphp
                      <tr>
                        <td class="col-md-4">
                          {!! Form::hidden('detail_id[]', $pd->id) !!}
                          <select name="stock_code[]" class="form-control detail-purchasing stock_code" id="stock_code{{ $i }}">
                            <option value=""></option>
                            @foreach ($inventories as $s)
                              @if ($pd->inventory_code == $s->inventory_code)
                                <option value="{{ $s->inventory_code }}" selected="selected">{{ $s->inventory_code }} - {{ $s->inventory_name }}</option>
                              @else 
                                <option value="{{ $s->inventory_code }}">{{ $s->inventory_code }} - {{ $s->inventory_name }}</option>
                              @endif
                            @endforeach
                          </select>
                        </td>
                        <td>
                          {!! Form::text('detail_qty[]',number_format($pd->detail_qty), ['class'=>'form-control detail_qty detail-table','id'=>'detail_qty'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                        <td>
                          {!! Form::text('detail_unit[]', $pd->detail_unit, ['class'=>'form-control detail_unit detail-table','id'=>'detail_unit'.$i,'readonly']) !!}
                        </td>
                        <td>{!! Form::text('retur_qty[]', null, ['class'=>'form-control retur_qty detail-table','id'=>'retur_qty'.$i]) !!}</td>
                        <td>{!! Form::select('retur_unit[]',[], null, ['class'=>'form-control retur_unit detail-table','id'=>'retur_unit'.$i]) !!}</td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
            @endif
        </div>
        <!-- /.col -->
    </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">        
        <button type="button" class="btn btn-flat btn-danger pull-right">
          <i class="fa  fa-ban" aria-hidden="true"></i> Cancel
        </button>
        &nbsp;
        <button type="button" class="btn btn-flat btn-primary pull-right" onclick="save()">
          <i class="fa fa-check-square" aria-hidden="true"></i> Save
        </button>
      </div>
  </div>
</div>
<!-- /.box -->
