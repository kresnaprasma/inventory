@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Purchasing Manajemen</small>
    </h1>
    <ol class="breadcrumb">
      <li>Purchasing</li>
      <li><a href="{{ route('purchasing.index') }}">index</a></li>
      <li class="active">Payment</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($purchasing = new \App\PurchasingPayment, ['route' => 'purchasing.payment.store','id'=>'formCreatePayment']) !!}
      @include('purchasing.payment._form',['edit'=>false])  
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('purchasing.payment._js')
@stop