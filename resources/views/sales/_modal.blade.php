{{-- Create Modal --}}
<div class="modal fade" id="createOrderModal" tabindex="-1" role="dialog" aria-labelledby="Create Bank">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Order List</h4>
			</div>
			{!! Form::open() !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-color" id="list-order-table">
							<thead>
								<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
								<th>Date</th>
								<th>Outlet</th>
								<th>Sell To:</th>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="checkOrder()" class="btn btn-red">Check</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>