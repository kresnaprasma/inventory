<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Sales - <b>@if ($edit){{ $sales->sales_no }}@else {{ App\Sales::OfMaxno() }}@endif</b>
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="form-group{{ $errors->has('sales_date') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('sales_date', "Date*", ['class'=>'control-label']) !!}
        <div class='input-group date' id='sales_date'>
          {!! Form::text('sales_date',old('sales_date'), ['class'=>'form-control']) !!}
          <span class="input-group-addon input-group-addon-silver">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </span>
        </div>
        @if ($errors->has('sales_date'))
            <span class="help-block">
                <strong>{{ $errors->first('sales_date') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('sales_due_date') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('sales_due_date', "Due Date", ['class'=>'control-label']) !!}
        <div class='input-group date' id='sales_due_date'>
          {!! Form::text('sales_due_date',old('sales_due_date'), ['class'=>'form-control']) !!}
          <span class="input-group-addon input-group-addon-silver">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </span>
        </div>
        @if ($errors->has('sales_due_date'))
            <span class="help-block">
                <strong>{{ $errors->first('sales_due_date') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('customer_no') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('customer_no', "Customer", ['class'=>'control-label']) !!}
        {!! Form::select('customer_no', $customer_list, old('customer_no'), ['class'=>'form-control']) !!}
        @if ($errors->has('customer_no'))
            <span class="help-block">
                <strong>{{ $errors->first('customer_no') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group col-xs-1 col-md-1">
        {!! Form::label('select_order', "Order", ['class'=>'control-label']) !!}
        <button class="btn btn-primary" type="button" onclick="listOrder()">
          <i class="fa fa-search"></i> Select Order
        </button>
      </div>
    </div>
    <!-- /.row -->
    <hr>
    <div class="row" style="">
        <div class="col-md-12">
            @if ($sales->details->count() > 0)
              <table class="table table-striped table-color" id="tableDetailSales">
                  <thead>
                    <th>Inventory No.</th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>&nbsp;</th>
                  </thead>
                  <tbody>
                    @php $i = 0; @endphp
                    @foreach ($sales->details as $sd)
                      @php $i = $i + 1; @endphp
                      <tr>
                        <td class="col-md-4">
                          <select name="stock_code[]" class="form-control detail-sales stock_code" id="stock_code".$i>
                            <option value=""></option>
                            @foreach ($stocks as $s)
                              @if ($sd->inventory_code == $s->inventory_code)
                                <option value="{{ $s->inventory_code }}" selected="selected">{{ $s->inventory_code }} - {{ $s->inventory_name }}</option>
                              @else 
                                <option value="{{ $s->inventory_code }}">{{ $s->inventory_code }} - {{ $s->inventory_name }}</option>
                              @endif
                            @endforeach
                          </select>
                        </td>
                        <td>
                          {!! Form::text('detail_qty[]',number_format($sd->detail_qty), ['class'=>'form-control detail_qty detail-table','id'=>'detail_qty'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                        <td>
                          @php
                            $unit = [];
                            foreach ($sd->converts as $value) {
                              $unit[$value->unit] = $value->unit;
                            }
                          @endphp
                          {!! Form::select('detail_unit[]',[$ps->inventory->stock_unit=>$ps->inventory->stock_unit]+$unit, $sd->detail_unit, ['class'=>'form-control detail_unit detail-table','id'=>'detail_unit'.$i]) !!}
                        </td>
                        <td>{!! Form::text('detail_amount[]', number_format($sd->detail_amount), ['class'=>'form-control detail_amount detail-table','id'=>'detail_amount'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                        <td>{!! Form::text('detail_total[]', number_format($sd->detail_total), ['class'=>'form-control detail_total detail-table','id'=>'detail_total'.$i,'readonly']) !!}</td>
                        <td>
                            {!! Form::hidden('detail_id[]', $sd->id, ['id'=>'detail_id'.$i,'class'=>'detail_id detail-table']) !!}
                            <a href="javascript:void(0)" class="del_rinc btn btn-danger btn-xs">
                              <i class="fa fa-close" aria-hidden="true"></i>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td class="text-right"><b>Total Qty:</b></td>
                      <td>{{ Form::text('total_qty', null,['class'=>'form-control','id'=>'total_qty']) }}</td>
                      <td colspan="1" class="text-right"><b>Sub Total</b></td>
                      <td colspan="3">{!! Form::text('sub_total',null,['class'=>'form-control form-total','id'=>'sub_total','readonly']) !!}</td>
                    </tr>

                    <tr>
                      <td>
                        <button type="button" class="btn btn-primary" onclick="addrow()"><i class="fa fa-plus-circle"></i> Add Row</button>
                        <button type="button" class="btn btn-primary" id="btn-note"><i class="fa fa-plus-circle"></i> Notes</button>
                      </td>
                      <td colspan="2" class="text-right"><b>Discount</b></td>
                      <td colspan="3">
                        {!! Form::number('sales_discount_percentage',null,['class'=>'form-control form-total','id'=>'sales_discount_percentage', 'max'=>'100','min'=>'1','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
                      </td>
                    </tr>
                    <tr>
                      <td>{{ Form::textarea('sales_note', null, ['class'=>'form-control', 'style'=>'display:none','id'=>'sales_note']) }}</td>
                      <td colspan="2" class="text-right">
                        <div class="checkbox">
                          <label>
                            @if ($sales->sales_tax > 0)
                              <input type="checkbox" id="sales_tax" checked="checked"> 
                              <b>Tax </b>
                            @else
                              <input type="checkbox" id="sales_tax">  
                              <b>Tax </b>
                            @endif
                          </label>
                        </div>
                      </td>
                      <td colspan="3">{!! Form::text('sales_tax',null,['class'=>'form-control form-total','id'=>'sales_tax_amount']) !!}</td>
                    </tr>
                    <tr>
                      <td colspan="3" class="text-right"><b>Total</b></td>
                      <td colspan="3">
                        {!! Form::text('sales_total',null,['class'=>'form-control form-total','id'=>'sales_total','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
                      </td>
                    </tr>
                  </tfoot>
              </table>
            @else
              @include('sales._tableCreateDetail')
            @endif
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">

        <button type="button" class="btn btn-flat btn-default">
          <i class="fa fa-pencil-square" aria-hidden="true"></i> Save as draft
        </button>
        
        <button type="button" class="btn btn-flat btn-danger pull-right">
          <i class="fa  fa-ban" aria-hidden="true"></i> Cancel
        </button>
        &nbsp;
        <button type="button" class="btn btn-flat btn-primary pull-right" onclick="save()">
          <i class="fa fa-check-square" aria-hidden="true"></i> Save
        </button>
      </div>
  </div>
</div>
<!-- /.box -->
