@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-cart-plus"></i> Sales
        <small>Sales Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('sales.index') }}">Sales</a></li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray-dark">
            <div class="inner">
              <h3>{{ number_format($sales->sum('sales_total')) }}</h3>

              <p>Total Sales</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
        	<!-- small box -->
        	<div class="small-box bg-red">
        		<div class="inner">
        			<h3>{{ number_format($sales->where('status','outstanding')->sum('sales_total')) }}</h3>
        			<p>Total Outstanding</p>
        		</div>
        		<div class="icon">
              		<i class="ion ion-stats-bars"></i>
            	</div>
        	</div>
        </div>
    </div>
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Tables Sales
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
		         	<a href="{{ route('sales.create') }}" class="btn btn-primary">
		            	<i class="fa fa-user-plus" aria-hidden="true"></i> New
		          	</a>
		          
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Sort By:
		          	</span>
		          	{!! Form::select('status',[''=>'--Status--','completed'=>'Completed','outstanding'=>'Outstanding'], null,['class'=>'btn btn-primary']) !!}
		          	<button type="button" class="btn btn-primary" id="reportrange">
			            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			            <span>
			            	@if ($begin)
			                	{{ $begin->format('M d, Y') }}
			                	-
			                	{{  $end->format('M d, Y') }}
			              	@endif
			            </span>
			            <b class="caret"></b>
			         </button>
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['id'=>'formStock']) !!}
				<table id="tableStock" class="table table-striped table-color">
					<thead>
						<tr>
							<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>No.</th>
							<th>Date</th>
							<th>Due Date</th>
							<th>Discount</th>
							<th>PPN</th>
							<th>Total</th>
							<th>Status</th>
	            			<th>Action</th>
						</tr>
					</thead>
					<tbody>
	          			@foreach ($sales as $s)
						<tr>
							<td>
								<input type="checkbox" id="idTablePurchasing" value="{{ $s->id }}" name="id[]" class="checkin">
							</td>
							<td>{{ $s->sales_no }}</td>
							<td>{{ date('d F y', strtotime($s->sales_date)) }}</td>
							<td>
								@if ($s->sales_due_date)
									{{ date('d F y', strtotime($s->sales_due_date)) }}
								@endif
							</td>
							<td>{{ number_format($s->sales_discount) }}</td>
							<td>{{ number_format($s->sales_tax) }}</td>
							<td>{{ number_format($s->sales_total) }}</td>
							<td>
								@if ($s->status == 'completed')
									<span class="label label-success" style="font-size: 12px">{{ ucfirst($s->status) }}</span>
								@else
									<span class="label label-warning" style="font-size: 12px">{{ ucfirst($s->status) }}</span>
								@endif
							</td>
							<td>
								<a href="{{ route('sales.show', $s->id) }}" class="btn btn-primary btn-xs">
									<i class="fa fa-pencil-square"></i> Show
								</a>

								@if (!$s->active)
									<a href="#" class="btn btn-xs btn-danger" onclick="active('{{ $s->id }}')">
										<i class="fa fa-close"></i> Deactive
									</a>
								@else
									<a href="#" class="btn btn-xs btn-primary" onclick="active('{{ $s->id }}')">
										<i class="fa fa-check-square"></i> Active
									</a>
								@endif
							</td>
						</tr>
						@endforeach
	        		</tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
  <script type="text/javascript">
    var tableSales = $("#tableSales").DataTable({
    	"dom": "rtip",
      	"pageLength": 10,
      	"retrieve": true,
      	"stateSave": true,
    });

    $("#searchDtbox").keyup(function() {
        tableSales.search($(this).val()).draw();
    });  

    $("select[name=outlet_code]").on('change',function(){
    	updateQueryStringParam('outlet', this.value);
    });
    $("select[name=status]").on('change',function(){
    	updateQueryStringParam('status', this.value);
    });

    $('#reportrange').daterangepicker({
      buttonClasses: ['btn', 'btn-sm'],
      applyClass: 'btn-red',
      cancelClass: 'btn-default',
      startDate: '{{ $begin->format('m/d/y') }}',
      endDate: '{{ $end->format('m/d/y') }}',
      locale: {
        applyLabel: 'Submit',
        cancelLabel: 'Cancel',
        fromLabel: 'From',
        toLabel: 'To',
      },
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, function(start, end, label){
      console.log(start.toISOString(), end.toISOString(), label);

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      updateQueryStringParam('begin', start.format('Y-MM-DD'));
      updateQueryStringParam('end', end.format('Y-MM-DD'));

    });

    function DeleteUser() {
      $('#formStock').submit();
    }
  </script>
@stop