<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Sales Payment - <b>{{ $sales->sales_no }}</b>
      {!! Form::hidden('sales_no', $sales->sales_no) !!}
    </h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p><b>Invoice #: </b>{{ $sales->sales_no }} 
          <b>Total: </b>{{ number_format($sales->sales_total) }} 
          <b>Amount Due: </b> {{ number_format($amount_due) }}</p>    
      </div>
    </div>
    @if ($amount_due != 0)
    <div class="row">
      <div class="col-md-12">
        <div class="form-group{{ $errors->has('payment_date') ? ' has-error' : '' }} col-xs-2 col-md-2">
          {!! Form::label('payment_date', "Date*", ['class'=>'control-label']) !!}
          <div class='input-group date' id='payment_date'>
            {!! Form::text('payment_date',old('payment_date'), ['class'=>'form-control']) !!}
            <span class="input-group-addon input-group-addon-silver">
              <i class="fa fa-calendar" aria-hidden="true"></i>
            </span>
          </div>
          @if ($errors->has('payment_date'))
            <span class="help-block">
                <strong>{{ $errors->first('payment_date') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('payment_amount') ? ' has-error' : '' }} col-xs-3 col-md-3">
            {!! Form::label('payment_amount', "Amount Paid", ['class'=>'control-label']) !!}
            {!! Form::text('payment_amount', number_format($amount_due), ['class'=>'form-control','required','onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}
            @if ($errors->has('payment_amount'))
              <span class="help-block">
                <strong>{{ $errors->first('payment_amount') }}</strong>
              </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('payment_type') ? ' has-error' : '' }} col-xs-2 col-md-2">
            {!! Form::label('payment_type', "Method", ['class'=>'control-label']) !!}
            {!! Form::select('payment_type', ['cash'=>'Cash','bank'=>'Transfer'],old('payment_type'), ['class'=>'form-control','cols'=>'50', 'rows'=>'1']) !!}
            @if ($errors->has('payment_type'))
              <span class="help-block">
                <strong>{{ $errors->first('payment_type') }}</strong>
              </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('payment_note') ? ' has-error' : '' }} col-xs-5 col-md-5">
            {!! Form::label('payment_note', "Notes", ['class'=>'control-label']) !!}
            {!! Form::textarea('payment_note', old('payment_note'), ['class'=>'form-control','cols'=>'50', 'rows'=>'1']) !!}
            @if ($errors->has('payment_note'))
              <span class="help-block">
                <strong>{{ $errors->first('payment_note') }}</strong>
              </span>
            @endif
        </div>
      </div>
    </div>
    @endif
    

    <div class="row">
      <div class="col-md-12">
        <h4>Billing Payment</h4>
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
              <th>Date</th>
              <th>Amount</th>
              <th>Notes</th>
            </thead>
            <tbody>
              @foreach ($payment as $pay)
              <tr>
                <td>{{ date('d/m/Y H:i:s', strtotime($pay->payment_date)) }}</td>
                <td>{{ number_format($pay->payment_amount) }}</td>
                <td>{{ $pay->payment_note }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <button type="button" class="btn btn-flat btn-danger pull-right" onclick="history.go(-1);">
      <i class="fa  fa-ban" aria-hidden="true"></i> Cancel
    </button>
    &nbsp;
    @if ($amount_due != 0)
    <button type="button" class="btn btn-flat btn-primary pull-right" onclick="save()">
      <i class="fa fa-check-square" aria-hidden="true"></i> Save
    </button>
    @endif
  </div>
</div>