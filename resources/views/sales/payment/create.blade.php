@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Sales Manajemen</small>
    </h1>
    <ol class="breadcrumb">
      <li>Sales</li>
      <li><a href="{{ route('sales.index') }}">index</a></li>
      <li class="active">Payment</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($sales, ['route' => 'sales.payment.store','id'=>'formCreatePayment']) !!}
      @include('sales.payment._form',['edit'=>false])  
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('sales.payment._js')
@stop