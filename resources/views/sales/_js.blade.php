<script type="text/javascript">
	total_price();
	$('.stock_code').change(function() {
		var inventory_code = $(this).val();
		var detail_unit = $(this).closest('tr').find('td .detail_unit').attr('id');
		var detail_amount = $(this).closest('tr').find('td .detail_amount').attr('id');
		var detail_qty = $(this).closest('tr').find('td .detail_qty').attr('id');
		var detail_total = $(this).closest('tr').find('td .detail_total').attr('id');

		getInventoryCode(inventory_code, detail_unit, detail_amount, detail_qty,detail_total);

		$('select[name=outlet_sell]').prop('disabled', true);
		$('input[name=sales_date').prop('disabled', true);

		// $(".stock_code option[value='"+stock_code+"']").remove();

		// getStockCode(stock_code,date,outlet,detail_unit,detail_amount);

	});

	$('.detail_unit').change(function(){
		var inventory_code_id = $(this).closest('tr').find('td .stock_code').attr('id');
		var detail_amount = $(this).closest('tr').find('td .detail_amount').attr('id');

		var inventory_code = $('#'+inventory_code_id).val();
		var detail_unit = $(this).val();
		
		$.ajax({
			url: '/api/web/inventory/convert-price/'+inventory_code+'?unit='+detail_unit+'&price-type=selling',
			type:'GET',
			success: function(response){
				$('#'+detail_amount).val(response.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,''));
			},
			error: function(response){
				console.log('Error :'+response);
			}
		});
		total_price();
	});

	$('#sales_date').datepicker({
    	autoclose: true
  	});

  	$('#sales_due_date').datepicker({
    	autoclose: true
  	});

  	$("select[name=outlet_code]").on('change',function(){
    	updateQueryStringParam('outlet', this.value);
    });

  	$("#outlet_sell").change(function(){
  		$.ajax({
  			url: '/api/web/sales/order?outlet='+$(this).val(),
  			type: 'GET',
  			success: function(response){
  				$('#list-order-table tbody tr').remove();
				$.each(response.data, function(key, value){
					$('#list-order-table').append('<tr><td><input type="checkbox" id="idTableOrderList" value="'+value.id+'" name="id_order" class="checkin"></td><td>'+value.sales_order_no+'</td><td>'+value.sales_order_date+'</td><td>'+value.sales+'</td></tr>');
				});
  			},
  			error: function(response){

  			}
  		})
  	});

  	$(".del_rinc").click(function(){
		var rowCount = $('#tableDetailSales tbody tr').length;
		var trFam = $(this).closest("tr");
		var id = trFam.find('.detail_id').attr('id');
		var id_val = $('#'+id).val();

	    if(rowCount > 1)
	    {
	    	$.ajax({
				url: '/api/web/sales/detail/'+id_val,
				type: 'DELETE',
				success: function(response){
					trFam.remove();
					total_price();
				},
				error: function(response){
					return false;
				}
			});
	    }
	    else{
	    	return false;
	    }
	});


  	$(".del_rinc").click(function(){
		var rowCount = $('#tableDetailPurchasing tbody tr').length;
		var trFam = $(this).closest("tr");
		var id = trFam.find('.detail_id').attr('id');
		var id_val = $('#'+id).val();

	    if(rowCount > 1)
	    {
	    	$.ajax({
				url: '/api/web/sales/detail/'+id_val,
				type: 'DELETE',
				success: function(response){
					trFam.remove();
					total_price();
				},
				error: function(response){
					return false;
				}
			});
	    }
	    else{
	    	return false;
	    }
	});

	$(".del_rincian_create").click(function(){
		var rowCount = $('#tableDetailSales tbody tr').length;
		var trFam = $(this).closest("tr");
		var id = trFam.find('.detail_id').attr('id');
		var id_val = $('#'+id).val();

	    if(rowCount > 1)
	    {
			trFam.remove();
			return false;
	    }
	    else{
	    	return false;
	    }
	});

	$('#sales_tax').change(function(){
		total_price();
	});

	$("#btn-note").click(function() {
  		$("#sales_note").toggle("slow");
	});

  	function addrow()
	{
    	var lastRow = $('#'+'tableDetailSales' + " tbody tr:last");
    	var newRow = lastRow.clone(true);
	    //append row to this table
	    $('#tableDetailSales').append(newRow);

	    // clear input
	    $("#tableDetailSales tbody tr:last .detail-table").val('');
	   
	    var this_id = $("#tableDetailSales tbody tr:last  .detail_qty").attr('id');
	    var this_id1 = $("#tableDetailSales tbody tr:last .detail_unit").attr('id');
	    var this_id2 = $("#tableDetailSales tbody tr:last  .detail_amount").attr('id');
	    var this_id3 = $("#tableDetailSales tbody tr:last  .detail_discount").attr('id');
	    var this_id4 = $("#tableDetailSales tbody tr:last  .detail_total").attr('id');
	    var this_id5 = $("#tableDetailSales tbody tr:last  .detail_id").attr('id');
	    var this_id_stock = $("#tableDetailSales tbody tr:last  .inventory_code").attr('id');
	    var this_id_order_no = $("#tableDetailSales tbody tr:last  .sales_order_no").attr('id');
	    
	    $("#tableDetailSales tbody tr:last td .detail_qty").attr('id', this_id + 1);
	    $("#tableDetailSales tbody tr:last td .detail_unit").attr('id', this_id1 + 1);
	    $("#tableDetailSales tbody tr:last td .detail_amount").attr('id', this_id2 + 1);
	    $("#tableDetailSales tbody tr:last td .detail_discount").attr('id', this_id3 + 1);
	    $("#tableDetailSales tbody tr:last td .detail_total").attr('id', this_id4 + 1);
	    $("#tableDetailSales tbody tr:last td .detail_id").attr('id', this_id4 + 1);
	    $("#tableDetailSales tbody tr:last td .inventory_code").attr('id', this_id_stock + 1);
	    $("#tableDetailSales tbody tr:last td .purchasing_order_no").attr('id', this_id_order_no + 1);
	    $('#'+this_id1 + 1).find('option').remove();
	}

	function getInventoryCode(inventory_code, detail_unit, detail_amount, detail_qty,detail_total) {
		$.ajax({
			url: '/api/web/inventory/'+ inventory_code+'?price=selling',
			type: 'GET',
			success: function (response){
				$('#'+detail_amount).val('');
				$('#'+detail_unit).find('option').remove();
				$("#"+detail_unit).append($("<option></option>")
                    		.attr("value",response.data.stock_unit)
                    		.text(response.data.stock_unit));

				if(response.data.prices){
					$('#'+detail_amount).val(response.data.prices[0].price.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,''));	
					$('#'+detail_total).val(response.data.prices[0].price.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,''));
				}else{
					$('#'+detail_amount).val('');
					$('#'+detail_total).val('');
				}

				var qty = $('#'+detail_qty).val(1);
				$.each(response.data.converts, function(key, value){
					$("#"+detail_unit).append($("<option></option>")
                    					.attr("value",value.unit)
                    					.text(value.unit));
				});
			},
			error: function(response){
				console.log('stock code', response);
			}
		})
	}

	function number(this_id){
		formatNumber(this_id);

		var thisTableId = $(this_id).parents("table tbody").attr("id");
		var id = $(this_id).closest('tr').find('.detail_qty').attr('id');
		var id2 = $(this_id).closest('tr').find('.detail_amount').attr('id');
		var id3 = $(this_id).closest('tr').find('.detail_total').attr('id');
		
		var id_val = $("#"+id).val();
		var id_num = parseFloat(id_val.replace(/[^0-9-.]/g, ''));

		var id_val2 = $("#"+id2).val();
		var id_num2 = parseFloat(id_val2.replace(/[^0-9-.]/g, ''));

		var cont = id_num * id_num2;

		var num = cont.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
		
		$("#"+id3).val(num);

		total_price();
		total_qty();
	}
	function numberDisc(this_id){
		total_price();
	}

	function total_price(){
		var sum = 0;
		$('#tableDetailSales').find('.detail_total').each(function(){
		    var value = $(this).val();
		    var value_num = parseFloat(value.replace(/[^0-9-.]/g, ''));
		    if (!isNaN(value_num)) sum += value_num;
		});
		
		var subtotal = sum.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
		
		var discount = $('#sales_discount_percentage').val();
		var tax = $('#sales_tax').is(':checked');
		
		if(discount != ""){
			var sub_total_discount = sum - (discount);
		}else{
			var sub_total_discount = sum;
		}

		if(tax){
			var tax = sub_total_discount * 0.1;
			tax = tax.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
			var total = sub_total_discount + (sub_total_discount * 0.1);
			$('#sales_tax_amount').val(tax);
		}else{
			var total = sub_total_discount;
		}

		var num = total.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');

		$('#sub_total').val(subtotal);
		$('#sales_total').val(num);
	}

	function total_qty(){
    	var sum_qty = 0;
    	$('#tableDetailSales').find('.detail_qty').each(function(){
		    var value = $(this).val();
		    var value_num = parseFloat(value.replace(/[^0-9-.]/g, ''));
		    if (!isNaN(value_num)) sum_qty += value_num;
		});

		var num = sum_qty.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
		
		if(num != 0){
			var total = $('#total_qty').val(num);
		}
    }

    function save() {
    	$('.form-control').prop('disabled', false);
    	$('#formCreateSales').submit();
    }

    function listOrder() {
    	getOrder();
    	$('#createOrderModal').modal('show');
    }

    function getOrder() {
    	var outlet = $('#outlet_code').val();
    	$.ajax({
    		url: '/api/web/sales/order?outlet='+outlet,
    		type: 'GET',
    		success: function(response){
    			$('#list-order-table tbody tr').remove();
    			$.each(response.data, function(key, value){
					$('#list-order-table').append('<tr><td><input type="checkbox" id="idTableOrderList" value="'+value.id+'" name="id_order" class="checkin"></td><td>'+value.sales_order_no+'</td><td>'+value.sales_order_date+'</td></tr>');
				});
    		},
    		error: function(response){

    		}
    	})
    }

    function checkOrder(){
    	deleteDetail()
		var outlet = $('#outlet_code').val();
    	$('.checkin:checkbox:checked').each(function(){
    		var sThisVal = (this.checked ? $(this).val() : "");
    		alert(sThisVal);
    		$.ajax({
    			url: '/api/web/sales/order/'+sThisVal,
    			type: 'GET',
    			success: function(response){
    				console.log(response);
    				var sales_order_no = response.data.sales_order_no;
    				var count = 0;
    				var detail_count = response.data.details.length - 1;
    				$('#outlet_sell').val(response.data.outlet_sell);

    				$.each(response.data.details, function(key, value){
    					var lastRow = $('#'+'tableDetailSales' + " tbody tr:last");
    					var id_order_no = $(lastRow).find('td .sales_order_no').attr('id');
						var id_inventory_code = $(lastRow).find('td .inventory_code').attr('id');
						var id_detail_unit = $(lastRow).find('td .detail_unit').attr('id');
						var id_detail_qty = $(lastRow).find('td .detail_qty').attr('id');
						var id_detail_amount = $(lastRow).find('td .detail_amount').attr('id');
						var id_detail_total = $(lastRow).find('td .detail_total').attr('id');		

						$.ajax({
							url: '/api/web/inventory/'+ value.inventory_code+'?price=selling',
							type: 'GET',
							success: function (response){
								$('#'+id_detail_unit).find('option').remove();
								$("#"+id_detail_unit).append($("<option></option>")
                    				.attr("value",response.data.stock_unit)
                    				.text(response.data.stock_unit));

								$.each(response.data.converts, function(key, value){
									$("#"+id_detail_unit).append($("<option></option>")
                    					.attr("value",value.unit)
                    					.text(value.unit));
								});
								
								$('#'+id_order_no).val(sales_order_no);
								$('#'+id_inventory_code).val(value.inventory_code);
    							$('#'+id_detail_unit).val(value.detail_unit);
    							$('#'+id_detail_qty).val(value.detail_qty.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,''));
    							$('#'+id_detail_amount).val(value.detail_amount.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,''));

    							var detail_total = value.detail_qty * value.detail_amount;
    							$('#'+id_detail_total).val(detail_total.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,''));
    							total_price();
    							total_qty();
							},	
							error: function (response){

							}
						});
						addrow();
    				});  
    			},
    			error: function(response){

    			}
    		})
    	})
    	$('#createOrderModal').modal('hide');
    }

    function deleteDetail() {
    	var rowCount = 0;
    	$('#tableDetailSales tbody tr').each(function(){
    		rowCount = rowCount + 1;
    		if(rowCount > 1){
    			this.remove();
    		}
    	});
    }

</script>