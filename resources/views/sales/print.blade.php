@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection

@section('content')
  <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> TB. Aneka Bangunan
            <small class="pull-right">Date: {{ date('d/M/Y', strtotime($sales->sales_date)) }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From:

            <address>
              <strong>Jl. Kaligawe KM. 3</strong><br>
              Semarang<br>
              Phone: 085727187957<br>
              Email: anekabangunan@gmail.com
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          @if ($sales->outletSell)
            To:

            <address>
              <strong>Jl. Kaligawe KM. 3</strong><br>
              Semarang<br>
              Phone: 085727187957<br>
              Email: anekabangunan@gmail.com
            </address>
          @endif
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice# {{ $sales->sales_no }}</b><br>
          <br>
          <b>Payment Due:</b> {{ date('d/M/Y', strtotime($sales->sales_due_date)) }}<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Stock No.</th>
              <th>Qty</th>
              <th>Unit</th>
              <th style="text-align: right;">Price</th>
              <th style="text-align: right;">Total</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($sales->details as $sd)
                <tr>
                  <td>{{ $sd->inventory_code }} - {{ App\Inventory::where('inventory_code', $sd->inventory_code)->first()->inventory_name }}</td>
                  <td>{{ $sd->detail_qty }}</td>
                  <td>{{ $sd->detail_unit }}</td>
                  <td style="text-align: right">{{ number_format($sd->detail_amount) }}</td>
                  <td style="text-align: right;">{{ number_format($sd->detail_qty * $sd->detail_amount) }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Notes:</p>
          @if ($sales->sales_note)          
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              {{$sales->sales_note}}
            </p>
          @endif
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table" style="text-align: right;">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>{{ number_format($sales->sales_sub_total) }}</td>
              </tr>
              <tr>
                <th>Discount</th>
                <td>({{ number_format($sales->sales_discount) }})</td>
              </tr>
              <tr>
                <th>PPN/VAT (10%)</th>
                <td>{{ number_format($sales->sales_tax) }}</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>{{ number_format($sales->sales_total) }}</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{ route('sales.print', $sales->id) }}" target="_blank" class="btnprn btn btn-default"><i class="fa fa-print"></i> Print</a>
          <a href="{{ route('sales.payment.create',$sales->id) }}" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </a>
          @if ($sales->payment->sum('payment_amount') >= $sales->sales_total)
            
          @else
          <a href="{{ route('sales.edit',$sales->id) }}" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Revise
          </a>
          @endif
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
@stop

@section('script')
  @include('purchasing._js')
  <script type="text/javascript">
     $(document).ready(function(){
        $('.btnprn').printPage();
     });
  </script>
@stop