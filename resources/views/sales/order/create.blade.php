@extends('layouts.admin.admin')

@section('style')
  <style type="text/css">
    .table-stock, th, td {
      border: 1px solid black;
      /*font-size: 12px;*/
    }
    .form-unit{
      border: 0px;
      width: 100%;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Sales Manajemen</small>
    </h1>
    <ol class="breadcrumb">
      <li>Sales</li>
      <li><a href="{{-- {{ route('sales.index') }} --}}">index</a></li>
      <li><a href="{{ route('sales.order.index') }}">Order</a></li>
      <li class="active">Create</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($order = new \App\SalesOrder, ['route' => 'sales.order.store','id'=>'formCreateSalesOrder']) !!}
      @include('sales.order._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('sales.order._js')
  <script type="text/javascript">
    function save(){
      $('#formCreateSalesOrder').submit();
    }
  </script>
@stop