@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    table.table{
      font-size: 8pt;
    }
    table.table-bordered{
      border:1px solid black;
      margin-top:20px;
    } 
    table.table-bordered > thead > tr > th{
      border:1px solid black;
    }
    table.table-bordered > tbody > tr > td{
      border:1px solid black;
    }
    .pagebreak { page-break-before: always; }
    @media print{
      .table thead th,
      .table tbody tr td{
        border-width: 1px !important;
        border-style: solid !important;
        border-color: black !important;
        font-size: 8px !important;
        background-color: red;
        padding:4px;
        -webkit-print-color-adjust:exact ;
      }
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Sales Manajemen</small>
    </h1>
    <ol class="breadcrumb">
      <li>Sales</li>
      <li><a href="{{-- {{ route('sales.index') }} --}}">index</a></li>
      <li class="active">Create</li>
    </ol>
  </section>
@stop


@section('content')
  <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> TB. Aneka Bangunan
            <small class="pull-right">Date: {{ date('d/M/Y', strtotime($sales->sales_order_date)) }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From:

            <address>
              <strong>{{ $sales->outlet_code }} | {{ $sales->outlet->name }}</strong><br>
              {{ $sales->outlet->address }}<br>
              Phone: {{ $sales->outlet->phone }}<br>
              Email: {{ $sales->outlet->email }}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice# {{ $sales->sales_order_no }}</b><br>
          <br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Inventory No.</th>
              <th>Qty</th>
              <th>Unit</th>
              <th style="text-align: right;">Price</th>
              <th style="text-align: right;">Total</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($sales->details as $sd)
                <tr>
                  <td>{{ $sd->inventory_code }} - {{ App\Inventory::where('inventory_code', $sd->inventory_code)->first()->inventory_name }}</td>
                  <td>{{ $sd->detail_qty }}</td>
                  <td>{{ $sd->detail_unit }}</td>
                  <td style="text-align: right">{{ number_format($sd->detail_amount) }}</td>
                  <td style="text-align: right;">{{ number_format($sd->detail_qty * $sd->detail_amount) }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <?php $count = 1; ?>
        @foreach ($inventory_category as $ic)
          <div class="col-md-4 invoice-col">
            <h4>{{ $ic->name }}</h4>
            <table class="table table-bordered">
              <thead>
                <th>No</th>
                <th>Inventory Item</th>
                <th>Qty</th>
                <th>Satuan</th>
              </thead>
              <tbody>
                @php 
                  $no = 0; 
                  $table_length = $table_row - $ic->inventories->count();
                @endphp
                @foreach ($ic->inventories as $s)
                  <tr>
                    <td>{{ $no = $no + 1 }}</td>
                    <td>{{ $s->inventory_code }} - {{ $s->inventory_name }}</td>
                    @if ($s->sales_orders->count() > 0)
                      @foreach ($s->sales_orders as $p)
                        <td>{{ $p->detail_qty }}</td>
                        <td>{{ $p->detail_unit }}</td>
                      @endforeach
                    @else
                    <td></td>
                    <td></td>
                    @endif
                  </tr>
                @endforeach
                @for ($i = 0; $i < $table_length; $i++)
                  <tr>
                    <td>{{ $no = $no + 1 }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                @endfor
              </tbody>
            </table>

            <div class="pagebreak"> </div>
          </div>
          <?php $count++; ?>
        @endforeach
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
@stop

@section('script')
  @include('purchasing._js')
  <script type="text/javascript">
     $(document).ready(function(){
        $('.btnprn').printPage();
     });
  </script>
@stop