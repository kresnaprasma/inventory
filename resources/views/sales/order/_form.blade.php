<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Sales - <b>@if ($edit){{ $order->sales_order_no }}@else {{ App\SalesOrder::OfMaxno('---') }}@endif</b>
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="form-group{{ $errors->has('sales_order_date') ? ' has-error' : '' }} col-xs-2 col-md-2">
        {!! Form::label('sales_order_date', "Date*", ['class'=>'control-label']) !!}
        <div class='input-group date' id='sales_order_date'>
          {!! Form::text('sales_order_date',old('sales_order_date'), ['class'=>'form-control','id'=>'sales_order_date']) !!}
          <span class="input-group-addon input-group-addon-silver">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </span>
        </div>
        @if ($errors->has('sales_order_date'))
            <span class="help-block">
                <strong>{{ $errors->first('sales_order_date') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <!-- /.row -->

    <hr>
    <div class="row">
      <div class="col-md-4">
        <h1>Topping</h1>
        <table style="width: 100%" class="table-stock">
          <colgroup>
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 50%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 20%;">
          </colgroup>
          
          
          
          <!-- Put <thead>, <tbody>, and <tr>'s here! -->
          <thead>
            <th>No.</th>
            <th>Item</th>
            <th>Qty</th>
            <th>Unit</th>
          </thead>
          <tbody>
            @php
              $no = 0;
            @endphp
            @foreach ($cat_am->inventory as $sa)
              @php
                $unit = 0;
              @endphp
              @if ($sa->converts->count() != 0)
                @foreach ($sa->converts as $sc)
                  @php
                    $unit = [
                      $sa->stock_unit => $sa->stock_unit,
                      $sc->unit => $sc->unit
                    ];
                  @endphp
                @endforeach
              @else
                @php
                  $unit = [
                    $sa->stock_unit => $sa->stock_unit,
                  ];
                @endphp 
              @endif
              
              <tr>
                <td>{{ $no = $no + 1 }}</td>
                <td>
                  {{ $sa->inventory_name }}
                  {!! Form::hidden('inventory_code[]', $sa->inventory_code) !!}
                </td>
                <td><input type="text" name="detail_qty[]" class="form-unit"></td>
                <td>{!! Form::select('detail_unit[]', $unit, null,['class'=>'form-unit']) !!}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <div class="col-md-4">
        <h1>Bahan & Alat</h1>
        <table style="width: 100%" class="table-stock">
          <colgroup>
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 50%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 20%;">
          </colgroup>
          
          
          
          <!-- Put <thead>, <tbody>, and <tr>'s here! -->
          <thead>
            <th>No.</th>
            <th>Item</th>
            <th>Qty</th>
            <th>Unit</th>
          </thead>
          <tbody>
            @php
              $no = 0;
            @endphp
            @foreach ($cat_ct->inventory as $sb)
              @php
                $unit = 0;
              @endphp
              @if ($sb->converts->count() != 0)
                @foreach ($sb->converts as $sc)
                  @php
                    $unit = [
                      $sb->stock_unit => $sb->stock_unit,
                      $sc->unit => $sc->unit
                    ];
                  @endphp
                @endforeach
              @else
                @php
                  $unit = [
                    $sb->stock_unit => $sb->stock_unit,
                  ];
                @endphp 
              @endif
              
              <tr>
                <td>{{ $no = $no + 1 }}</td>
                <td>
                  {{ $sb->inventory_name }}
                  {!! Form::hidden('inventory_code[]', $sb->inventory_code) !!}
                </td>
                <td><input type="text" name="detail_qty[]" class="form-unit"></td>
                <td>{!! Form::select('detail_unit[]', $unit, null,['class'=>'form-unit']) !!}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <div class="col-md-4">
        <h1>Bahan & Alat</h1>
        <table style="width: 100%" class="table-stock">
          <colgroup>
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 50%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 20%;">
          </colgroup>
          <thead>
            <th>No.</th>
            <th>Item</th>
            <th>Qty</th>
            <th>Unit</th>
          </thead>
          <tbody>
            @php
              $no = 0;
            @endphp
            @foreach ($cat_at->inventory as $sc)
              @php
                $unit = 0;
              @endphp
              @if ($sc->converts->count() != 0)
                @foreach ($sc->converts as $sc)
                  @php
                    $unit = [
                      $sc->stock_unit => $sc->stock_unit,
                      $sc->unit => $sc->unit
                    ];
                  @endphp
                @endforeach
              @else
                @php
                  $unit = [
                    $sc->stock_unit => $sc->stock_unit,
                  ];
                @endphp 
              @endif
              
              <tr>
                <td>{{ $no = $no + 1 }}</td>
                <td>
                  {{ $sc->inventory_name }}
                  {!! Form::hidden('inventory_code[]', $sc->inventory_code) !!}
                </td>
                <td><input type="text" name="detail_qty[]" class="form-unit"></td>
                <td>{!! Form::select('detail_unit[]', $unit, null,['class'=>'form-unit']) !!}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">

        <button type="button" class="btn btn-flat btn-default">
          <i class="fa fa-pencil-square" aria-hidden="true"></i> Save as draft
        </button>
        
        <button type="button" class="btn btn-flat btn-danger pull-right">
          <i class="fa  fa-ban" aria-hidden="true"></i> Cancel
        </button>
        &nbsp;
        <button type="button" class="btn btn-flat btn-primary pull-right" onclick="save()">
          <i class="fa fa-check-square" aria-hidden="true"></i> Save
        </button>
      </div>
  </div>
</div>
<!-- /.box -->
