@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Sales
        <small>Sales Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{-- {{ route('sales.index') }} --}}">Sales</a></li>
        <li><a href="{{ route('sales.order.index') }}">Order</a></li>
      </ol>
    </section>
@stop

@section('content')
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Tables Sales Order
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
		         	{{-- <a href="{{ route('order.create') }}" class="btn btn-primary">
		            	<i class="fa fa-user-plus" aria-hidden="true"></i> New
		          	</a>
		          	<button type="button" class="btn btn-danger" onclick="DeleteSales()">
		            	<i class="fa fa-trash" aria-hidden="true"></i> Delete
		          	</button>
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Filter
		          	</span> --}}
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['id'=>'formSalesOrder']) !!}
				<table id="tableSalesOrder" class="table table-striped table-color">
					<thead>
						<tr>
							<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>No.</th>
							<th>Date</th>
	            			<th>Action</th>
						</tr>
					</thead>
					<tbody>
	          			@foreach ($order as $o)
						<tr>
							<td>
								<input type="checkbox" id="idTableSalesOrder" value="{{ $o->id }}" name="id[]" class="checkin">
							</td>
							<td>{{ $o->sales_order_no }}</td>
							<td>{{ date('d F y', strtotime($o->sales_order_date)) }}</td>
							<td>
								<a href="{{ route('sales.order.show', $o->id) }}" class="btn btn-default btn-xs">
									<i class="fa fa-pencil-square"></i> Edit
								</a>
							</td>
						</tr>
						@endforeach
	        		</tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
  <script type="text/javascript">
    var tableSalesOrder = $("#tableSalesOrder").DataTable({
    	"dom": "rtip",
      	"pageLength": 10,
      	"retrieve": true,
      	"stateSave": true,
    });

    $("#searchDtbox").keyup(function() {
          tableSalesOrder.search($(this).val()).draw() ;
    });  

    function DeleteSales() {
      $('#formSalesOrder').submit();
    }
  </script>
@stop