<script>

 getChart()

 function getChart(){
      var label = [];
      var data = [];

      var dataChart = $.ajax({
        url: '{{ route('api.web.inventory.chart.purchasing') }}/?invent='+invent+'&begin='+begin+'&end='+end,
        type: 'GET',
        success: function(response){
          $.each(response, function(key, value){
            var date_value = moment(value.DATE).format('DD')
            var total_qty = value.total_qty;

            label.push(date_value)
            data.push(value.total_qty)
          })

          var barChartData = {
          labels: label,
          datasets: [{
              label: 'Bill',
              fill: false,
              backgroundColor: '#c0392b',
              borderColor: '#c0392b',
              borderWidth: 1,
              data: data
                }]
          }
          var ctx = document.getElementById('barChart').getContext('2d');
          myBar = new Chart(ctx, 
              {
              type: 'bar',
              data: barChartData,
              options: {  
                responsive: true, 
                legend: {
                  position: 'top',
                },
                title: {
                  display: true,
                  text: 'Inventory Purchasing '
                },
                tooltips: {
                    mode: 'label',
                    label: 'mylabel',
                    callbacks: {
                      label: function(tooltipItem, data) {
                        return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
                      }, 
                    },
                },
              }
            });
        }
      })
    }
</script>