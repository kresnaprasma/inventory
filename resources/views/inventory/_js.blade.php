<script type="text/javascript">
	getConvert();
	getPrice();
	//getDiscount();
	getQuantity();
	selectizeme();

	var options = $('select[name=category_id]').find('option');
	$.each(options, function(i, opt){
		if ($(opt).hasClass('newSelect'))
  		{
   	 		$(opt).addClass('add-new-select');
  		}
	});

	$("select[name=category_id]").on('change',function(){
		if(this.value == 'new'){
			$('#createCategoryModal').modal('show');
		}
		else if(this.value == ''){
			$('#inventory_code').val(' ');
		}
		else{
			// updateQueryStringParam('category', this.value);
			$.ajax({
				url: '/api/web/inventory/no/'+this.value,
				type: 'GET',
				success: function(response){
					$('#inventory_code').val(response.data);
				},
				error: function(response){
					console.log(response);
				}
			})
		}		
	});

	function selectizeme() {
		$('#merk').selectize({});
	}

    $('select[id="merk"]').on('change', function() {
        var merkID = $(this).val();
        if(merkID) {
            $.ajax({
                url: 'http://localhost:8000/api/web/inventory/type/'+merkID,
                type: "GET",
                dataType: "json",
                success:function(data) {
					$('select[id="type"]').empty();                	
                    $.each(data, function(key, value) {
                        $('select[id="type"]').append('<option value="'+ key +'">'+ value +'</option>');
					});
				}
			});
		}else{
			$('select[id="type"]').empty();
		}
	});

	$('#purchasing_date_unit').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	$('#purchasing_date_unit_convert').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});


	$('#selling_date_unit').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	$('#selling_date_unit_convert').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	$('#distribution_date_unit').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	$('#distribution_date_unit_convert').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	function saveCategory(){
		var category_id = $('#category_modal_id').val();
		var category_name = $('#category_modal_name').val();

		$.ajax({
			url: '{{ route('api.web.inventory.category.store') }}',
			type: 'POST',
			data: {
				'id': category_id,
				'name': category_name
			},
			success: function(response){
					var id = response.data.category_id;
					var name = response.data.category_name;
					$('select[name=category_id]')
						.append($("<option></option>")
                    	.attr("value",category_id)
                    	.text(name));
                    $('#createCategoryModal').modal('hide');
			},
			error: function(response){
				// var res = JSON.stringify(response.message.id);
				console.log(response.responseJSON);
				if(response.responseJSON){
					$.each(response.responseJSON.message, function(key, value){
						$("#category_modal_"+key).parent().addClass('has-error');
						$("#category_modal_"+key).after("<span class='help-block'><strong>"+value+"</strong></span>");
					});
				}else{
					return false;
				}
			}
		});
	}

	function save() {
		$('#formInventory').submit();
	}

	function newConvert(){
		$("#createConvertModal").modal("show");
	}

	function newPrice(){
		$("#createPriceModal").modal('show');
	}

	function newDiscount(){
		$("#createDiscountModal").modal('show');	
	}

	function newQuantity() {
		$("#createQuantityModal").modal('show');	
	}

	function saveConvert(){
		var inventory_code = $('#inventory_code').val();
		$.ajax({
			url: '/api/web/inventory/convert',
			type: 'POST',
			data: {
				'inventory_code': inventory_code,
				'unit': $('#convert_modal_unit').val(),
				'size': $('#convert_modal_size').val(),
				'convert_type': $('#convert_modal_type').val(),
			},
			success: function(response){
				getConvert();	
			},
			error: function(response){
				console.log(response);
			}
		});
	}

	function savePrice(){
		var inventory_code = $('#inventory_code').val();
		$.ajax({
			url: '/api/web/inventory/price',
			type: 'POST',
			data: {
				'inventory_code': inventory_code,
				'price': $('#price_modal_price').val(),
				'price_date': $('#price_modal_date').val(),
			},
			success: function(response){
				getPrice();	
			},
			error: function(response){
				console.log(response);
			}
		});
	}

	function saveDiscount(){
		var inventory_code = $('#inventory_code').val();
		$.ajax({
			url: '/api/web/inventory/discount',
			type: 'POST',
			data: {
				'inventory_code': inventory_code,
				'discount': $('#discount_modal_discount').val(),
			},
			success: function(response){
				getDiscount();	
			},
			error: function(response){
				console.log(response);
			}
		});
	}

	function saveQuantity(){
		var inventory_code = $('#inventory_code').val();

		$.ajax({
			url: '/api/web/inventory/quantity',
			type: 'POST',
			data: {
				'inventory_code': inventory_code,
				'qty_min': $('#qty_min_modal_qty').val(),
				'qty_max': $('#qty_max_modal_qty').val(),
			},
			success: function(response){
				getQuantity();	
			},
			error: function(response){
				console.log(response);
			}
		});
	}

	function getConvert(){
		var inventory_code = $('#inventory_code').val();
		$("#tableConvert tbody").empty();
		$.ajax({
			url: '/api/web/inventory/convert/'+inventory_code,
			type: 'GET',
			success: function(response){
				var content = "";
				var no = 0;
				var type  = "";
				$.each(response, function(key, value){
					no = no + 1;
					if(value.convert_type == '<'){
						type = 'Lesser <';
					}else{
						type = 'Greater';
					}
					$("#tableConvert tbody").append("<tr><td>"+no+"</td><td>"+value.unit+"</td><td>"+value.size+"</td><td>"+type+"</td></tr>");
				});
				$("#createConvertModal").modal("hide");
			},
			error: function(response){
				console.log(response);
			}
		})
	}

	function getPrice(){
		var inventory_code = $('#inventory_code').val();
		// var outlet = '';
		$("#tablePrice tbody").empty();
		$.ajax({
			url: '/api/web/inventory/price/'+inventory_code/*+'?outlet='+outlet*/,
			type: 'GET',
			success: function(response){
				var content = "";
				var no = 0;
				$.each(response, function(key, value){
					no = no + 1;
					$("#tablePrice tbody").append("<tr><td>"+no+"</td><td>"+value.price+"</td><td>"+value.price_type+"</td><td>"+value.price_unit+"</td><td>"+value.price_date+"</td><td><a href='#' class='btn btn-default btn-xs'>Edit</a></td></tr>");
				});
				$("#createPriceModal").modal("hide");
			},
			error: function(response){
				console.log(response);
			}
		})
	}

	// function getDiscount(){
	// 	var inventory_code = $('#inventory_code').val();
	// 	$("#tableDiscount tbody").empty();
	// 	$.ajax({
	// 		url: '/api/web/inventory/discount/'+inventory_code,
	// 		type: 'GET',
	// 		success: function(response){
	// 			var content = "";
	// 			var no = 0;
	// 			$.each(response, function(key, value){
	// 				no = no + 1;
	// 				$("#tableDiscount tbody").append("<tr><td>"+no+"</td><td>"+value.discount+"</td><td>"+value.created_at+"</td></tr>");
	// 			});
	// 			$("#createDiscountModal").modal("hide");
	// 		},
	// 		error: function(response){
	// 			console.log(response);
	// 		}
	// 	})
	// }

	function getQuantity(){
		var inventory_code = $('#inventory_code').val();
		$("#tableQuantity tbody").empty();
		$.ajax({
			url: '/api/web/inventory/quantity/'+inventory_code,
			type: 'GET',
			success: function(response){
				var content = "";
				var no = 0;

				$.each(response, function(key, value){
					no = no + 1;
					$("#tableQuantity tbody").append("<tr><td>"+no+"</td><td>"+value.qty_type+"</td><td>"+value.qty+"</td></tr>");
				});
				$("#createQuantityModal").modal("hide");
			},
			error: function(response){
				console.log(response);
			}
		})
	}

</script>