@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Colorant
        <small>Colorant Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="{{ route('inventory.index') }}">Inventory</a></li>
        <li><a href="{{ route('inventory.colorant.index') }}">Colorant</a></li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				   <h3 class="box-title">Colorant</h3>
				   <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				   </div>
				</div>

				<div class="box-body">
					<div class="col-md-12 box-body-header">
					   <div class="col-md-8">
					      <a href="{{ route('inventory.colorant.create') }}" class="btn btn-default">
					      	<i class="fa fa-plus" aria-hidden="true"></i> New
					      </a>
					      <button type="button" class="btn btn-default" onclick="deleteColorant()">
					      <i class="fa fa-times" aria-hidden="true"></i> Delete
					      </button>
					   </div>
					   <div class="col-md-4">
					      <input type="text" id="searchDtbox" class="form-control" placeholder="Search">
				   		</div>
					</div>

					{{-- {!! Form::open(['route'=>'inventory.colorant.delete', 'id'=>'formDeleteColorant']) !!} --}}
					<div>
						
					<table class="table table-bordered table-striped table-color" id="tableColorant">
						<thead>
							<th><input type="checkbox" id="check_all"></th>
							<th>Colorant Code</th>
							<th>Name</th>
							<th>Merk</th>
							<th>Unit</th>
							<th>Capacity</th>
							<th>Min. Capacity</th>
							<th>Max. Capacity</th>
							<th>Action</th>
						</thead>
						<tbody>
							@foreach($colorant as $c)
								<tr>
									<td>
										<input type="checkbox" id="idTableColorant" name="id[]" class="checkin" value="{{ $c->id }}">
									</td>
									<td><b>{{ $c->colorant_code }}</b></td>
									<td>{{ $c->name }}</td>
									<td>{{ $c->merks->name }}</td>
									<td>{{ $c->unit_stock }}</td>
									<td>{{ $c->capacity }}</td>
									<td>{{ $c->min_capacity }}</td>
									<td>{{ $c->max_capacity }}</td>
									<td>
										<a href="{{ route('inventory.colorant.edit', $c->id) }}" class="btn btn-default btn-xs">
											<i class="fa fa-pencil-square"></i> Edit
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! Form::close() !!}
					</div><!-- box body -->
				</div>
			</div>
		</div>
	</div>

	@include('master.supplier._modal')
@stop

@section('scripts')
	<script type="text/javascript">
		var tableColorant = $('#tableColorant').DataTable({
			// "sDom": 'rt',
   //    		"columnDefs": [{
   //      		"targets": [],
   //      		// "orderable": false,

   //    		}],
   //    		"stateSave": true,
   		"dom": "rtip",
          "pageLength": 10,
          "retrieve": true,
		});
		$("#searchDtbox").keyup(function() {
      		tableColorant.search($(this).val()).draw();
    	});
    	$('#tableColorant tbody').on('dblclick', 'tr', function () {
      		if ( $(this).hasClass('selected') ) {
        		$(this).removeClass('selected');
      		}
	      	else {
	        	tableColorant.$('tr.selected').removeClass('selected');
	        	$(this).addClass('selected');
	        	var id = $(this).find('#idTableSupplier').val();
	          	window.location.href = "/inventory/colorant/"+id+"/edit";
	      	}
    	});
    	function deleteColorant() {
			if ($('.checkin').is(':checked')) 
			{
				$('#deleteColorantModal').modal("show");
			} else {
				$('#deleteNoModal').modal("show");
				}
		}
		function DeleteColorant() {
			$("#formDeleteColorant").submit();
		}
	</script>
@stop