@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Colorant
        <small>Colorant Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="{{ route('inventory.colorant.index') }}">Colorant</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
  	{!! Form::model($colorant = new \App\Colorant, ['route' => 'inventory.colorant.store','id'=>'formColorant']) !!}
    	@include('inventory.colorant._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
  @include('inventory.colorant._modal')
@stop

@section('script')
  @include('inventory.colorant._js')
@stop