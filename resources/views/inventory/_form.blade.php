<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab">Data</a></li>
    @if ($edit)
    <li><a href="#tab_2" data-toggle="tab">Pricing</a></li>
    <li><a href="#tab_3" data-toggle="tab">Convert</a></li>
    <li><a href="#tab_4" data-toggle="tab">Quantity</a></li>
    @endif
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <div class="row">
        <div class="col-md-4">
          
          <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            {!! Form::label('category_id', "Category", ['class'=>'control-label']) !!}
            @if ($edit)
            <select class="form-control" id="category_id" name="category_id" disabled="disabled">
            @else
            <select class="form-control" id="category_id" name="category_id">
            @endif
                <option value="">---</option>
                <option value="new" class="newSelect">+ Add new</option>
              @foreach ($category_list as $key=>$value)
                @if ($key == $invent->category_id)
                  <option value="{{ $key }}" selected="selected" readonly>{{ $value }}</option>
                @else 
                  <option value="{{ $key }}">{{ $value }}</option>
                @endif
              @endforeach
            </select>
            @if ($errors->has('category_id'))
              <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::label('merk', 'Merk:') !!}
            <select name="merk" id="merk">
              <option value="">--- Select Merk ---</option>
              @foreach ($merk_list as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            {!! Form::label('type_id', 'Type:') !!}
            <select name="type" id="type" class="form-control">
              <option value="">--- Select Type ---</option>
            </select>
          </div>

          <div class="form-group{{ $errors->has('inventory_code') ? ' has-error' : '' }}">
            {!! Form::label('inventory_code', "Inventory No.", ['class'=>'control-label']) !!}
            {!! Form::text('inventory_code', null, ['class'=>'form-control','required','id'=>'inventory_code']) !!}
            @if ($errors->has('inventory_code'))
              <span class="help-block">
                <strong>{{ $errors->first('inventory_code') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('inventory_name') ? ' has-error' : '' }}">
            {!! Form::label('inventory_name', "Name*", ['class'=>'control-label']) !!}
            {!! Form::text('inventory_name', old('inventory_name'), ['class'=>'form-control','required','autofocus']) !!}
            @if ($errors->has('inventory_name'))
            <span class="help-block">
              <strong>{{ $errors->first('inventory_name') }}</strong>
            </span>
            @endif
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group{{ $errors->has('stock_unit') ? ' has-error' : '' }}">
            {!! Form::label('stock_unit', "Satuan Terkecil*", ['class'=>'control-label']) !!}
            {!! Form::text('stock_unit', old('stock_unit'), ['class'=>'form-control','required','autofocus']) !!}
            @if ($errors->has('stock_unit'))
              <span class="help-block">
                <strong>{{ $errors->first('stock_unit') }}</strong>
              </span>
            @endif
          </div>
          
          <div class="form-group{{ $errors->has('convert_unit') ? ' has-error' : '' }}">
            {!! Form::label('convert_unit', 'Satuan Kemasan') !!}
            @if ($invent->converts->count() > 0)
              {!! Form::text('convert_unit', $invent->converts[0]->unit,['class'=>'form-control', 'id'=>'convert_modal_unit']) !!}
            @else 
              {!! Form::text('convert_unit', null,['class'=>'form-control', 'id'=>'convert_modal_unit']) !!}
            @endif

            @if ($errors->has('convert_unit'))
              <span class="help-block">
                <strong>{{ $errors->first('convert_unit') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('convert_size') ? ' has-error' : '' }}">
            {!! Form::label('convert_size', 'Angka Konversi') !!}
            @if ($invent->converts->count() > 0)
              {!! Form::text('convert_size', $invent->converts[0]->size,['class'=>'form-control','id'=>'convert_modal_size']) !!}
            @else
              {!! Form::text('convert_size', null,['class'=>'form-control','id'=>'convert_modal_size']) !!}
            @endif
            @if ($errors->has('convert_size'))
              <span class="help-block">
                <strong>{{ $errors->first('convert_size') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('stock_minimum') ? ' has-error' : '' }}">
            {!! Form::label('stock_minimum', 'Minimum Quantity') !!}
            {!! Form::text('stock_minimum', $qty_min,['class'=>'form-control', 'id'=>'qty_min_modal_qty']) !!}
            @if ($errors->has('stock_minimum'))
              <span class="help-block">
                <strong>{{ $errors->first('stock_minimum') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('stock_maximum') ? ' has-error' : '' }}">
            {!! Form::label('stock_maximum', 'Maximum Quantity') !!}
            {!! Form::text('stock_maximum', $qty_max,['class'=>'form-control', 'id'=>'qty_max_modal_qty']) !!}
            @if ($errors->has('stock_maximum'))
              <span class="help-block">
                <strong>{{ $errors->first('stock_maximum') }}</strong>
              </span>
            @endif
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group{{ $errors->has('barcode') ? ' has-error' : '' }}">
            {!! Form::label('barcode', "Barcode", ['class'=>'control-label']) !!}
            {!! Form::text('barcode', old('barcode'), ['class'=>'form-control','required']) !!}
            @if ($errors->has('barcode'))
            <span class="help-block">
            <strong>{{ $errors->first('barcode') }}</strong>
            </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('stock_location') ? ' has-error' : '' }}">
            {!! Form::label('stock_location', "Location", ['class'=>'control-label']) !!}
            {!! Form::text('stock_location', old('stock_location'), ['class'=>'form-control','required']) !!}
            @if ($errors->has('stock_location'))
              <span class="help-block">
                <strong>{{ $errors->first('stock_location') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('stock_description') ? ' has-error' : '' }}">
            {!! Form::label('stock_description', "Description", ['class'=>'control-label']) !!}
            {!! Form::textarea('stock_description', old('stock_description'), ['class'=>'form-control','rows'=>5]) !!}
            @if ($errors->has('stock_description'))
            <span class="help-block">
            <strong>{{ $errors->first('stock_description') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>
      <!-- /.row -->
      <hr>
      {{-- Pricing Form --}}
      <div class="row">
        <div class="col-md-12">
          <h4>Pricing</h4>
        </div>

        {{-- Purchasing Price --}}
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('purchasing_price_unit') ? ' has-error' : '' }}">
            {!! Form::label('purchasing_price_unit', "Harga Beli - Satuan Terkecil", ['class'=>'control-label']) !!}
            {!! Form::text('purchasing_price_unit', $purchasing_price, ['class'=>'form-control','required']) !!}
            @if ($errors->has('purchasing_price_unit'))
              <span class="help-block">
                <strong>{{ $errors->first('purchasing_price_unit') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('purchasing_date_unit') ? ' has-error' : '' }}">
            {!! Form::label('purchasing_date_unit', "Date", ['class'=>'control-label']) !!}
            <div class='input-group date' id="purchasing_date_unit">
                {!! Form::text('purchasing_date_unit', $purchasing_date, ['class'=>'form-control','required']) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            @if ($errors->has('purchasing_date_unit'))
              <span class="help-block">
                <strong>{{ $errors->first('purchasing_date_unit') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('purchasing_price_unit_convert') ? ' has-error' : '' }}">
            {!! Form::label('purchasing_price_unit_convert', "Harga Beli - Kemasan", ['class'=>'control-label']) !!}
            {!! Form::text('purchasing_price_unit_convert', $purchasing_price_convert, ['class'=>'form-control','required']) !!}
            @if ($errors->has('purchasing_price_unit_convert'))
              <span class="help-block">
                <strong>{{ $errors->first('purchasing_price_unit_convert') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('purchasing_date_unit_convert') ? ' has-error' : '' }}">
            {!! Form::label('purchasing_date_unit_convert', "Date", ['class'=>'control-label']) !!}
            <div class='input-group date' id="purchasing_date_unit_convert">
                {!! Form::text('purchasing_date_unit_convert', $purchasing_date_convert, ['class'=>'form-control','required']) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            @if ($errors->has('purchasing_date_unit_convert'))
              <span class="help-block">
                <strong>{{ $errors->first('purchasing_date_unit_convert') }}</strong>
              </span>
            @endif
          </div>
        </div>

        {{-- Selling Price --}}
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('selling_price_unit') ? ' has-error' : '' }}">
            {!! Form::label('selling_price_unit', "Harga Jual - Satuan Terkecil", ['class'=>'control-label']) !!}
            {!! Form::text('selling_price_unit', $selling_price, ['class'=>'form-control','required']) !!}
            @if ($errors->has('selling_price_unit'))
              <span class="help-block">
                <strong>{{ $errors->first('selling_price_unit') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('selling_date_unit') ? ' has-error' : '' }}">
            {!! Form::label('selling_date_unit', "Date", ['class'=>'control-label']) !!}
            <div class='input-group date' id="selling_date_unit">
                {!! Form::text('selling_date_unit', $selling_date, ['class'=>'form-control','required']) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            @if ($errors->has('selling_date'))
              <span class="help-block">
                <strong>{{ $errors->first('selling_date_unit') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('selling_price_unit_convert') ? ' has-error' : '' }}">
            {!! Form::label('selling_price_unit_convert', "Harga Jual - Kemasan", ['class'=>'control-label']) !!}
            {!! Form::text('selling_price_unit_convert', $selling_price_convert, ['class'=>'form-control','required']) !!}
            @if ($errors->has('selling_price_unit_convert'))
              <span class="help-block">
                <strong>{{ $errors->first('selling_price_unit_convert') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('selling_date_unit_convert') ? ' has-error' : '' }}">
            {!! Form::label('selling_date_unit_convert', "Date", ['class'=>'control-label']) !!}
            <div class='input-group date' id="selling_date_unit_convert">
                {!! Form::text('selling_date_unit_convert', $selling_date_convert, ['class'=>'form-control','required']) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            @if ($errors->has('selling_date'))
              <span class="help-block">
                <strong>{{ $errors->first('selling_date_unit_convert') }}</strong>
              </span>
            @endif
          </div>
        </div>

        {{-- Distribution Price --}}
        <div class="col-md-4">
          <div class="form-group{{ $errors->has('distribution_price_unit') ? ' has-error' : '' }}">
            {!! Form::label('distribution_price_unit', "Harga Distribusi - Satuan Terkecil", ['class'=>'control-label']) !!}
            {!! Form::text('distribution_price_unit', $distribution_price, ['class'=>'form-control','required']) !!}
            @if ($errors->has('distribution_price_unit'))
              <span class="help-block">
                <strong>{{ $errors->first('distribution_price_unit') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('distribution_date_unit') ? ' has-error' : '' }}">
            {!! Form::label('distribution_date_unit', "Date", ['class'=>'control-label']) !!}
            <div class='input-group date' id="distribution_date_unit">
                {!! Form::text('distribution_date_unit', $distribution_date, ['class'=>'form-control','required']) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            @if ($errors->has('distribution_date'))
              <span class="help-block">
                <strong>{{ $errors->first('distribution_date_unit') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('distribution_price_unit_convert') ? ' has-error' : '' }}">
            {!! Form::label('distribution_price_unit_convert', "Harga Distribusi - Kemasan", ['class'=>'control-label']) !!}
            {!! Form::text('distribution_price_unit_convert', $distribution_price_convert, ['class'=>'form-control','required']) !!}
            @if ($errors->has('distribution_price_unit_convert'))
              <span class="help-block">
                <strong>{{ $errors->first('distribution_price_unit_convert') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('distribution_date_unit_convert') ? ' has-error' : '' }}">
            {!! Form::label('distribution_date_unit_convert', "Date", ['class'=>'control-label']) !!}
            <div class='input-group date' id="distribution_date_unit_convert">
                {!! Form::text('distribution_date_unit_convert', $distribution_date_convert, ['class'=>'form-control','required']) !!}
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            @if ($errors->has('distribution_date_unit_convert'))
              <span class="help-block">
                <strong>{{ $errors->first('distribution_date_unit_convert') }}</strong>
              </span>
            @endif
          </div>
        </div>
      </div>
      {{-- /.end Pricing--}}
    </div>
    <!-- /.tab-pane -->
    <div class="tab-pane" id="tab_2">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped table-color" id="tablePrice">
                <thead>
                  <th>No</th>
                  <th>Price</th>
                  <th>Price Category</th>
                  <th>Price Unit</th>
                  <th>Date</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.tab-pane -->
    <div class="tab-pane" id="tab_3">
      <div class="row">
        <div class="col-md-12">  
            <div class="col-md-8 box-">
              <button class="btn btn-primary" onclick="newConvert()">
                  <i class="fa fa-plus" aria-hidden="true"></i> New
              </button>
            </div>

            {{-- <div class="col-md-4">
                <input type="text" id="searchDtbox" class="form-control" placeholder="search...">
            </div> --}}
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-color" id="tableConvert">
            <thead>
              <th>No</th>
              <th>unit</th>
              <th>size</th>
              <th>Type</th>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /.tab-pane -->
    <div class="tab-pane" id="tab_4">
      <div class="row">
        <div class="col-md-12">  
            <div class="col-md-8 box-">
              <button class="btn btn-primary" onclick="newQuantity()">
                  <i class="fa fa-plus" aria-hidden="true"></i> New
              </button>
            </div>

            {{-- <div class="col-md-4">
                <input type="text" id="searchDtbox" class="form-control" placeholder="search...">
            </div> --}}
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-color" id="tableQuantity">
            <thead>
              <th>No</th>
              <th>Type</th>
              <th>Qty</th>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /.tab-pane -->

  </div>
  <!-- /.tab-content -->
</div>

<button type="button" class="btn btn-primary" onclick="save()">
  <i class="fa fa-save"></i> Save
</button>
