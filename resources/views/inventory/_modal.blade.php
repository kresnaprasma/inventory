{{-- Create Modal --}}
<div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="Create Bank">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Create Stock Category</h4>
			</div>
			{!! Form::open(['route'=> 'api.web.inventory.category.store']) !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="alert-modal">
							
						</div>

						<div class="form-group">
							{!! Form::label('id', 'Id:') !!}
							{!! Form::text('id', null,['class'=>'form-control', 'id'=>'category_modal_id','maxlength'=>1,'style'=>"text-transform:uppercase"]) !!}
						</div>
						<div class="form-group">
							{!! Form::label('name', 'Name') !!}
							{!! Form::text('name', null,['class'=>'form-control','id'=>'category_modal_name']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveCategory()" class="btn btn-red">Create</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

{{-- Create Convert Modal --}}
<div class="modal fade" id="createConvertModal" tabindex="-1" role="dialog" aria-labelledby="Create Bank">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Convert | Original Unit: {{ $invent->stock_unit }}</h4>
			</div>
			{!! Form::open(['route'=> 'api.web.inventory.category.store']) !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="alert-modal">
							
						</div>

						<div class="form-group">
							{!! Form::label('unit', 'Convert To Unit:') !!}
							{!! Form::text('unit', null,['class'=>'form-control', 'id'=>'convert_modal_unit']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('convert_type','Convert from original size:') !!}
							{!! Form::select('convert_type',['<'=>'Lesser <','>'=>'Greater >'], null,['class'=>'form-control','id'=>'convert_modal_type']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('size', 'Size:') !!}
							{!! Form::text('size', null,['class'=>'form-control','id'=>'convert_modal_size']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveConvert()" class="btn btn-red">Create</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

{{-- Create Price Modal --}}
<div class="modal fade" id="createPriceModal" tabindex="-1" role="dialog" aria-labelledby="Create Bank">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Create Stock Price</h4>
			</div>
			{!! Form::open(['route'=> 'api.web.inventory.price.store']) !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="alert-modal">
							
						</div>

						<div class="form-group">
							{!! Form::label('unit', 'Price:') !!}
							{!! Form::text('unit', null,['class'=>'form-control', 'id'=>'price_modal_price']) !!}
						</div>
						{{-- <div class="form-group">
							{!! Form::label('outlet', 'Outlet:') !!}
							<select name="outlet" class="form-control">
								@foreach ($outlet_list as $o)
									<option value="{{ $o->outlet_code }}">{{ $o->name }}</option>
								@endforeach
							</select>
						</div> --}}

						<div class="form-group">
							{!! Form::label('price_date', 'Date:') !!}
							{!! Form::text('price_date', null,['class'=>'form-control', 'id'=>'price_modal_date']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="savePrice()" class="btn btn-red">Create</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

{{-- Create Discount Modal --}}
<div class="modal fade" id="createDiscountModal" tabindex="-1" role="dialog" aria-labelledby="Create Bank">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Create Stock Discount</h4>
			</div>
			{!! Form::open(['route'=> 'api.web.inventory.discount.store']) !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="alert-modal">
							
						</div>

						<div class="form-group">
							{!! Form::label('discount', 'Discount:') !!}
							{!! Form::text('discount', null,['class'=>'form-control', 'id'=>'discount_modal_discount']) !!}
						</div>
						{{-- <div class="form-group">
							{!! Form::label('outlet', 'Outlet') !!}
							{!! Form::select('outlet', old('outlet'), null,['class'=>'form-control','id'=>'outlet_modal_discount']) !!}
						</div> --}}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveDiscount()" class="btn btn-red">Create</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

{{-- Create Quantity Modal --}}
{{-- <div class="modal fade" id="createQuantityModal" tabindex="-1" role="dialog" aria-labelledby="Create Quantity">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Create Stock Quantity </h4>
			</div>
			{!! Form::open(['route'=> 'api.web.stock.quantity.store']) !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="alert-modal">
							
						</div>

						<div class="form-group">
							{!! Form::label('qty_min', 'Min:') !!}
							{!! Form::text('qty_min', null,['class'=>'form-control', 'id'=>'qty_min_modal_qty']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('qty_max', 'Max:') !!}
							{!! Form::text('qty_max', null,['class'=>'form-control', 'id'=>'qty_max_modal_qty']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveQuantity()" class="btn btn-red">Create</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> --}}