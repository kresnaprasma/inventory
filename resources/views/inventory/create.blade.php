@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Inventory
        <small>Inventory Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="{{ route('inventory.index') }}">Inventory</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
  	{!! Form::model($invent = new \App\Inventory, ['route' => 'inventory.store','id'=>'formInventory']) !!}
    	@include('inventory._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
  @include('inventory._modal')
@stop

@section('script')
  @include('inventory._js')
@stop