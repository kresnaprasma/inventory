@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Paint
        <small>Paint Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="{{ route('inventory.index') }}">Inventory</a></li>
        <li><a href="{{ route('inventory.paint.index') }}">Paint</a></li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				   <h3 class="box-title">Paint</h3>
				   <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				   </div>
				</div>

				<div class="box-body">
					<div class="col-md-12 box-body-header">
					   <div class="col-md-8">
					      <a href="{{ route('inventory.paint.create') }}" class="btn btn-primary">
					      	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
					      </a>
					      <button type="button" class="btn btn-danger" onclick="deletePaint()">
					      <i class="fa fa-trash" aria-hidden="true"></i> Delete
					      </button>
					   </div>
					   <div class="col-md-4">
					      <input type="text" id="searchDtbox" class="form-control" placeholder="Search">
				   		</div>
					</div>

					{{-- {!! Form::open(['route'=>'inventory.colorant.delete', 'id'=>'formDeleteColorant']) !!} --}}
					<div>
						
					<table class="table table-bordered table-striped table-color" id="tablePaint">
						<thead>
							<th><input type="checkbox" id="check_all"></th>
							<th>Paint Code</th>
							<th>Name</th>
							<th>Merk</th>
							<th>Type</th>
							<th>Stock Unit</th>
							<th>Min. Capacity</th>
							<th>Max. Capacity</th>
							<th>Action</th>
						</thead>
						<tbody>
							@foreach($paint as $p)
								<tr>
									<td>
										<input type="checkbox" id="idTablePaint" name="id[]" class="checkin" value="{{ $p->id }}">
									</td>
									<td><b>{{ $p->paint_code }}</b></td>
									<td>{{ $p->paint_name }}</td>
									<td>{{ $p->merks->name }}</td>
									<td>{{ $p->types->name }}</td>
									<td>{{ $p->stock_unit }}</td>
									<td>{{ $p->min_capacity }}</td>
									<td>{{ $p->max_capacity }}</td>
									<td>
										<a href="{{ route('inventory.paint.edit', $p->id) }}" class="btn btn-default btn-xs">
											<i class="fa fa-pencil-square"></i> Edit
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! Form::close() !!}
					</div><!-- box body -->
				</div>
			</div>
		</div>
	</div>

	@include('inventory.paint._modal')
@stop

@section('scripts')
	<script type="text/javascript">
		var tablePaint = $('#tablePaint').DataTable({
			// "sDom": 'rt',
   //    		"columnDefs": [{
   //      		"targets": [],
   //      		// "orderable": false,

   //    		}],
   //    		"stateSave": true,
   		"dom": "rtip",
          "pageLength": 10,
          "retrieve": true,
		});
		$("#searchDtbox").keyup(function() {
      		tablePaint.search($(this).val()).draw();
    	});
    	$('#tablePaint tbody').on('dblclick', 'tr', function () {
      		if ( $(this).hasClass('selected') ) {
        		$(this).removeClass('selected');
      		}
	      	else {
	        	tablePaint.$('tr.selected').removeClass('selected');
	        	$(this).addClass('selected');
	        	var id = $(this).find('#idTablePaint').val();
	          	window.location.href = "/inventory/paint/"+id+"/edit";
	      	}
    	});
    	function deletePaint() {
			if ($('.checkin').is(':checked')) 
			{
				$('#deletePaintModal').modal("show");
			} else {
				$('#deleteNoModal').modal("show");
				}
		}
		function DeletePaint() {
			$("#formDeletePaint").submit();
		}
	</script>
@stop