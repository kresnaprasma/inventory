@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Paint
        <small>Paint Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="{{ route('inventory.paint.index') }}">Paint</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
  	{!! Form::model($paint = new \App\Paint, ['route' => 'inventory.paint.store','id'=>'formPaint']) !!}
    	@include('inventory.paint._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
  @include('inventory.paint._modal')
@stop

@section('script')
  @include('inventory.paint._js')
@stop