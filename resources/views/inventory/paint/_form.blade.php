<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Paint <b>@if ($edit){{ $paint->paint_code }}@endif</b>
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="form-group{{ $errors->has('paint_code') ? ' has-error' : '' }} col-xs-3 col-md-3">
        {!! Form::label('paint_code', "Paint Code*", ['class'=>'control-label']) !!}
        {!! Form::text('paint_code', old('paint_code'), ['class'=>'form-control','required','autofocus']) !!}
        @if ($errors->has('paint_code'))
            <span class="help-block">
                <strong>{{ $errors->first('paint_code') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group{{ $errors->has('paint_name') ? ' has-error' : '' }} col-xs-3 col-md-3">
        {!! Form::label('paint_name', "Name*", ['class'=>'control-label']) !!}
        {!! Form::text('paint_name', old('paint_name'), ['class'=>'form-control','required','autofocus']) !!}
        @if ($errors->has('paint_name'))
            <span class="help-block">
                <strong>{{ $errors->first('paint_name') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group{{ $errors->has('base_paint') ? ' has-error' : '' }} col-xs-3 col-md-3">
        {!! Form::label('base_paint', "Base Paint*", ['class'=>'control-label']) !!}
        {!! Form::text('base_paint', old('base_paint'), ['class'=>'form-control','required','autofocus']) !!}
        @if ($errors->has('base_paint'))
            <span class="help-block">
                <strong>{{ $errors->first('base_paint') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group{{ $errors->has('paint_date') ? ' has-error' : '' }} col-xs-3 col-md-3">
        {!! Form::label('paint_date', "Date*", ['class'=>'control-label']) !!}
        <div class='input-group date' id='paint_date'>
          {!! Form::text('paint_date',date('m/d/Y'), ['class'=>'form-control']) !!}
          <span class="input-group-addon input-group-addon-silver">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </span>
        </div>
        @if ($errors->has('paint_date'))
            <span class="help-block">
                <strong>{{ $errors->first('paint_date') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="row">

      <div class="form-group{{ $errors->has('merk') ? ' has-error' : '' }} col-xs-3 col-md-3">
        {!! Form::label('merk', "Merk*", ['class'=>'control-label']) !!}
        {!! Form::select('merk', $merk_list, old('merk'), ['required','autofocus', 'id'=>'merkpaint', 'placeholder'=> '--- Select Merk ---']) !!}
        @if ($errors->has('merk'))
          <span class="help-block">
            <strong>{{ $errors->first('merk') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group col-xs-3 col-md-3">
        {!! Form::label('type', 'Type:') !!}
        <select name="type" id="type" class="form-control">
          <option value="">--- Select Type ---</option>
        </select>
      </div>
    </div>
    <!-- /.row -->
    <hr>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-color" id="tableDetailPaint">
            <thead>
              <th>Colorant Code</th>
              <th>Qty</th>
              <th>Price</th>
              <th>Total</th>
              <th>&nbsp;</th>
            </thead>
            <tbody>
              @for ($i = 1; $i <= 2; $i++)
              <tr>
                <td class="col-md-4">
                  <select name="colorant_code[]" class="detail-paint colorant_code" id="colorant_code{{ $i }}">
                    <option value="">-- Colorant --</option>
                    @foreach ($colorants as $ss)
                      <option value="{{ $ss->colorant_code }}">{{ $ss->merks->name }} {{ $ss->colorant_code }} - {{ $ss->name }}</option>
                    @endforeach
                  </select>
                </td>
                <td>
                  {!! Form::text('detail_qty[]',null, ['class'=>'form-control detail_qty detail-table','id'=>'detail_qty'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                <td>{!! Form::text('detail_amount[]', null, ['class'=>'form-control detail_amount detail-table','id'=>'detail_amount'.$i,'onkeyup'=>'number(this)','onkeypress'=>'number(this)']) !!}</td>
                <td>{!! Form::text('detail_total[]', 0, ['class'=>'form-control detail_total detail-table','id'=>'detail_total'.$i,'readonly']) !!}</td>
                <td>
                  {!! Form::hidden('detail_id[]', null, ['id'=>'detail_id'.$i,'class'=>'detail_id detail-table']) !!}
                    <a href="javascript:void(0)" class="del_rincian_create btn btn-danger btn-xs">
                      <i class="fa fa-close" aria-hidden="true"></i>
                    </a>
                </td>
              </tr>
              @endfor
            </tbody>
            
            <tfoot>
              <tr>
                <td></td>
                <td></td>
                <td><b>Total</b></td>
                <td>
                  {!! Form::text('paint_price',null,['class'=>'form-control form-total','id'=>'paint_price','onkeyup'=>'numberDisc(this)','onkeypress'=>'numberDisc(this)']) !!}
                </td>
              </tr>
            </tfoot>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">

        <button type="button" class="btn btn-primary" onclick="addrow()"><i class="fa fa-plus-circle"></i> Add Row</button>
        
        <button type="button" class="btn btn-flat btn-danger pull-right">
          <i class="fa  fa-ban" aria-hidden="true"></i> Cancel
        </button>
        &nbsp;
        <button type="button" class="btn btn-flat btn-primary pull-right" onclick="savePaint()">
          <i class="fa fa-check-square" aria-hidden="true"></i> Save
        </button>
      </div>
  </div>
</div>
<!-- /.box -->
@include('purchasing._modal')