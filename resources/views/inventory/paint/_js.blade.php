<script type="text/javascript">
	selectizeme();
	selectizemerk();
	
	$('#price_date').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	function selectizeme() {
		$('.colorant_code').selectize({});
	}

	function selectizemerk() {
		$('#merkpaint').selectize({});
	}

	$('select[id="merkpaint"]').on('change', function() {
        var merkID = $(this).val();
        if(merkID) {
            $.ajax({
                url: 'http://localhost:8000/api/web/paint/type/'+merkID,
                type: "GET",
                dataType: "json",
                success:function(data) {
					$('select[id="type"]').empty();                	
                    $.each(data, function(key, value) {
                        $('select[id="type"]').append('<option value="'+ key +'">'+ value +'</option>');
					});
				}
			});
		}else{
			$('select[id="type"]').empty();
		}
	});

	function addrow()
	{
		$('.colorant_code').each(function(){ // do this for every select with the 'combobox' class
			if ($(this)[0].selectize) { // requires [0] to select the proper object
				var value = $(this).val(); // store the current value of the select/input
				$(this)[0].selectize.destroy(); // destroys selectize()
				$(this).val(value);  // set back the value of the select/input
			}
		});

    	var lastRow = $('#'+'tableDetailPaint' + " tbody tr:last");
    	var newRow = lastRow.clone(true);
	    //append row to this table
	    $('#tableDetailPaint').append(newRow);

	    // clear input
	    $("#tableDetailPaint tbody tr:last .detail-table").val('');
	   
	    var this_id = $("#tableDetailPaint tbody tr:last  .colorant_code").attr('id');
	    var this_id1 = $("#tableDetailPaint tbody tr:last .detail_qty").attr('id');
	    var this_id2 = $("#tableDetailPaint tbody tr:last  .detail_price").attr('id');
	    var this_id3 = $("#tableDetailPaint tbody tr:last  .detail_total").attr('id');

	    $("#tableDetailPaint tbody tr:last td .colorant_code").attr('id', this_id + 1);
	    $("#tableDetailPaint tbody tr:last td .detail_qty").attr('id', this_id1 + 1);
	    $("#tableDetailPaint tbody tr:last td .detail_price").attr('id', this_id2 + 1);
	    $("#tableDetailPaint tbody tr:last td .detail_total").attr('id', this_id3 + 1);

	    selectizeme();
	}

	function number(this_id){
		// formatNumber(this_id);
		console.log($(this_id).val());
		var thisTableId = $(this_id).parents("table tbody").attr("id");

		var id = $(this_id).closest('tr').find('.detail_qty').attr('id');		
		var id2 = $(this_id).closest('tr').find('.detail_price').attr('id');
		var id3 = $(this_id).closest('tr').find('.detail_total').attr('id');
		
		var id_val = $("#"+id).val();

		var id_val2 = $("#"+id2).val();

		var total = id_val * id_val2;
		
		$("#"+id3).val(total);

		total_promo();
	}

	function total_promo() {
		var sum = 0;
		$('#tableDetailMenu').find('.detail_total').each(function(){
		    var value = $(this).val();
		    if (!isNaN(value)) sum += parseInt(value);
		});

		$('#price').val(sum);
		$('#total_price_menu').val(sum);
	}

	function savePaint(){
		$('#formPaint').submit();
	}
</script>