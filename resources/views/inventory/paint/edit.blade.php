@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Paint
        <small>Paint Manajemen</small>
      </h1>
      <div style="margin-top: 10px">
        <button type="button" class="btn btn-primary" id="reportrange">
          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
          <span>
            @if ($begin)
              {{ $begin->format('M d, Y') }}
              -
              {{  $end->format('M d, Y') }}
            @endif
          </span>
          <b class="caret"></b>
        </button>
      </div>
      <ol class="breadcrumb">
      	<li><a href="{{ route('inventory.paint.index') }}">Paint</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
  	{!! Form::model($paint, ['route' => ['inventory.paint.update', $paint->id],'id'=>'formPaint','method'=>'PATCH']) !!}
      <div class="row">
        {{-- <div class="box box-default">
          <div class="box-header"></div>

          <div class="box-body">
            <div class="chart">
              <canvas id="barChart" style="height:230px"></canvas>
            </div>
          </div>
        </div> --}}
      </div>
      @include('inventory.paint._form',['edit'=>true])
    {!! Form::close() !!}
  </section>
  @include('inventory.paint._modal')
@stop

@section('script')
  @include('inventory._js')
  <script type="text/javascript">
    var begin = '{{ $begin->format('Y-m-d') }}';
    var end = '{{ $end->format('Y-m-d') }}';
    var invent = $('#inventory_code').val();

    
    $('#reportrange').daterangepicker({
      buttonClasses: ['btn', 'btn-sm'],
      applyClass: 'btn-red',
      cancelClass: 'btn-default',
      startDate: '{{ $begin->format('m/d/y') }}',
      endDate: '{{ $end->format('m/d/y') }}',
      locale: {
      applyLabel: 'Submit',
      cancelLabel: 'Cancel',
      fromLabel: 'From',
      toLabel: 'To',
      },
      ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
      }, function(start, end, label){
      console.log(start.toISOString(), end.toISOString(), label);

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      // window.location="{{  request()->url() }}?begin="+ start.format('Y-MM-DD') +"&end=" + end.format('Y-MM-DD');
      updateQueryStringParam('begin', start.format('Y-MM-DD'));
      updateQueryStringParam('end', end.format('Y-MM-DD'));
    });

   
  </script>
@stop