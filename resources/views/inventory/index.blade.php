@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Inventory
        <small>Inventory Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="#">Inventory</a></li>
      </ol>
    </section>
@stop

@section('content')
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Tables Inventory
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
		         	<a href="{{ route('inventory.create') }}" class="btn btn-primary">
		            	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
		          	</a>
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Sort By:
		          	</span>
		          	<button type="button" class="btn btn-primary" id="reportrange">
            			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
            			<span>
                			{{ date('M d, Y', strtotime($date)) }}
            			</span>
            			<b class="caret"></b>
          			</button>
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['id'=>'formInventory']) !!}
				<table id="tableStock" class="table table-striped table-color">
					<thead>
						<tr>
							<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>Inventory No</th>
							<th>Category</th>
	            			<th>Name</th>
	            			<th>Unit</th>
	            			<th>Purchase Price</th>
	            			<th>Sell Price</th>
	            			<th>Stock In</th>
	            			<th>Stock Out</th>
	            			<th>Stock Retur</th>
	            			<th>Action</th>
						</tr>
					</thead>
					<tbody>
	          			@foreach ($inventories as $i)
						<tr>
							<td>
								<input type="checkbox" id="idTableUser" value="{{ $i->id }}" name="id[]" class="checkin">
							</td>
							<td><b>{{ $i->inventory_code }}</b></td>
							<td>{{ $i->category->name }}</td>
							<td>{{ $i->inventory_name }}</td>
							<td>{{ $i->stock_unit }}</td>
							<td>
								@php
									$purchase_price = $i->prices()
										->where('price_type','purchasing')
										->whereDate('price_date','<=', date('Y-m-d', strtotime($date)))
										->orderBy('price_date','desc')
										->first();
								@endphp
								{{ number_format($purchase_price['price']) }}
							</td>
							<td>
								@php
									$sell_price = $i->prices()
										->where('price_type','selling_retail')
										->whereDate('price_date','<=', date('Y-m-d', strtotime($date)))
										->orderBy('price_date','desc')
										->first();
								@endphp
								{{ number_format($sell_price['price']) }}
							</td>
							<td>
								@php
									$stock_in_origin = $i->purchasings()
												->whereDate('detail_date', '<=', date('Y-m-d', strtotime($date)))
												->whereNotNull('detail_date')
												->orderBy('detail_date','desc')
												->where('detail_unit', $i->stock_unit)
												->get();

									$stock_in_convert = $i->purchasings()
												->whereDate('detail_date', '<=', date('Y-m-d', strtotime($date)))
												->whereNotNull('detail_date')
												->orderBy('detail_date','desc')
												->where('detail_unit', '!=',$i->stock_unit)
												->get();

									$stock_in_retur = $i->purchasings()
														->where('detail_date','<=',date('Y-m-d', strtotime($date)))
														->whereNotNull('detail_date')
														->where('retur_qty','!=',null)
														->where('retur_unit', $i->stock_unit)
														->get();
									$stock_in_retur_convert = $i->purchasings()
														->where('detail_date','<=',date('Y-m-d', strtotime($date)))
														->whereNotNull('detail_date')
														->where('retur_qty','!=',null)
														->where('retur_unit', '!=',$i->stock_unit)
														->get();
									
									$stock_convert = $i->converts()->first();

									$sum_stock_in_convert = 0;
									foreach ($stock_in_convert as $p_detail) {
										if($p_detail->detail_unit == $stock_convert['unit']){
											if($stock_convert['convert_type'] == '>'){
												$convert_total = $p_detail->detail_qty * $stock_convert['size'];
												$sum_stock_in_convert += $convert_total;
											}else{
												$convert_total = $p_detail->detail_qty / $stock_convert['size'];
												$sum_stock_in_convert += $convert_total;
											}
										}
									}

									$sum_stock_retur_convert = 0;
									foreach($stock_in_retur_convert as $p_retur){
										if($p_retur->retur_unit == $stock_convert['unit']){
											if ($stock_convert['convert_type'] == '>') {
												$convert_total = $p_retur->retur_qty * $stock_convert['size'];
												$sum_stock_retur_convert += $convert_total;
											}else{
												$convert_total = $p_retur->retur_qty / $stock_convert['size'];
												$sum_stock_retur_convert += $convert_total;
											}
										}
									}

									
									
									$stock_retur = $stock_in_retur->sum('retur_qty') - $sum_stock_retur_convert;
									$stock_in = $stock_in_origin->sum('detail_qty') + $sum_stock_in_convert - $stock_retur;
								@endphp
								<!-- Stock In -->
								{{ number_format($stock_in_origin->sum('detail_qty') + $sum_stock_in_convert) }}
							</td>
							<td>
								@php
									$stock_out_origin = $i->sales()
														->whereDate('detail_date', '<=', date('Y-m-d', strtotime($date)))
														->whereNotNull('detail_date')
														->orderBy('detail_date','desc')
														->where('detail_unit', $i->stock_unit)
														->get();
									
									$stock_out_convert = $i->sales()
												->whereDate('detail_date', '<=', date('Y-m-d', strtotime($date)))
												->whereNotNull('detail_date')
												->orderBy('detail_date','desc')
												->where('detail_unit', '!=',$i->stock_unit)
												->get();
									$stock_convert = $i->converts()->first();

									$sum_stock_out_convert = 0;
									foreach ($stock_out_convert as $p_detail) {
										if($p_detail->detail_unit == $stock_convert['unit']){
											if($stock_convert['convert_type'] == '>'){
												$convert_total = $p_detail->detail_qty * $stock_convert['size'];
												$sum_stock_out_convert += $convert_total;
											}else{
												$convert_total = $p_detail / $stock_convert['size'];
												$sum_stock_out_convert += $convert_total;
											}
										}
									}
									$stock_out = $stock_out_origin->sum('detail_qty') + $sum_stock_in_convert;
								@endphp

								<!-- Stock Out Sales -->
								{{ number_format($stock_out) }}
							</td>
							<td>
								{{ number_format($stock_retur) }}
							</td>
							<td>
								<a href="{{ route('inventory.edit', $i->id) }}" class="btn btn-primary btn-xs">
									<i class="fa fa-pencil-square"></i> Edit
								</a>
							</td>
						</tr>
						@endforeach
	        		</tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>

@stop

@section('script')
  <script type="text/javascript">
  	// localStorage.clear();
    var tableInventory = $("#tableInventory").DataTable({
    	"dom": "rtip",
      	"pageLength": 10,
      	"retrieve": true,
      	"stateSave": true,
      	"scrollY": true,
        "scrollX": true
    });

    $("#searchDtbox").keyup(function() {
          tableInventory.search($(this).val()).draw() ;
    });

    $('#reportrange').daterangepicker({
      singleDatePicker: true,
      startDate: '{{ date('m/d/Y', strtotime($date)) }}',
    }, function(start, end, label){
      console.log(start.toISOString(), end.toISOString(), label);

      $('#reportrange span').html(start.format('MMMM D, YYYY'));
      updateQueryStringParam('date', start.format('Y-MM-DD'));

    });
  </script>
@stop