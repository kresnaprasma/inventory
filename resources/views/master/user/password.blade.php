@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> User
        <small>User Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#">Master</a></li>
        <li><a href="{{ route('master.user.index') }}">User</a></li>
        <li class="active">Edit Password</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
    {!! Form::model($user, ['route' => ['master.user.password.update', $user->id],'id'=>'formUser','method'=>'PATCH']) !!}
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">User {{ $user->username }} | {{ $user->email }}</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                {!! Form::label('old_password', 'Old password', ['class'=>'control-label']) !!}
                {!! Form::password('old_password', ['class'=>'form-control','required']) !!}
                @if ($errors->has('old_password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('old_password') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password', ['class'=>'control-label']) !!}
                {!! Form::password('password', ['class'=>'form-control','required']) !!}
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">
                {!! Form::label('password-confirm', 'Confirm Password',['class'=>'control-label']) !!}
                {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'password-confirm','required']) !!}
              </div>
            </div>
          </div>
        </div>

        <div class="box-footer">
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
    {!! Form::close() !!}
  </section>
@stop