@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> User
        <small>User Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#">Master</a></li>
        <li><a href="{{ route('master.user.index') }}">User</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
    {!! Form::model($user = new \App\User, ['route' => 'master.user.store','id'=>'formUser']) !!}
      @include('master.user._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  {{-- @include('master.outlet._js') --}}
@stop