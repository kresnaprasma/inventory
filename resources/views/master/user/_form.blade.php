@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">User >> Create</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', "Name", ['class'=>'control-label']) !!}
            {!! Form::text('name', old('name'), ['class'=>'form-control','required','autofocus']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('username') ? 'has-error' : '' }}">
            {!! Form::label('username', 'Username', ['class'=>'control-label']) !!}
            {!! Form::text('username', old('username'), ['class'=>'form-control']) !!}
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email', 'E-mail Address',['class'=>'control-label']) !!}
            {!! Form::email('email', old('email'), ['class'=>'form-control','required']) !!}
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>

          {{-- <div class="form-group{{ $errors->has('outlet_code') ? ' has-error' : '' }}">
            {!! Form::label('outlet_code', 'Outlet',['class'=>'control-label']) !!}
            {!! Form::select('outlet_code', $outlet, old('outlet_code'), ['class'=>'form-control']) !!}
            @if ($errors->has('outlet_code'))
                <span class="help-block">
                    <strong>{{ $errors->first('outlet_code') }}</strong>
                </span>
            @endif
          </div>  --}}         

          {{-- Password --}}
          @if (!$edit)
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              {!! Form::label('password', 'Password', ['class'=>'control-label']) !!}
              {!! Form::password('password', ['class'=>'form-control','required']) !!}
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group">
              {!! Form::label('password-confirm', 'Confirm Password',['class'=>'control-label']) !!}
              {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'password-confirm','required']) !!}
            </div>
          @endif
      </div>
      <!-- /.col -->
      <div class="col-md-6">
        <div class="bs-example">
          <div class="panel-group" id="accordion">
            @php
              $no = 0;
            @endphp
              @foreach ($permissions as $p)
                @php $no = $no + 1; @endphp
              <div class="panel panel-default">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $no }}">
                            {{ ucfirst($p->name) }}
                          </a>
                      </h4>
                  </div>
                  <div id="collapse{{ $no }}" class="panel-collapse collapse in">
                      <div class="panel-body">
                        @if ($user->permission()->count() > 0)
                          @foreach ($p->permission as $pp)
                            @php
                              $checked = in_array($pp->name, $user_permission) ? true :false;
                            @endphp
                            <div class="checkbox">
                                <label>
                                  {{ Form::checkbox('permission_id[]',$pp->id, $checked) }} 
                                  {{ $pp->description }}
                                </label>
                            </div>
                          @endforeach
                        @else 
                          @foreach ($p->permission as $pp)
                            <div class="checkbox">
                                <label>
                                  {{ Form::checkbox('permission_id[]',$pp->id) }} 
                                  {{ $pp->description }}
                                </label>
                            </div>
                          @endforeach
                        @endif
                      </div>
                  </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </div>
</div>
<!-- /.box -->
