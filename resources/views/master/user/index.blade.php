@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User
        <small>Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#">Master</a></li>
        <li>User</li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Tables User
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
		         	<a href="{{ route('master.user.create') }}" class="btn btn-primary">
		            	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
		          	</a>
		          	<button type="button" class="btn btn-danger" onclick="DeleteUser()">
		            	<i class="fa fa-trash" aria-hidden="true"></i> Delete
		          	</button>
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Filter
		          	</span>
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['route'=>'master.user.delete', 'id'=>'formOutlet']) !!}
				<table id="tableUser" class="table table-striped table-color">
					<thead>
						<tr>
							<th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>Username</th>
	            			<th>Name</th>
	            			<th>Email</th>
	            			<th>Action</th>
						</tr>
					</thead>
					<tbody>
	          			@foreach ($user as $u)
						<tr>
							<td>
								<input type="checkbox" id="idTableUser" value="{{ $u->id }}" name="id[]" class="checkin">
							</td>
							<td>{{ $u->username }}</td>
							<td>{{ $u->name }}</td>
							<td>{{ $u->email }}</td>
							<td>
								<a href="{{ route('master.user.edit', $u->id) }}" class="btn btn-default btn-xs">
									<i class="fa fa-pencil-square"></i> Edit
								</a>
								<a href="{{ route('master.user.password.edit', $u->id) }}" class="btn btn-default btn-xs">
									<i class="fa fa-pencil-square"></i> Change Password
								</a>
								@if (!$u->active)
									<a href="#" class="btn btn-xs btn-danger" onclick="active('{{ $u->id }}')">Deactive</a>
								@else
									<a href="#" class="btn btn-xs btn-primary" onclick="active('{{ $u->id }}')">Active</a>
								@endif
							</td>
						</tr>
						@endforeach
	        		</tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>
	@include('master.user._modal')
@stop

@section('script')
  <script type="text/javascript">
    var tableUser = $("#tableUser").DataTable({
    	"dom": "rtip",
      	"pageLength": 10,
      	"retrieve": true,
      	"stateSave": true,
    });

    $("#searchDtbox").keyup(function() {
          tableUser.search($(this).val()).draw() ;
    });  

    function DeleteUser() {
      if ($('.checkin').is(':checked')) 
        {
          $('#deleteUserModal').modal("show");
        }
        else
        {
          $('#deleteNoModal').modal("show");
        }
    }

    function deleteRole(){
		$('#formOutlet').submit();
    }

    function active(id) {
    	$.ajax({
    		url: '/api/web/user/active/'+id,
    		type: 'GET',
    		success:function(response){
    			location.reload();
    		},
    		error:function(response){
    			console.log(response);
    		}
    	})
    }
  </script>
@stop