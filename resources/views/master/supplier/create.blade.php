@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Supplier
        <small>Supplier Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.supplier.index') }}">Supplier</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
    {!! Form::model($supplier = new \App\Supplier, ['route' => 'master.supplier.store','id'=>'formCreateSupplier']) !!}
      @include('master.supplier._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('master.supplier._js')
@stop