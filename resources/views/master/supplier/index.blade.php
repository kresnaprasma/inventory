@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Supplier
        <small>Supplier Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.supplier.index') }}">Supplier</a></li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				   <h3 class="box-title">Supplier</h3>
				   <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				   </div>
				</div>

				<div class="box-body">
					<div class="col-md-12 box-body-header">
					   <div class="col-md-8">
					      <a href="{{ route('master.supplier.create') }}" class="btn btn-primary">
					      	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
					      </a>
					      <button type="button" class="btn btn-danger" onclick="deleteSupplier()">
					      <i class="fa fa-trash" aria-hidden="true"></i> Delete
					      </button>
					   </div>
					   <div class="col-md-4">
					      <input type="text" id="searchDtbox" class="form-control" placeholder="Search">
				   		</div>
					</div>

					{!! Form::open(['route'=>'master.supplier.delete', 'id'=>'formDeleteSupplier']) !!}
					<div>
						
					<table class="table table-bordered table-striped table-color" id="tableSupplier">
						<thead>
							<th><input type="checkbox" id="check_all"></th>
							<th>Supplier No.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Address</th>
							<th>PIC Name</th>
							<th>PIC Phone</th>
							<th>Action</th>
						</thead>
						<tbody>
							@foreach($supplier as $s)
								<tr>
									<td>
										<input type="checkbox" id="idTableSupplier" name="id[]" class="checkin" value="{{ $s->id }}">
									</td>
									<td><b>{{ $s->supplier_no }}</b></td>
									<td>{{ $s->name }}</td>
									<td>{{ $s->email }}</td>
									<td>{{ $s->phone }}</td>
									<td>{{ $s->address }}</td>
									<td>{{ $s->pic_name }}</td>
									<td>{{ $s->pic_phone }}</td>
									<td>
								<a href="{{ route('master.supplier.edit', $s->id) }}" class="btn btn-default btn-xs">
									<i class="fa fa-pencil-square"></i> Edit
								</a>
							</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! Form::close() !!}
					</div><!-- box body -->
				</div>
			</div>
		</div>
	</div>

	@include('master.supplier._modal')
@stop

@section('scripts')
	<script type="text/javascript">
		var tableSupplier = $('#tableSupplier').DataTable({
			"sDom": 'rt',
      		"columnDefs": [{
        		"targets": [],
        		"orderable": false,

      		}],
      		"stateSave": true,
		});
		$("#searchDtbox").keyup(function() {
      		tableSupplier.search($(this).val()).draw();
    	});
    	$('#tableSupplier tbody').on('dblclick', 'tr', function () {
      		if ( $(this).hasClass('selected') ) {
        		$(this).removeClass('selected');
      		}
	      	else {
	        	tableSupplier.$('tr.selected').removeClass('selected');
	        	$(this).addClass('selected');
	        	var id = $(this).find('#idTableSupplier').val();
	          	window.location.href = "/master/supplier/"+id+"/edit";
	      	}
    	});
    	function deleteSupplier() {
			if ($('.checkin').is(':checked')) 
			{
				$('#deleteSupplierModal').modal("show");
			} else {
				$('#deleteNoModal').modal("show");
				}
		}
		function DeleteSupplier() {
			$("#formDeleteSupplier").submit();
		}
	</script>
@stop