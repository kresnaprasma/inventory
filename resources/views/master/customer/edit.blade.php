@extends('layouts.admin.admin')

@section('content')
	{!! Form::model($customer, ['route' => ['master.customer.update', $customer->id], 'id'=>'formCustomer', 'method'=>'PATCH']) !!}
    	@include('master.customer._form',['edit'=>true])
    {!! Form::close() !!}
    @include('master.customer._modal')
@endsection

@section('script')
	@include('master.customer._js')
@endsection