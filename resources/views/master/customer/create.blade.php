@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Customer
        <small>Customer Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.customer.index') }}">Customer</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
@stop

@section('content')
	<section class="content">
		{!! Form::model($customer = new \App\Customer, ['route' => 'master.customer.store','id'=>'formCustomer']) !!}
	    	@include('master.customer._form',['edit'=>false])
	    {!! Form::close() !!}
	    @include('master.customer._modal')
	</section>
@endsection

@section('script')
	@include('master.customer._js')
@endsection