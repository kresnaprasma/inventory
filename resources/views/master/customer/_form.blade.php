<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Create Customer - <b>@if ($edit){{ $customer->customer_no }}@else {{ $customer->Maxno() }}@endif</b>
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-4">
        <div class="form-group{{ $errors->has('customer_no') ? ' has-error' : '' }}">
          {!! Form::label('customer_no', "Customer No.", ['class'=>'control-label']) !!}
          @if($edit)
            {!! Form::text('customer_no', old('customer_no'), ['class'=> $errors->has('customer_no') ? 'form-control is-invalid' : 'form-control','required','id'=>'customer_no']) !!}
          @else
            {!! Form::text('customer_no', \App\Customer::Maxno(), ['class'=> $errors->has('customer_no') ? 'form-control is-invalid' : 'form-control','required','id'=>'customer_no']) !!}
          @endif
          @if ($errors->has('customer_no'))
            <div class="invalid-feedback">{{ $errors->first('customer_no') }}</div>
          @endif
        </div>

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          {!! Form::label('name', "Name*", ['class'=>'control-label']) !!}
          {!! Form::text('name', old('name'), ['class'=> $errors->has('name') ? 'form-control is-invalid' : 'form-control','required','autofocus']) !!}
          @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
          @endif
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            {!! Form::label('phone', "Phone*", ['class'=>'control-label']) !!}
            {!! Form::text('phone', old('phone'), ['class'=> $errors->has('phone') ? 'form-control is-invalid' : 'form-control']) !!}
            @if ($errors->has('phone'))
              <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
            @endif
        </div>

        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
          {!! Form::label('address', 'Address*') !!}
          {!! Form::text('address', null,['class'=>  $errors->has('address') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('address'))
            <div class="invalid-feedback">{{ $errors->first('address') }}</div>
          @endif
        </div>

        <div class="form-group{{ $errors->has('identity_number') ? ' has-error' : '' }}">
          {!! Form::label('identity_number', "No. KTP", ['class'=>'control-label']) !!}
          {!! Form::text('identity_number', old('identity_number'), ['class'=> $errors->has('identity_number') ? 'form-control is-invalid' : 'form-control','required','autofocus']) !!}
          @if ($errors->has('identity_number'))
            <div class="invalid-feedback">{{ $errors->first('identity_number') }}</div>
          @endif
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group{{ $errors->has('rt') ? ' has-error' : '' }}">
          {!! Form::label('rt', 'RT') !!}
          {!! Form::text('rt', null,['class'=>  $errors->has('rt') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('rt'))
            <div class="invalid-feedback">{{ $errors->first('rt') }}</div>
          @endif
        </div>
        <div class="form-group{{ $errors->has('rw') ? ' has-error' : '' }}">
          {!! Form::label('rw', 'RW') !!}
          {!! Form::text('rw', null,['class'=>  $errors->has('rw') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('rw'))
            <div class="invalid-feedback">{{ $errors->first('rw') }}</div>
          @endif
        </div>

        <div class="form-group{{ $errors->has('kelurahan') ? ' has-error' : '' }}">
          {!! Form::label('kelurahan', 'Kelurahan') !!}
          {!! Form::text('kelurahan', null,['class'=>  $errors->has('kelurahan') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('kelurahan'))
            <div class="invalid-feedback">{{ $errors->first('kelurahan') }}</div>
          @endif
        </div>

        <div class="form-group{{ $errors->has('kecamatan') ? ' has-error' : '' }}">
          {!! Form::label('kecamatan', 'Kecamatan') !!}
          {!! Form::text('kecamatan', null,['class'=>  $errors->has('kecamatan') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('kecamatan'))
            <div class="invalid-feedback">{{ $errors->first('kecamatan') }}</div>
          @endif
        </div>

        <div class="form-group{{ $errors->has('kabupaten') ? ' has-error' : '' }}">
            {!! Form::label('kabupaten', "City", ['class'=>'control-label']) !!}
            {!! Form::text('kabupaten', old('kabupaten'), ['class'=> $errors->has('kabupaten') ? 'form-control is-invalid' : 'form-control']) !!}
            @if ($errors->has('kabupaten'))
              <div class="invalid-feedback">{{ $errors->first('kabupaten') }}</div>
            @endif
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          {!! Form::label('email', 'Email') !!}
          {!! Form::text('email', null,['class'=>  $errors->has('email') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
          @endif
        </div>
        <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
          {!! Form::label('birthday', "Birthday", ['class'=>'control-label']) !!}
          {!! Form::date('birthday', old('birthday'), ['class'=> $errors->has('birthday') ? 'form-control is-invalid' : 'form-control','required','autofocus']) !!}
          @if ($errors->has('birthday'))
            <div class="invalid-feedback">{{ $errors->first('birthday') }}</div>
          @endif
        </div>
        <div class="form-group{{ $errors->has('birthplace') ? ' has-error' : '' }}">
          {!! Form::label('birthplace', "Birthplace", ['class'=>'control-label']) !!}
          {!! Form::date('birthplace', old('birthplace'), ['class'=> $errors->has('birthplace') ? 'form-control is-invalid' : 'form-control','required','autofocus']) !!}
          @if ($errors->has('birthplace'))
            <div class="invalid-feedback">{{ $errors->first('birthplace') }}</div>
          @endif
        </div>
        <div class="form-group{{ $errors->has('postalcode') ? ' has-error' : '' }}">
          {!! Form::label('postalcode', "Postcode", ['class'=>'control-label']) !!}
          {!! Form::text('postalcode', old('postalcode'), ['class'=> $errors->has('postalcode') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('postalcode'))
            <div class="invalid-feedback">{{ $errors->first('zipcode') }}</div>
          @endif
        </div>
        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
          {!! Form::label('province', "Province", ['class'=>'control-label']) !!}
          {!! Form::text('province', old('province'), ['class'=> $errors->has('province') ? 'form-control is-invalid' : 'form-control']) !!}
          @if ($errors->has('province'))
            <div class="invalid-feedback">{{ $errors->first('province') }}</div>
          @endif
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">
          <button type="button" class="btn btn-primary" onclick="save()">Save</button>
      </div>
  </div>
</div>
<!-- /.box -->
