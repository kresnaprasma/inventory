@extends('layouts.admin.admin')

@section('content-header')
  <section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Customer
        <small>Customer Manajemen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#">Master</a></li>
        <li><a href="{{ route('master.customer.index') }}">Customer</a></li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
           <h3 class="box-title">Customer</h3>
           <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
           </div>
        </div>

        <div class="box-body">
          <div class="col-md-12 box-body-header">
             <div class="col-md-8">
                <a href="{{ route('master.customer.create') }}" class="btn btn-default">
                  <i class="fa fa-plus" aria-hidden="true"></i> New
                </a>
                <button type="button" class="btn btn-default" onclick="deleteSupplier()">
                <i class="fa fa-times" aria-hidden="true"></i> Delete
                </button>
             </div>
             <div class="col-md-4">
                <input type="text" id="searchDtbox" class="form-control" placeholder="Search">
              </div>
          </div>

          {!! Form::open(['route'=>'master.customer.delete', 'id'=>'formDeleteSupplier']) !!}
          <div>
            
          <table class="table table-bordered table-striped table-color" id="tableSupplier">
            <thead>
              <tr>
                <th><input type="checkbox" id="check_all"></th>
                <th>Customer No</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($customers as $c)
                <tr>
                  <td>
                    <input type="checkbox" id="idTableCustomer" name="id[]" class="checkin" value="{{ $c->id }}">
                  </td>
                  <td>{{ $c->customer_no }}</td>
                  <td>{{ $c->name }}</td>
                  <td>{{ $c->phone }}</td>
                  <td>{{ $c->address }}</td>
                  <td>
                    <a href="{{ route('master.customer.edit', $c->id) }}" class="btn btn-default btn-xs">
                      <i class="fa fa-pencil-square"></i> Edit
                    </a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          {!! Form::close() !!}
          </div><!-- box body -->
        </div>
      </div>
    </div>
  </div>

  @include('master.customer._modal')
@stop

@section('scripts')
  <script type="text/javascript">
    var tableCustomer = $('#tableCustomer').DataTable({
      "sDom": 'rt',
          "columnDefs": [{
            "targets": [],
            "orderable": false,

          }],
          "stateSave": true,
    });
    $("#searchDtbox").keyup(function() {
          tableCustomer.search($(this).val()).draw();
      });
      $('#tableCustomer tbody').on('dblclick', 'tr', function () {
          if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
          }
          else {
            tableCustomer.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var id = $(this).find('#idTableCustomer').val();
              window.location.href = "/master/customer/"+id+"/edit";
          }
      });
      function deleteCustomer() {
      if ($('.checkin').is(':checked')) 
      {
        $('#deleteCustomerModal').modal("show");
      } else {
        $('#deleteNoModal').modal("show");
        }
    }
    function DeleteCustomer() {
      $("#formDeleteCustomer").submit();
    }
  </script>
@stop