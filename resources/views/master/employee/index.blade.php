@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Employee
        <small>Employee Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.employee.index') }}">Employee</a></li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				   <h3 class="box-title">Employee</h3>
				   <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				   </div>
				</div>

				<div class="box-body">
					<div class="col-md-12 box-body-header">
					   <div class="col-md-8">
					      <a href="{{ route('master.employee.create') }}" class="btn btn-primary">
					      	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
					      </a>
					      <button type="button" class="btn btn-danger" onclick="deleteEmployee()">
					      <i class="fa fa-trash" aria-hidden="true"></i> Delete
					      </button>
					   </div>
					   <div class="col-md-4">
					      <input type="text" id="searchDtbox" class="form-control" placeholder="Search">
				   		</div>
					</div>

					{{-- {!! Form::open(['route'=>'master.employee.delete', 'id'=>'formDeleteEmployee']) !!} --}}
					<div>
						
					<table class="table table-bordered table-striped table-color" id="tableEmployee">
						<thead>
							<th><input type="checkbox" id="check_all"></th>
							<th>Employee No.</th>
							<th>Name</th>
							<th>Position</th>
							<th>Phone</th>
							<th>Address</th>
							<th>Action</th>
						</thead>
						<tbody>
							@foreach($emp as $e)
								<tr>
									<td>
										<input type="checkbox" id="idTableEmployee" name="id[]" class="checkin" value="{{ $e->id }}">
									</td>
									<td><b>{{ $e->supplier_no }}</b></td>
									<td>{{ $e->name }}</td>
									<td>{{ $e->position_no }}</td>
									<td>{{ $e->phone }}</td>
									<td>{{ $e->address }}</td>
									<td>
								<a href="{{ route('master.employee.edit', $e->id) }}" class="btn btn-default btn-xs">
									<i class="fa fa-pencil-square"></i> Edit
								</a>
							</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! Form::close() !!}
					</div><!-- box body -->
				</div>
			</div>
		</div>
	</div>

	@include('master.employee._modal')
@stop

@section('scripts')
	<script type="text/javascript">
		var tableEmployee = $('#tableEmployee').DataTable({
			"sDom": 'rt',
      		"columnDefs": [{
        		"targets": [],
        		"orderable": false,

      		}],
      		"stateSave": true,
		});
		$("#searchDtbox").keyup(function() {
      		tableEmployee.search($(this).val()).draw();
    	});
    	$('#tableEmployee tbody').on('dblclick', 'tr', function () {
      		if ( $(this).hasClass('selected') ) {
        		$(this).removeClass('selected');
      		}
	      	else {
	        	tableEmployee.$('tr.selected').removeClass('selected');
	        	$(this).addClass('selected');
	        	var id = $(this).find('#idTableEmployee').val();
	          	window.location.href = "/master/employee/"+id+"/edit";
	      	}
    	});
    	function deleteEmployee() {
			if ($('.checkin').is(':checked')) 
			{
				$('#deleteSupplierModal').modal("show");
			} else {
				$('#deleteNoModal').modal("show");
				}
		}
		function DeleteEmployee() {
			$("#formDeleteEmployee").submit();
		}
	</script>
@stop