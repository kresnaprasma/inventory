@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Employee
        <small>Employee Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.employee.index') }}">Employee</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
    {!! Form::model($emp = new \App\Employee, ['route' => 'master.employee.store','id'=>'formEmployee']) !!}
      @include('master.employee._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('master.employee._js')
  {{-- <script type="text/javascript">
    window.onbeforeunload = function (e) {
      var id = $('#id_employee').val();
      alert(checkload);
      alert(id);
      
      if (checkload == true) {
        $.ajax({
          url: '/api/v1/human-resource/employee/'+id,
          type: 'DELETE',
          success: function(response){
            e = e || window.event;

            // For IE and Firefox prior to version 4
            if (e) {
              e.returnValue = 'Any string';
            }

            // For Safari
            return 'Any string';
          }
        });
      }
    }
    @if (!$errors->any())
      createNo();
    @else 
      getPicture();
    @endif
  </script> --}}
@stop