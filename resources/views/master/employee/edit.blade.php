@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Employee
        <small>Employee Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.employee.index') }}">Employee</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
@stop

@section('content')
  <section class="content">
    {!! Form::model($emp, ['route' => ['master.employee.update', $emp->id],'id'=>'formEmployee','method'=>'PATCH']) !!}
      @include('master.employee._form',['edit'=>true])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('master.employee._js')
  {{-- <script type="text/javascript">
    getPicture();
  </script> --}}
@stop