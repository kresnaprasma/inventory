@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Category
        <small>Category Manajemen</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#">Master</a></li>
        <li><a href="{{ route('master.inventorycategory.index') }}">Inventory Category</a></li>
        <li class="active">Index</li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				   <h3 class="box-title">Inventory Category</h3>
				   <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				   </div>
				</div>

				<div class="box-body">
					<div class="col-md-12 box-body-header">
					   <div class="col-md-8">
					      <button type="button" class="btn btn-primary" onclick="AddInventoryCategory()">
					      	<i class="fa fa-plus-circle" aria-hidden="true"></i> New
					      </button>
					      <button type="button" class="btn btn-danger" onclick="deleteInventoryCategory()">
					      <i class="fa fa-trash" aria-hidden="true"></i> Delete
					      </button>
					      <div class="btn-group">
					         <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					         Action <span class="caret"></span>
					         </button>
					         <ul class="dropdown-menu">
					            <li><a href="#">Print</a></li>
					            <li><a href="#">Import</a></li>
					            <li><a href="#">Export</a></li>
					            <li role="separator" class="divider"></li>
					            <li><a href="#">Find</a></li>
					         </ul>
					      </div>
					   </div>
					   <div class="col-md-4">
					      <input type="text" id="searchDtbox" class="form-control" placeholder="Search">
				   		</div>
					</div>
					<hr>

					{{-- {!! Form::open(['route'=>'master.bank.delete', 'id'=>'formDeleteBank']) !!} --}}
					<div>
						
					<table class="table table-bordered table-striped table-color" id="tableInventoryCategory">
						<thead>
							<th>Id</th>
							<th>Name</th>
						</thead>
						<tbody>
							@foreach($categories as $c)
								<tr>
									<td>
										{{ $c->id }}
										{!! Form::hidden('id', $c->id,['id'=>'idTableInventoryCategory']) !!}
									</td>
									<td>
										{{ $c->name }}
										{!! Form::hidden('name',$c->name,['id'=>'nameTableInventoryCategory']) !!}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! Form::close() !!}
					</div><!-- box body -->
				</div>
			</div>
		</div>
	</div>

	@include('master.inventorycategory._modal')
@stop


@section('script')
	<script type="text/javascript">
		var tableInventoryCategory = $('#tableInventoryCategory').DataTable({
			"dom": "rtip",
        	"pageLength": 15,
        	"retrieve": true,
        	"stateSave": true,
		});

		$("#searchDtbox").keyup(function() {
      		tableInventoryCategory.search($(this).val()).draw();
    	});

    	$('#tableInventoryCategory tbody').on('dblclick', 'tr', function () {
      		if ( $(this).hasClass('selected') ) {
        		$(this).removeClass('selected');
      		}
	      	else {
	        	tableInventoryCategory.$('tr.selected').removeClass('selected');
	        	$(this).addClass('selected');
	          	var id = $(this).find('#idTableInventoryCategory').val();
	          	var name = $(this).find('#nameTableInventoryCategory').val();

	          	EditBank(id, name);
	      	}
    	});

    	function AddInventoryCategory() {
      		$("#createInventoryCategoryModal").modal("show");
    	}

    	function EditBank(id, name) {
      		$("#editInventoryCategoryForm").attr('action', '/inventorycategory/' + id);
      		$("#nameInventoryCategory").val(name);
      		$("#editInventoryCategoryModal").modal("show");
    	}

    	function deleteInventoryCategory() {
			if ($('.checkin').is(':checked')) 
			{
				$('#deleteInventoryCategoryModal').modal("show");
			} else {
				$('#deleteNoModal').modal("show");
				}
		}

		function DeleteInventoryCategory() {
			$("#formDeleteInventoryCategory").submit();
		}
	</script>
@stop