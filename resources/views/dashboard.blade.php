@extends('layouts.admin.admin')

@section('style')
	<style type="text/css">
		.box .knob-label{
			font-weight: bold;
			font-size: 18px;
			color: #505255;
		}
		.panel-fullscreen {
			display: block;
			z-index: 9999;
			position: fixed;
			width: 100%;
			height: auto;
			top: 0;
			right: 0;
			left: 0;
			bottom: 0;
			overflow: auto;
		}
	</style>
@endsection
@section('content-header')
  <section class="content-header">
      <h1>
        <i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard
      </h1>
      <div style="margin-top: 10px">
        <button type="button" class="btn btn-primary" id="reportrange">
          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
          <span>
            @if ($begin)
              {{ $begin->format('M d, Y') }}
              -
              {{  $end->format('M d, Y') }}
            @endif
          </span>
          <b class="caret"></b>
        </button>
      </div>
    </section>
@stop

@section('content')
	<div class="row">
	    <div class="col-lg-3 col-xs-6">
	      <!-- small box -->
	      <div class="small-box bg-gray-dark">
	        <div class="inner">
	          <h3>{{ number_format($total_sales) }}</h3>

	          <p>Total Sales</p>
	        </div>
	        <div class="icon">
	          <i class="ion ion-bag"></i>
	        </div>
	      </div>
	    </div>
	    <!-- ./col -->
	    <div class="col-lg-3 col-xs-6">
	    	<!-- small box -->
	    	<div class="small-box bg-red">
	    		<div class="inner">
	    			<h3>{{ number_format($total_visitor) }}</h3>
	    			<p>Total Visitor</p>
	    		</div>
	    		<div class="icon">
	          		<i class="ion ion-stats-bars"></i>
	        	</div>
	    	</div>
	    </div>
	    <!-- ./col -->

	    <div class="col-lg-3 col-xs-6">
	      <!-- small box -->
	      <div class="small-box bg-gray-dark">
	        <div class="inner">
	          <h3>{{ number_format($total_average_sales) }}</h3>

	          <p>Average Sales</p>
	        </div>
	        <div class="icon">
	          <i class="ion ion-bag"></i>
	        </div>
	      </div>
	    </div>
	    <!-- ./col -->

	    <div class="col-lg-3 col-xs-6">
	    	<!-- small box -->
	    	<div class="small-box bg-red">
	    		<div class="inner">
	    			<h3>0</h3>
	    			<p>Refund</p>
	    		</div>
	    		<div class="icon">
	          		<i class="ion ion-stats-bars"></i>
	        	</div>
	    	</div>
	    </div>
	    <!-- ./col -->
	</div>

	<!-- Chart box -->
	<div class="row">
		<div class="col-md-4">
			<div class="box box-warning">
				<div class="box-header">
					<button type="button" class="toggle-expand-btn btn bg-yellow btn-sm"><i class="fa fa-expand"></i></button>
				</div>

				<div class="box-body">
			    	<div class="chart">
			      		<canvas id="barChart" style="height:230px"></canvas>
			    	</div>
			  	</div>
			</div>
			<!-- end Chart box -->
		</div>
		<div class="col-md-4">
			<div class="box box-warning">
				<div class="box-header">
					<button type="button" class="toggle-expand-btn btn bg-yellow btn-sm"><i class="fa fa-expand"></i></button>
				</div>

				<div class="box-body">
			    	<div class="chart">
			      		<canvas id="barChartVisitor" style="height:230px"></canvas>
			    	</div>
			  	</div>
			</div>
			<!-- end Chart box -->
		</div>

		<div class="col-md-4">
			<div class="box box-warning">
				<div class="box-header">
					<button type="button" class="toggle-expand-btn btn bg-yellow btn-sm"><i class="fa fa-expand"></i></button>
				</div>

				<div class="box-body">
					<div class="chart">
						<canvas id="barChartAverageVisitor" style="height: 230px"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>


	{{-- Sales --}}
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header">
					<h3><b>ORDERS</b></h3>
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="chart">
				      			<canvas id="pieSalesCategoryChart"></canvas>
				    		</div>	
						</div>

						<div class="col-md-4 text-center">
							<input type="text" class="knob" value="{{ $total_sales_daily }}" disabled="disabled">	
							@if ($total_sales_daily)
								<div class="knob-label">Sales Daily: {{ number_format($total_sales_daily) }}</div>
							@endif
						</div>
						
						<div class="col-md-4 text-center">
							<input type="text" class="knob" value="{{ $total_sales }}" disabled="disabled">	
							@if ($total_sales)
								<div class="knob-label">Target Sales: {{ number_format($total_sales) }}</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header">
					<h3><b>OPERATIONAL</b></h3>
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-3 text-center">
							<input type="text" class="knob-precentage" value="{{ round($percentage_purchasing,0,PHP_ROUND_HALF_DOWN) }}" disabled="disabled">
							<div class="knob-label">Purchasing Percentage</div>
						</div>
						<div class="col-md-3 text-center">
							<input type="text" class="knob-precentage" value="{{ round($percentage_gross_profit,0,PHP_ROUND_HALF_DOWN) }}" disabled="disabled">
							<div class="knob-label">Gross Profit</div>
						</div>

						<div class="col-md-3 text-center">
							<input type="text" class="knob-precentage" value="#" disabled="disabled">
							<div class="knob-label">Nett Profit</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="chart">
								<canvas id="pieCashChart"></canvas>
							</div>
						</div>
						<div class="col-md-6">
							<div class="chart">
				      			<canvas id="pieMenuChart"></canvas>
				    		</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Menu Rank Box-->
		
		<!-- end Menu Rank Box-->
		
	</div>
@endsection

@section('script')
	@include('_js')
@stop