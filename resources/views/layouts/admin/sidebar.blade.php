<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      {{-- <div class="pull-left image">
        <img src="/admin-lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div> --}}
      <div class="pull-left info">
        {{-- <p>{{ auth()->user()->name }}</p> --}}
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">TB. Aneka Bangunan</li>

      <li><a href="/"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
      <li class="treeview {{-- {{ set_active(['master.user.index', 'master.bank.index', 'master.supplier.index', 'master.employee.index', 'master.customer.index']) }} --}}">
        <a href="#">
          <i class="fa fa-folder"></i> <span>Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{-- {{ set_active('master.user.index') }} --}}">
            <a href="{{ route('master.user.index') }}">
              <i class="fa fa-user-secret"></i> User
            </a>
          </li>
          <li class="{{-- {{ set_active('master.bank.index') }} --}}">
            <a href="{{ route('master.bank.index') }}">
              <i class="fa fa-bank"></i> Bank
            </a>
          </li>
          <li class="{{-- {{ set_active('master.supplier.index') }} --}}">
            <a href="{{ route('master.supplier.index') }}">
              <i class="fa fa-truck"></i> Supplier
            </a>
          </li>
          <li class="{{-- {{ set_active('master.employee.index') }} --}}">
            <a href="{{ route('master.employee.index') }}">
              <i class="fa fa-user"></i> Employee
            </a>
          </li>
          <li class="{{-- {{ set_active('master.customer.index') }} --}}">
            <a href="{{ route('master.customer.index') }}">
              <i class="fa fa-users"></i> Customer
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview {{-- {{ set_active(['inventory.index', 'master.inventorycategory.index', 'master.merk.index', 'master.type.index']) }} --}}">
        <a href="#">
          <i class="fa fa-tags"></i> <span>Inventory</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{-- {{ set_active('inventory.index') }} --}}">
            <a href="{{ route('inventory.index') }}">
              <i class="fa fa-book"></i> Index
            </a>
          </li>
          <li class="{{-- {{ set_active('master.inventorycategory.index') }} --}}">
            <a href="{{ route('master.inventorycategory.index') }}">
              <i class="fa fa-circle-o"></i>Category
            </a>
          </li>
          <li class="{{-- {{ set_active('master.merk.index') }} --}}">
            <a href="{{ route('master.merk.index') }}">
              <i class="fa fa-circle-o"></i> Merk
            </a>
          </li>
          <li class="{{-- {{ set_active('master.type.index') }} --}}">
            <a href="{{ route('master.type.index') }}">
              <i class="fa fa-circle-o"></i> Type
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview {{-- {{ set_active(['inventory.paint.index', 'inventory.colorant.index']) }} --}}">
        <a href="#">
          <i class="fa fa-cube"></i> <span>Cat Tinting</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{-- {{ set_active('inventory.paint.index') }} --}}">
            <a href="{{ route('inventory.paint.index') }}">
              <i class="fa fa-paint-brush"></i> Cat Tinting
            </a>
          </li>
          <li class="{{-- {{ set_active('inventory.colorant.index') }} --}}">
            <a href="{{ route('inventory.colorant.index') }}">
              <i class="fa fa-flask"></i> Colorant
            </a>
          </li>
        </ul>
      </li>

      {{-- <li class="header">Purchasing</li> --}}
        <li class="{{-- {{ set_active(['purchasing.index', 'purchasing.create', 'purchasing.edit']) }} --}}">
          <a href="{{ route('purchasing.index') }}">
            <i class="fa fa-newspaper-o"></i> <span>Purchasing</span>
          </a>
        </li>
        <li class="{{-- {{ set_active(['purchasing.order.index', 'purchasing.order.create', 'purchasing.order.edit']) }} --}}">
          <a href="{{ route('purchasing.order.index') }}">
            <i class="fa fa-clipboard"></i> <span>Purchasing Order</span>
          </a>
        </li>
      {{-- <li class="header">Sales</li> --}}
        <li class="{{-- {{ set_active(['sales.index', 'sales.create', 'sales.edit']) }} --}}">
          <a href="{{ route('sales.index') }}">
            <i class="fa fa-cart-plus"></i> <span>Sales</span>
          </a>
        </li>
        {{-- <li class="{{ set_active(['sales.order.index', 'sales.order.create', 'sales.order.edit']) }}">
          <a href="{{ route('sales.order.index') }}">
            <i class="fa fa-clipboard"></i> <span>Sales Order</span>
          </a>
        </li> --}}
        <li>
          <a href="{{ route('cash.index') }}">
            <i class="fa fa-book"></i> <span>Cash Flow</span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-line-chart"></i> <span>Report</span>
          </a>
        </li>
      {{-- <li><a href="{{ route('order.index') }}"><i class="fa fa-book"></i> <span>Order</span></a></li> --}}
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>