@extends('layouts.admin.admin')

@section('content-header')
	<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Cash Flow
        <small>Cash Flow</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('cash.index') }}" class="active">Cash</a></li>
      </ol>
    </section>
@stop

@section('content')
	<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                {{ number_format($cos->where('cash_type', 'out')->sum('cash_amount')) }}
              </h3>

              <p>Total Cost</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
    </div>

	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">
				Cash Flow Table
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 box-body-header">  
		        <div class="col-md-8">
                <a href="{{ route('cash.create') }}" class="btn btn-primary">
                  <i class="fa fa-plus-circle" aria-hidden="true"></i> New
                </a>
		          	<span style="margin-left: 10px;">
		            	<i class="fa fa-filter" aria-hidden="true"></i> Sort By:
		          	</span>
		          	<button type="button" class="btn btn-primary" id="reportrange">
			            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			            <span>
			            	@if ($begin)
			                	{{ $begin->format('M d, Y') }}
			                	-
			                	{{  $end->format('M d, Y') }}
			              	@endif
			            </span>
			            <b class="caret"></b>
			         </button>
		        </div>

        		<div class="col-md-4">
          			<input type="text" id="searchDtbox" class="form-control" placeholder="search...">
        		</div>
      		</div>
      		{!! Form::open(['id'=>'formCash']) !!}
				<table id="tableCash" class="table table-striped table-color">
					<thead>
						<tr>
						  <th data-sortable="false"><input type="checkbox" id="check_all"/></th>
							<th>Amount</th>
              <th>Notes</th>
							<th>Date</th>
							<th>Method</th>
              <th>Type</th>
							<th>Created By</th>
						</tr>
				  </thead>
					<tbody>
            @foreach ($cos as $c)
              <tr>
                <td>
                  <input type="checkbox" id="idTableCash" value="{{ $c->id }}" name="id[]" class="checkin">
                </td>
                <td>{{ number_format($c->cash_amount) }}</td>
                <td>{{ $c->cash_note }}</td>
                <td>{{ date('d/M/Y H:i:s', strtotime($c->cash_date)) }}</td>
                <td>{{ $c->cash_type }}</td>
                <td>{{ $c->type->name }}</td>
                <td>{{ $c->user->username }}</td>
              </tr>
            @endforeach	       
	        </tbody>
				</table>
      		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
  <script type="text/javascript">
    var tableCash = $("#tableCash").DataTable({
      "dom": "rtip",
      "pageLength": 10,
      "retrieve": true,
      "stateSave": true,
      "scrollY": true,
      "scrollX": true,
      "autoWidth": false,
    });

    $("#searchDtbox").keyup(function() {
          tableCash.search($(this).val()).draw() ;
    });

    $('#reportrange').daterangepicker({
      buttonClasses: ['btn', 'btn-sm'],
      applyClass: 'btn-red',
      cancelClass: 'btn-default',
      startDate: '{{ $begin->format('m/d/y') }}',
      endDate: '{{ $end->format('m/d/y') }}',
      locale: {
        applyLabel: 'Submit',
        cancelLabel: 'Cancel',
        fromLabel: 'From',
        toLabel: 'To',
      },
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, function(start, end, label){
      console.log(start.toISOString(), end.toISOString(), label);

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      updateQueryStringParam('begin', start.format('Y-MM-DD'));
      updateQueryStringParam('end', end.format('Y-MM-DD'));

    });
  </script>
@stop