<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">
      Cash Form Create
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-4">

        <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
          {!! Form::label('type_id', "Type:", ['class'=>'control-label']) !!}
          {!!  Form::select('type_id', $cash_type, null, ['class'=>'form-control','id'=>'type_id']) !!}
          @if ($errors->has('type_id'))
              <span class="help-block">
                  <strong>{{ $errors->first('type_id') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('cash_amount') ? ' has-error' : '' }}">
          {!! Form::label('cash_amount', "Amount:", ['class'=>'control-label']) !!}
          {!!  Form::text('cash_amount', null, ['class'=>'form-control']) !!}
          @if ($errors->has('cash_amount'))
              <span class="help-block">
                  <strong>{{ $errors->first('cash_amount') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('cash_date') ? ' has-error' : '' }}">
          {!! Form::label('cash_date', "Date*", ['class'=>'control-label']) !!}
          <div class='input-group date' id='cash_date'>
            {!! Form::text('cash_date',date('m/d/Y'), ['class'=>'form-control']) !!}
            <span class="input-group-addon input-group-addon-silver">
              <i class="fa fa-calendar" aria-hidden="true"></i>
            </span>
          </div>
          @if ($errors->has('cash_date'))
              <span class="help-block">
                  <strong>{{ $errors->first('cash_date') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('cash_type') ? ' has-error' : '' }}">
          {!! Form::label('cash_type', "Method:", ['class'=>'control-label']) !!}
          {!!  Form::select('cash_type',['in'=>'in','out'=>'out'], null, ['class'=>'form-control']) !!}
          @if ($errors->has('cash_type'))
              <span class="help-block">
                  <strong>{{ $errors->first('cash_type') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('cash_note') ? ' has-error' : '' }}">
          {!! Form::label('cash_note', "Notes:", ['class'=>'control-label']) !!}
          {!!  Form::textarea('cash_note', null, ['class'=>'form-control']) !!}
          @if ($errors->has('cash_note'))
              <span class="help-block">
                  <strong>{{ $errors->first('cash_note') }}</strong>
              </span>
          @endif
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      <div class="box-footer">
        <button type="button" class="btn btn-flat btn-primary pull-right" onclick="save()">
          <i class="fa fa-check-square" aria-hidden="true"></i> Save
        </button>
      </div>
  </div>
</div>
<!-- /.box -->