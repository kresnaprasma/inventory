@extends('layouts.admin.admin')
@section('style')
  <style type="text/css">
    .table-form thead{
      padding: 10px;
    }
  </style>
@endsection
@section('content-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-newspaper-o"></i> 
      <small>Cash Flow</small>
    </h1> 
    <ol class="breadcrumb">
      <li><a href="{{ route('cash.index') }}">Cash</a></li>
      <li class="active">Create</li>
    </ol>
  </section>
@stop


@section('content')
  <section class="content">
    {!! Form::model($cash = new \App\Cash, ['route' => 'cash.store','id'=>'formCreateCash']) !!}
      @include('cashflow._form',['edit'=>false])
    {!! Form::close() !!}
  </section>
@stop

@section('script')
  @include('cashflow._js')
@stop