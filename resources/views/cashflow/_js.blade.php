<script type="text/javascript">
	$('#cash_date').datepicker({
    	autoclose: true,
    	setDate: new Date(),
  	});

  	function save() {
  		$('#formCreateCash').submit();
  	}
  	$('#type_id').change(function(){
  		updateQueryStringParam('type', this.value);
  	});

    function saveCategory(){
    var category_id = $('#category_modal_id').val();
    var category_name = $('#category_modal_name').val();

    $.ajax({
      url: '{{ route('api.web.cash.category.store') }}',
      type: 'POST',
      data: {
        'id': category_id,
        'name': category_name
      },
      success: function(response){
          var id = response.data.category_id;
          var name = response.data.category_name;
          $('select[name=category_id]')
            .append($("<option></option>")
                      .attr("value",category_id)
                      .text(name));
                    $('#createCategoryModal').modal('hide');
      },
      error: function(response){
        // var res = JSON.stringify(response.message.id);
        console.log(response.responseJSON);
        if(response.responseJSON){
          $.each(response.responseJSON.message, function(key, value){
            $("#category_modal_"+key).parent().addClass('has-error');
            $("#category_modal_"+key).after("<span class='help-block'><strong>"+value+"</strong></span>");
          });
        }else{
          return false;
        }
      }
    });
  }

</script>