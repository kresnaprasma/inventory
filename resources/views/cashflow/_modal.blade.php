{{-- Create Modal --}}
<div class="modal fade" id="createCashtypeModal" tabindex="-1" role="dialog" aria-labelledby="Create CashType">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="CreateColor">Create Cash Type</h4>
			</div>
			{!! Form::open(['route'=> 'api.web.inventory.category.store']) !!}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="alert-modal">
							
						</div>

						<div class="form-group">
							{!! Form::label('id', 'Id:') !!}
							{!! Form::text('id', null,['class'=>'form-control', 'id'=>'category_modal_id','maxlength'=>1,'style'=>"text-transform:uppercase"]) !!}
						</div>
						<div class="form-group">
							{!! Form::label('name', 'Name') !!}
							{!! Form::text('name', null,['class'=>'form-control','id'=>'category_modal_name']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveCategory()" class="btn btn-red">Create</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
