<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasingOrderDetail extends Model
{
    protected $fillable = ['purchasing_order_no', 'stock_code', 'detail_qty', 'detail_unit', 'detail_disc', 'detail_total', 'detail_date', 'detail_sku'];

    public function po()
    {
    	return $this->belongsTo('App\PurchasingOrder', 'purchasing_order_no', 'purchasing_order_no');
    }

    public function stock()
    {
    	return $this->belongsTo('App\Stock', 'stock_Code', 'stock_Code');
    }

    public function converts()
    {
        return $this->hasMany('App\InventoryConvert','stock_code','stock_code');
    }
}