<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['bank_code', 'name', 'alias', 'address', 'phone', 'email'];

    public $incrementing = false;

    public function supplier()
    {
    	return $this->hasMany('App\Supplier');
    }
}
