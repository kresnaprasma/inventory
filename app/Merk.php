<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    protected $fillable = ['alias', 'name'];

    public function type()
    {
    	return $this->hasMany('App\Type');
    }

    public function colorant()
    {
    	return $this->hasMany('App\Colorant');
    }

    public function paint()
    {
    	return $this->hasMany('App\Paint');
    }
}
