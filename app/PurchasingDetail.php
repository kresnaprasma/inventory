<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasingDetail extends Model
{
    protected $fillable = ['purchasing_no', 'inventory_code', 'detail_qty', 'detail_unit', 'retur_qty', 'retur_unit', 'detail_amount', 'detail_total', 'detail_date', 'detail_sku'];

    public function purchasing()
    {
    	return $this->belongsTo('App\Purchasing', 'purchasing_no', 'purchasing_no');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code', 'inventory_code');
    }

    public function converts()
    {
    	return $this->hasMany('App\InventoryConvert','inventory_code','inventory_code');
    }
}