<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryLocation extends Model
{
    protected $fillable = ['name'];

    public function stock()
    {
    	return $this->hasMany('App\Inventory','inventory_location_id','id');
    }
}
