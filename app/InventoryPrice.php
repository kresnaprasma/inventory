<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryPrice extends Model
{
    protected $fillable = ['inventory_code', 'price', 'price_type', 'price_date'];

    // public function category()
    // {
    // 	return $this->hasMany('App\InventoryPriceCategory','category_id','id');
    // }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code', 'inventory_code');
    }
}
