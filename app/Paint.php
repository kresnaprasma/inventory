<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paint extends Model
{
    protected $fillable = ['paint_code', 'paint_name', 'merk', 'type', 'base_paint', 'capacity', 'stock_unit', 'barcode', 'paint_description'];

    public $incrementing = false;

    public function details()
    {
    	return $this->hasMany('App\PaintDetail');
    }

    public function merks()
    {
    	return $this->belongsTo('App\Merk', 'merk', 'alias');
    }
}
