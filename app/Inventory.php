<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inventory extends Model
{
    protected $fillable = ['inventory_code', 'category_id', 'merk', 'type', 'serries', 'inventory_name', 'stock_location', 'stock_minimum', 'stock_maximum', 'stock_unit', 'barcode', 'stock_description'];

    public $incrementing = false;

    public function category()
    {
    	return $this->belongsTo('App\InventoryCategory', 'category_id', 'id');
    }

    // public function location()
    // {
    // 	return $this->belongsTo('App\InventoryLocation', 'stock_location', 'id');
    // }

    public function purchasings()
    {
        return $this->hasMany('App\PurchasingDetail', 'inventory_code', 'inventory_code');
    }

    public function converts()
    {
        return $this->hasMany('App\InventoryConvert','inventory_code','inventory_code');
    }

    public function prices()
    {
        return $this->hasMany('App\InventoryPrice','inventory_code','inventory_code');
    }

    public function purchasing_returs()
    {
        return $this->hasMany('App\PurchasingReturDetail','inventory_code','inventory_code');
    }

    public function sales()
    {
        return $this->hasMany('App\SalesDetail', 'inventory_code','inventory_code');
    }

    public function sales_orders()
    {
        return $this->hasMany('App\SalesOrderDetail', 'inventory_code','inventory_code');
    }

    public function scopeOfMaxno($query, $category)
    {
        $queryMax =  $query->select(DB::raw('SUBSTRING(`inventory_code` ,2) AS kd_max'))
            ->where('category_id', $category)
            ->orderBy('inventory_code', 'asc')
            ->get();
        
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return strtoupper($category).$kd_fix;
    }
}
