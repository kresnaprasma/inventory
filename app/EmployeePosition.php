<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeePosition extends Model
{
    protected $fillable = ['position_no', 'name'];

    public $incrementing = false;

    public function employee()
    {
    	return $this->hasMany('App\Employee');
    }
}
