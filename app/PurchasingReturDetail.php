<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasingReturDetail extends Model
{
    protected $fillable = ['retur_no', 'stock_code', 'detail_qty', 'detail_unit', 'detail_date'];

    public function purchasingretur()
    {
    	return $this->belongsTo('App\PurchasingRetur', 'retur_no', 'retur_no');
    }

    public function stock()
    {
    	return $this->belongsTo('App\Stock', 'stock_code', 'stock_code');
    }
}
