<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = ['username','address','phone','city'];

    public function user()
    {
    	return $this->belongsTo('App\User','username','username');
    }
}
