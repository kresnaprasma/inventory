<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasingPayment extends Model
{
    protected $fillable = ['purchasing_no', 'payment_amount', 'payment_date', 'payment_note', 'payment_type'];

    public function purchasing()
    {
    	return $this->hasMany('App\Purchasing', 'purchasing_no', 'purchasing_no');
    }
}