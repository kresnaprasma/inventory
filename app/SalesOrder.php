<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SalesOrder extends Model
{
    protected $fillable = ['sales_order_no', 'sales_order_date', 'sales_order_due_date', 'sales_order_extno', 'sales_order_ref', 'sales_no', 'user_id', 'sales_order_note', 'draft', 'status', 'sales_order_sub_total', 'sales_order_discount_percentage', 'sales_order_discount', 'sales_order_total', 'sales_order_tax', 'sales_order_tax_type'];

    protected $table = 'sales_orders';
    public $incrementing = false;

    public function details()
    {
    	return $this->hasMany('App\SalesOrderDetail', 'sales_order_no', 'sales_order_no');
    }

    public function sales()
    {
    	return $this->hasMany('App\Sales', 'sales_no', 'sales_no');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function scopeOfMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`sales_order_no`,1, 4) AS kd_max'))
            ->where(DB::raw('YEAR(sales_order_date)'), '=', date('Y'))
            ->orderBy('sales_order_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'/SO/'.date('m').'/'.$year;
        
    }
}