<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarMonth extends Model
{
    public $fillable = ['monthfield','name'];
}
