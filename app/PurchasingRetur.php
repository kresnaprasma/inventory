<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PurchasingRetur extends Model
{
    protected $fillable = ['retur_no', 'purchasing_no', 'retur_date', 'user_id', 'retur_note', 'draft'];

    public $incrementing = false;

    public function purchasing()
    {
    	return $this->hasMany('App\Purchasing', 'purchasing_no', 'purchasing_no');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'username');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`retur_no`,1,4) AS kd_max'))
            ->where(DB::raw('YEAR(retur_date)'), '=', date('Y'))
            ->orderBy('retur_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'/PI-RT/'.date('m').'/'.$year;
    }
}
