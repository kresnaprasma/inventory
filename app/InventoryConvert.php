<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryConvert extends Model
{
    protected $fillable = ['inventory_code','unit','size','convert_type'];

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code','inventory_code');
    }
}
