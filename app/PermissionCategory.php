<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionCategory extends Model
{
    protected $fillable = ['name'];

    public function permission()
    {
    	return $this->hasMany('App\Permission','category_id','id');
    }
}
