<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryQuantity extends Model
{
    protected $fillable = ['inventory_code','qty_type','qty'];

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory','inventory_code','inventory_code');
    }
}
