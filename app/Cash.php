<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    public $fillable = ['cash_amount', 'cash_date', 'cash_note', 'cash_type', 'user_id', 'type_id'];

    public $incrementing = false;

    public function user()
    {
    	return $this->belongsTo('App\User','user_id','username');
    }

    public function type()
    {
        return $this->belongsTo('App\CashType');
    }
}
