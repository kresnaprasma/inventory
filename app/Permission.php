<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name','category_id','description'];

    public function category()
    {
    	return $this->belongsTo('App\PermissionCategory','id','category_id');
    }

    public function user()
    {
    	return $this->belongsToMany('App\User');
    }
}