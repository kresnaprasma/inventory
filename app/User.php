<?php

namespace App;

use App\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','name', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function permission()
    {
        return $this->belongsToMany(Permission::class);

    }
    
    public function isSuper()
    {
       if ($this->permission->contains('name', 'super')) {
            return true;
        }
        return false;
    }

    public function hasPermission($permission)
    {
        if ($this->isSuper()) {
            return true;
        }
        if (is_string($permission)) {
            return $this->permission->contains('name', $permission);
        }
        return !! $this->permission->intersect($permission)->count();
    }

    public function assignPermission($permission)
    {
        if (is_string($permission)) {
            $permission = Permission::where('name', $permission)->first();
        }
        return $this->permission()->attach($permission);
    }

    public function revokePermission($permission)
    {
        if (is_string($permission)) {
            $permission = Role::where('name', $permission)->first();
        }
        return $this->permission()->detach($permission);
    }

    public function details()
    {
        return $this->hasMany('App\UserDetail','username','username');
    }
}
