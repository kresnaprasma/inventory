<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesReturDetail extends Model
{
    protected $fillable = ['retur_no', 'stock_code', 'detail_qty', 'detail_unit', 'detail_date'];

    public function salesretur()
    {
    	return $this->belongsTo('App\SalesRetur', 'retur_no', 'retur_no');
    }

    public function stock()
    {
    	return $this->belongsTo('App\Stock', 'stock_code', 'stock_code');
    }
}
