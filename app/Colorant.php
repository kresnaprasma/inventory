<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colorant extends Model
{
    protected $fillable = ['colorant_code', 'name', 'merk', 'unit_stock', 'capacity', 'min_capacity', 'max_capacity', 'description'];

    public function merks()
    {
    	return $this->belongsTo('App\Merk', 'merk', 'alias');
    }
}
