<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesPayment extends Model
{
    protected $fillable = ['sales_no', 'payment_amount', 'payment_date', 'payment_note', 'payment_type'];

    public function sales()
    {
    	return $this->belongsTo('App\Sales', 'sales_no', 'sales_no');
    }
}