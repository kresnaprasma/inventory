<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Purchasing extends Model
{
    protected $fillable = ['purchasing_no', 'purchasing_date', 'purchasing_due_date', 'purchasing_extno', 'purchasing_ref', 'supplier_no', 'user_id', 'purchasing_note', 'draft', 'status', 'purchasing_sub_total', 'purchasing_discount_percentage', 'purchasing_discount', 'purchasing_total', 'purchasing', 'purchasing_tax', 'purchasing_tax_type'];

    public $incrementing = false;

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier','supplier_no','supplier_no');
    }
    
    public function details()
    {
        return $this->hasMany('App\PurchasingDetail','purchasing_no','purchasing_no');
    }

    public function payment()
    {
        return $this->hasMany('App\PurchasingPayment','purchasing_no','purchasing_no');
    }

    public function sales()
    {
        return $this->belongsTo('App\Sales','sales_no','sales_no');
    }

    public function returs()
    {
        return $this->hasOne('App\PurchasingRetur','purchasing_no','purchasing_no');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_no', 'customer_no');
    }
    
    public function scopeOfMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`purchasing_no`,1,4) AS kd_max'))
            ->where(DB::raw('YEAR(purchasing_date)'), '=', date('Y'))
            ->orderBy('purchasing_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'/PI/'.date('m').'/'.$year;
    }
}
