<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryPriceCategory extends Model
{
    protected $fillable = ['name'];

    public function inventoryprice()
    {
    	return $this->hasMany('App\InventoryPrice','category_id','id');
    }
}
