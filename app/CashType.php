<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashType extends Model
{
    protected $fillable = ['name'];

    public $incrementing = false;

    public function cash()
    {
    	return $this->hasMany('App\Cash','type_id','id');
    }
}
