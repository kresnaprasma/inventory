<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sales extends Model
{
    protected $fillable = ['sales_no', 'customer_no', 'user_id', 'sales_date', 'sales_due_date', 'sales_note', 'draft', 'status', 'sales_sub_total', 'sales_discount_percentage', 'sales_discount', 'sales_total', 'sales_tax', 'sales_tax_type', 'active'];

    // protected $table = 'sales';

    public $incrementing = false;

    public function details()
    {
    	return $this->hasMany('App\SalesDetail', 'sales_no', 'sales_no');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'username');
    }

    public function payment()
    {
        return $this->hasMany('App\SalesPayment','sales_no','sales_no');
    }

    public function orders()
    {
        return $this->hasMany('App\SalesOrder','sales_no','sales_no');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'username', 'user_id');
    }

    public function scopeOfMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`sales_no`,1, 4) AS kd_max'))
            ->where(DB::raw('YEAR(sales_date)'), '=', date('Y'))
            ->orderBy('sales_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'/SI/'.date('m').'/'.$year;
    }
}
