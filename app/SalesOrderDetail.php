<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderDetail extends Model
{
    protected $fillable = ['sales_order_no', 'stock_code', 'detail_qty', 'detail_unit', 'detail_amount', 'detail_disc', 'detail_total', 'detail_date', 'detail_sku'];

    protected $table = 'sales_order_details';

    public function sales()
    {
    	return $this->hasMany('App\SalesOrder', 'sales_order_no', 'sales_order_no');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code', 'inventory_code');
    }
}