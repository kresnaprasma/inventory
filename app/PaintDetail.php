<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaintDetail extends Model
{
    protected $fillable = ['paint_code', 'inventory_code', 'detail_qty', 'detail_unit', 'detail_amount', 'detail_disc', 'detail_total', 'detail_date', 'detail_sku'];

    public $incrementing = false;

    public function paint()
    {
    	return $this->belongsTo('App\Paint', 'paint_code', 'paint_code');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code', 'inventory_code');
    }
}
