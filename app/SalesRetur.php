<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesRetur extends Model
{
    protected $fillable = ['retur_no', 'sales_no', 'retur_date', 'user_id', 'retur_note', 'draft'];

    public $incrementing = false;

    public function sales()
    {
    	return $this->hasMany('App\Sales', 'sales_no', 'sales_no');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'username');
    }
}
