<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCategory extends Model
{
    protected $fillable = ['name'];

    public $incrementing = false;

    public function inventory()
    {
    	return $this->hasMany('App\Inventory','category_id','id');
    }
}
