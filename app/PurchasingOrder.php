<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PurchasingOrder extends Model
{
    protected $fillable = ['purchasing_order_no', 'purchasing_order_date', 'purchasing_order_due_date', 'purchasing_order_extno', 'purchasing_order_ref', 'supplier_no', 'user_id', 'purchasing_order_note', 'draft', 'status', 'purchasing_order_sub_total', 'purchasing_order_discount_percentage', 'purchasing_order_discount', 'purchasing_order_total', 'purchasing_order_tax', 'purchasing_order_tax_type', 'sales_no', 'purchasing_no'];

    public $incrementing = false;

    public function details()
    {
    	return $this->hasMany('App\PurchasingOrderDetail', 'purchasing_order_no', 'purchasing_order_no');
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier','supplier_no','supplier_no');
    }

    public function sales()
    {
        return $this->belongsTo('App\Sales','sales_no','sales_no');
    }

    public function scopeOfMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`purchasing_order_no`,1,4) AS kd_max'))
            ->where(DB::raw('YEAR(purchasing_order_date)'), '=', date('Y'))
            ->orderBy('purchasing_order_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'/PO/'.date('m').'/'.$year;    
    }
}