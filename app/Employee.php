<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    protected $fillable = ['nik', 'name', 'alias', 'ktp_no', 'kk_no', 'email', 'address', 'city', 'kelurahan', 'kecamatan', 'province', 'birthday', 'birthplace', 'phone', 'marrital', 'blood_type', 'zipcode', 'gender', 'bank_account', 'npwp', 'bank_branch', 'bank_name', 'job_status', 'job_start', 'job_end', 'bpjs_kesehatan_no', 'bpjs_tenagakerja_no', 'position_no', 'mother_name', 'is_user'];

    public $incrementing = false;

    public function position()
    {
    	return $this->belongsTo('App\EmployeePosition');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select('nik')
            ->orderBy('nik', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->nik;
            }
            $arr2 = range(1001, max($arr1));
            
            $missing = array_diff($arr2, $arr1);
            
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = $tmp;
            }else{
                $kd_fix = reset($missing);
            }
        }
        else{
            $kd_fix = '1001';
        }
        return $kd_fix;
    }
}
