<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = ['alias','name','merk_id'];

    public $incrementing = false;
    
    public function merk()
    {
    	return $this->belongsTo('App\Merk', 'merk_id', 'alias');
    }
}
