<?php

namespace App\Http\Controllers;

use App\PurchasingOrder;
use App\Inventory;
use App\InventoryCategory;
use App\Supplier;
use Illuminate\Http\Request;

class PurchasingOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchasing = PurchasingOrder::all();
        
        return view('purchasing.order.index', compact('purchasing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $inventories = InventoryCategory::with(['inventory'=> function($query){
                        $query->orderBy('inventory_code','asc');
                    }])->get();
        
        $supplier_list = Supplier::all();

        return view('purchasing.order.create', compact('inventories','supplier_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchasing_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $p_date = empty($request->purchasing_date)? null : date('Y-m-d', strtotime($request->purchasing_date));
        $p_due_date = empty($request->purchasing_due_date) ? null : date('Y-m-d', strtotime($request->purchasing_due_date));
        
        $p = new Purchasing();
        $p->id = Uuid::uuid4()->getHex();
        $p->purchasing_order_no = $p->OfMaxno();
        $p->purchasing_order_date = $p_date;
        $p->purchasing_order_due_date = $p_due_date;
        $p->purchasing_order_extno = $request->purchasing_extno;
        $p->purchasing_order_ref = 0;
        $p->supplier_no = $request->supplier_no;
        $p->user_id = auth()->user()->username;
        $p->purchasing_order_note = $request->purchasing_note;
        $p->draft = false;
        $p->status = 'outstanding';
        $p->save();

        $this->purchasingDetail($request, $p);

        $pd_total = PurchasingOrderDetail::where('purchasing_no', $p->purchasing_no)->sum('detail_total');
        $p->purchasing_order_sub_total = $pd_total;
        $p->purchasing_order_discount_percentage =  empty($request->purchasing_discount_percentage) ? 0 : $request->purchasing_discount_percentage;
        $p->purchasing_order_discount = $pd_total * $request->purchasing_discount_percentage/100;
        if ($request->purchasing_tax !=0) {
            $p->purchasing_order_tax = ($pd_total - $p->purchasing_order_discount) * 0.1;
            $p->purchasing_order_tax_type = 'in';
        }else{
            $p->purchasing_order_tax = 0;
        }

        $p->purchasing_order_total = ($pd_total - $p->purchasing_order_discount) + $p->purchasing_order_tax;
        $p->save();

        if (!$p) {
            return redirect()->back()->withInput()->withError('cannot create purchasing order');
        }else{
            return redirect('/purchasing')->with('success', 'Successfully create Purchasing Order');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
