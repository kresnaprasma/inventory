<?php

namespace App\Http\Controllers;

use App\Purchasing;
use App\PurchasingPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PurchasingPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $pi = Purchasing::find($id);
        $payment = PurchasingPayment::where('purchasing_no', $pi->purchasing_no)->get();
        $amount_due = $pi->purchasing_total - $payment->sum('payment_amount');
        
        return view('purchasing.payment.create', compact('pi','payment','amount_due'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_date'=>'required',
            'payment_amount'=>'required',
            'payment_note'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $purchasing = Purchasing::where('purchasing_no', $request->purchasing_no)->first();

        PurchasingPayment::create([
            'payment_date'=>date('Y-m-d', strtotime($request->payment_date)).' '.date('H:i:s'),
            'purchasing_no' => $request->purchasing_no,
            'payment_amount' => intval(str_replace(',', '', $request->payment_amount)),
            'payment_type'=>$request->payment_type,
            'payment_note' => $request->payment_note,
        ]);

        $payment = PurchasingPayment::where('purchasing_no', $purchasing->purchasing_no)->sum('payment_amount');
        if ($payment >= $purchasing->purchasing_total) {
            $purchasing->status = 'completed';
            $purchasing->save();
        }

        $purchasing = Purchasing::where('purchasing_no', $request->purchasing_no)->first();
        
        foreach ($purchasing->details as $pd) {
            $pd->detail_date = $purchasing->purchasing_date;
            $pd->save();
        }
        
        return redirect('purchasing/'.$purchasing->id)->with('success', 'Successfully create Payment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
