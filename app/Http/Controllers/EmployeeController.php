<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeePosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp = Employee::all();
        return view('master.employee.index', compact('emp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee_no = Employee::Maxno();
        $position_list = EmployeePosition::pluck('name', 'id');
        return view('master.employee.create', compact('position_list', 'employee_no'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nik'=>'required|string|max:10',
            'name'=>'required',
            'ktp_no'=>'required|string|unique:employees',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $emp = new Employee();
        $emp->id = Uuid::uuid4()->getHex();
        $emp->nik = Employee::Maxno();
        $emp->name = $request->input('name');
        $emp->alias = $request->input('alias');
        $emp->ktp_no = $request->input('ktp_no');
        $emp->kk_no = $request->input('kk_no');
        $emp->email = $request->input('email');
        $emp->address = $request->input('address');
        $emp->city = $request->input('city');
        $emp->kelurahan = $request->input('kelurahan');
        $emp->kecamatan = $request->input('kecamatan');
        $emp->province = $request->input('province');
        $emp->birthday = $request->input('birthday');
        $emp->birthplace = $request->input('birthplace');
        $emp->marrital = $request->input('marrital');
        $emp->phone = $request->input('phone');
        $emp->blood_type = $request->input('blood_type');
        $emp->zipcode = $request->input('zipcode');
        $emp->gender = $request->input('gender');
        $emp->npwp = $request->input('npwp');
        $emp->job_status = $request->input('job_status');
        $emp->job_start = $request->input('job_start');
        $emp->job_end = $request->input('job_end');
        $emp->bpjs_kesehatan_no = $request->input('bpjs_kesehatan_no');
        $emp->bpjs_tenagakerja_no = $request->input('bpjs_tenagakerja_no');
        $emp->position_no = $request->input('position_no');
        $emp->mother_name = $request->input('mother_name');        
        $emp->bank_account = $request->input('bank_account');        
        $emp->bank_name = $request->input('bank_name');
        $emp->bank_branch = $request->input('bank_branch');
        $emp->is_user = '1';
        // $emp->user_id = auth()->user()->username;
        $emp->save();

        if (!$emp) {
            return redirect()->back()->withInput()->withError('cannot create employee');
        }else{
            return redirect('/master/employee')->with('success', 'Successfully create employee');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Employee::find($id);
        $emp_position = EmployeePosition::pluck('name', 'id');
        return view('master.employee.edit', compact('emp', 'emp_position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'employee_no'=>'required|string|max:10',
            'name'=>'required',
            'no_ktp'=>'required|string|unique:emps',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $emp = Employee::find($id);
        $emp->nik = $request->input('nik');
        $emp->name = $request->input('name');
        $emp->alias = $request->input('alias');
        $emp->ktp_no = $request->input('ktp_no');
        $emp->kk_no = $request->input('kk_no');
        $emp->email = $request->input('email');
        $emp->address = $request->input('address');
        $emp->city = $request->input('city');
        $emp->kelurahan = $request->input('kelurahan');
        $emp->kecamatan = $request->input('kecamatan');
        $emp->province = $request->input('province');
        $emp->birthday = $request->input('birthday');
        $emp->birthplace = $request->input('birthplace');
        $emp->marrital = $request->input('marrital');
        $emp->phone = $request->input('phone');
        $emp->blood_type = $request->input('blood_type');
        $emp->zipcode = $request->input('zipcode');
        $emp->gender = $request->input('gender');
        $emp->npwp = $request->input('npwp');
        $emp->job_status = $request->input('job_status');
        $emp->job_start = $request->input('job_start');
        $emp->job_end = $request->input('job_end');
        $emp->bpjs_kesehatan_no = $request->input('bpjs_kesehatan_no');
        $emp->bpjs_tenagakerja_no = $request->input('bpjs_tenagakerja_no');
        $emp->position_no = $request->input('position_no');
        $emp->mother_name = $request->input('mother_name');        
        $emp->bank_account = $request->input('bank_account');        
        $emp->bank_name = $request->input('bank_name');
        $emp->bank_branch = $request->input('bank_branch');
        $emp->is_user = $request->input('is_user');
        // $emp->user_id = auth()->user()->username;
        $emp->save();

        if (!$emp) {
            return redirect()->back()->withInput()->withError('cannot create employee');
        }else{
            return redirect('/master/employee')->with('success', 'Successfully create employee');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
