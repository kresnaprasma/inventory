<?php

namespace App\Http\Controllers;

use App\PermissionCategory;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return view('master.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = PermissionCategory::with('permission')->get();
        // $outlet = Outlet::pluck('name','outlet_code');

        return view('master.user.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=>'required|string|max:255|unique:users',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password); 
        $user->save();

        $user->permission()->sync($request->permission_id);

        if (!$user) {
            return redirect()->back()->withInput()->withError('cannot create user');
        }else{
            return redirect('/master/user')->with('success', 'Successfully create user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id',$id)->first();
        $permissions = PermissionCategory::with('permission')->get();
        $user_permission = [];
        foreach ($user->permission as $key => $value) {
            $user_permission += [$key =>$value->name];
        }

        // $outlet = Outlet::pluck('name','outlet_code');

        return view('master.user.edit', compact('user','permissions','user_permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::where('id',$id)->first();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->save();

        $user->permission()->sync($request->permission_id);
        
        if (!$user) {
            return redirect()->back()->withInput()->withError('cannot create user');
        }else{
            return redirect('/master/user')->with('success', 'Successfully update user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('master/user')->with('success', 'Successfully delete user');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $user = User::find($value);
            $user->delete();
        }

        return redirect('/master/user')->with('success', 'Successfully delete user');
    }

    public function editPassword($id)
    {
        $user = User::find($id);

        return view('master.user.password', compact('user'));
    }

    public function updatePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'old_password'=> 'required',
            'password' => 'required|confirmed|min:6|max:50|different:old_password',
            'password_confirmation' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::where('id',$id)->first();

        if(!Hash::check($request->old_password, $user->password)){
            return back()->withInput()->withErrors('The specified password does not match the database password');
        }else{
            $user->password = Hash::make($request->password);      
            $user->save();
        }

        if (!$user) {
            return redirect()->back()->withInput()->withError('cannot update user password');
        }else{
            if (auth()->user()->email == $user->email) {
                Auth::logout();
                return redirect('/');
            }else{
                return redirect('/master/user')->with('success', 'Successfully update user password');
            }
        }
    }
}
