<?php

namespace App\Http\Controllers;

use App\Purchasing;
use App\PurchasingDetail;
use App\Inventory;
use App\InventoryCategory;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDF;
use Ramsey\Uuid\Uuid;

class PurchasingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $this->authorize('purchasing.read.modify');
        $begin = $request->input('begin');
        $end = $request->input('end');

        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            // $begin = new \DateTime($now.'01');
            // $end = new \DateTime($now.$d1);
            $year = date('Y-01-01');
            $year_end  = date('Y-12-31');
            $begin = new \DateTime($year);
            $end = new \DateTime($year_end);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->status)) {
            $purchasing = Purchasing::orderBy('purchasing_date', 'desc')
                                    ->whereDate('purchasing_date', '>=', $begin)
                                    ->whereDate('purchasing_date','<=', $end)
                                    ->get();
        }else{
            $purchasing = Purchasing::orderBy('purchasing_date', 'desc')
                                    ->whereDate('purchasing_date', '>=', $begin)
                                    ->whereDate('purchasing_date','<=', $end)
                                    ->where('status', $request->status)
                                    ->get();
        }
        
        
        return view('purchasing.index', compact('purchasing','begin','end'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $inventories = InventoryCategory::with(['inventory'=> function($query){
                        $query->orderBy('inventory_code','asc');
                    }])->get();
        
        $supplier_list = Supplier::all();

        return view('purchasing.create', compact('inventories', 'supplier_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchasing_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $p_date = empty($request->purchasing_date)? null : date('Y-m-d', strtotime($request->purchasing_date));
        $p_due_date = empty($request->purchasing_due_date) ? null : date('Y-m-d', strtotime($request->purchasing_due_date));

        $p = new Purchasing();
        $p->id = Uuid::uuid4()->getHex();
        $p->purchasing_no = $p->OfMaxno();
        $p->purchasing_date = $p_date;
        $p->purchasing_due_date = $p_due_date;
        $p->purchasing_extno = $request->purchasing_extno;
        $p->purchasing_ref = 0;
        $p->supplier_no = $request->supplier_no;
        $p->user_id = auth()->user()->username;
        $p->purchasing_note = $request->purchasing_note;
        $p->draft = false;
        $p->status = 'outstanding';
        $p->save();

        $this->purchasingDetail($request, $p);

        $pd_total = PurchasingDetail::where('purchasing_no', $p->purchasing_no)->sum('detail_total');
        $p->purchasing_sub_total = $pd_total;
        $p->purchasing_discount_percentage =  empty($request->purchasing_discount_percentage) ? 0 : $request->purchasing_discount_percentage;
        $p->purchasing_discount = $pd_total * $request->purchasing_discount_percentage/100;
        if ($request->purchasing_tax !=0) {
            $p->purchasing_tax = ($pd_total - $p->purchasing_discount) * 0.1;
            $p->purchasing_tax_type = 'in';
        }else{
            $p->purchasing_tax = 0;
        }

        $p->purchasing_total = ($pd_total - $p->purchasing_discount) + $p->purchasing_tax;
        $p->save();

        if (!$p) {
            return redirect()->back()->withInput()->withError('cannot create purchasing');
        }else{
            return redirect('/purchasing')->with('success', 'Successfully create Purchasing');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchasing = Purchasing::with('details.inventory.converts')->with('returs.details')->where('id',$id)->first();
        $purchasing_no = $purchasing->purchasing_no;
        
        
        $inventories = InventoryCategory::with(['inventory'=> function($query) use ($purchasing_no){
                            $query->orderBy('inventory_code', 'asc');
                            $query->with(['purchasings'=>function($query) use($purchasing_no){
                                $query->where('purchasing_no', $purchasing_no);
                            }]);
                        }])->get();
        
        $detail = PurchasingDetail::with('inventory.category')->where('purchasing_no', $purchasing_no)->get();
        // return $detail;
        return view('purchasing.show', compact('purchasing','inventories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchasing  = Purchasing::with('details.inventory.converts')->where('id',$id)->first();
        // return $purchasing;
        $inventories = Inventory::orderBy('inventory_code','asc')->get();
        $supplier_list = Supplier::all();

        return view('purchasing.edit', compact('purchasing','inventories','supplier_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'purchasing_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $p_date = empty($request->purchasing_date)? null :date('Y-m-d', strtotime($request->purchasing_date));
        $p_due_date = empty($request->purchasing_due_date) ? null : date('Y-m-d', strtotime($request->purchasing_due_date));

        $p = Purchasing::find($id);
        $p->purchasing_date = $p_date;
        $p->purchasing_due_date = $p_due_date;
        $p->purchasing_extno = $request->purchasing_extno;
        $p->purchasing_ref = 0;
        $p->supplier_no = $request->supplier_no;
        $p->user_id = auth()->user()->username;
        $p->purchasing_note = $request->purchasing_note;
        $p->draft = false;
        if($p->status == 'outstanding'){
            $p->status = 'outstanding';
        }else{
            $p->status = 'complete';
        }        
        $p->save();

        $this->purchasingDetail($request, $p);

        $pd_total = PurchasingDetail::where('purchasing_no', $p->purchasing_no)->sum('detail_total');
        $p->purchasing_sub_total = $pd_total;
        $p->purchasing_discount_percentage =  $request->purchasing_discount_percentage;
        $p->purchasing_discount = $pd_total * $request->purchasing_discount_percentage/100;

        if ($request->purchasing_tax !=0) {
            $p->purchasing_tax = ($pd_total - $p->purchasing_discount) * 0.1;
            $p->purchasing_tax_type = 'in';   
        }else{
            $p->purchasing_tax = 0;
            $p->purchasing_tax_type = '';
        }

        $p->purchasing_total = ($pd_total - $p->purchasing_discount) + $p->purchasing_tax;
        $p->save();

        if (!$p) {
            return redirect()->back()->withInput()->withError('cannot create purchasing');
        }else{
            return redirect('/purchasing')->with('success', 'Successfully create Purchasing');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Purchasing::find($id);

        if (!$p) {
            return redirect()->back()->withInput()->withError('cannot delete purchasing');
        }else{
            $p->delete();
            return redirect('purchasing')->with('success', 'Successfully delete Purchasing');
        }
    }

    public function print($id)
    {
        $purchasing = Purchasing::find($id);
        $purchasing_no = $purchasing->purchasing_no;

        $inventory_category = InventoryCategory::with(['inventory'=> function($query)use($purchasing_no){
                            $query->orderBy('inventory_code', 'asc');
                            $query->with('converts');
                            $query->with(['purchasings'=> function($query) use($purchasing_no){
                                $query->where('purchasing_no', $purchasing_no);
                            }]);
                        }])->get();
        
        $table_row = 30;        
        return view('purchasing.print', compact('inventory_category', 'table_row','purchasing'));
    }

    public function purchasingDetail($request, $purchasing)
    {
        foreach ($request->detail_id as $key => $value) {
            if ($value != '') {
                $pd = PurchasingDetail::find($value);
                $pd->inventory_code = $request->inventory_code[$key];
                $pd->purchasing_no = $purchasing->purchasing_no;
                $pd->detail_qty = intval(str_replace(',','',$request->detail_qty[$key]));
                $pd->detail_unit = $request->detail_unit[$key];
                $pd->detail_amount = intval(str_replace(',','',$request->detail_amount[$key]));
                $pd->detail_total = intval(str_replace(',','',$request->detail_total[$key]));
                $pd->detail_date = null;
                $pd->detail_sku = ' ';
                $pd->save();
            }else{
                if($request->detail_amount[$key]!= null){
                    $detail = [
                        'inventory_code' => $request->inventory_code[$key],
                        'purchasing_no' => $purchasing->purchasing_no,
                        'detail_qty' => intval(str_replace(',','',$request->detail_qty[$key])),
                        'detail_unit' => $request->detail_unit[$key],
                        'detail_amount' => intval(str_replace(',','',$request->detail_amount[$key])),
                        'detail_total' => intval(str_replace(',','',$request->detail_total[$key])),
                        'detail_date' => null,
                        'detail_sku' =>' ',
                    ];
                    PurchasingDetail::create($detail);
                }
            }
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeRetur(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchasing_no'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $p = Purchasing::where('purchasing_no', $request->purchasing_no)->first();

        foreach ($request->detail_id as $key => $value) {
            if ($value != '') {
                $pd = PurchasingDetail::find($value);
                $inventory = Inventory::with('converts')->where('inventory_code', $pd->inventory_code)->first();
                $pd->retur_qty = $request->retur_qty[$key];
                $pd->retur_unit = $request->retur_unit[$key];

                if ($pd->detail_unit != $pd->retur_unit) {
                    foreach ($inventory->converts as $conv) {
                        if ($conv->convert_type == '<'){
                            $to_convert_size = $pd->detail_qty * $conv->size; 
                            $to_retur_size = $to_convert_size - $pd->retur_qty;
                            $to_origin_size = $to_retur_size / $conv->size;
                        }else{
                            $to_convert_size = $pd->detail_qty / $conv->size; 
                            $to_retur_size = $to_convert_size - $pd->retur_qty;
                            $to_origin_size = $to_retur_size * $conv->size;
                        }
                    }
                }else{
                    $to_origin_size = $pd->detail_qty - $pd->retur_qty;   
                }
                $pd->detail_total = $to_origin_size * $pd->detail_amount;
                $pd->save();
            }
        }

        $pd_total = PurchasingDetail::where('purchasing_no', $p->purchasing_no)->sum('detail_total');
        $p->purchasing_sub_total = $pd_total;
        $p->purchasing_discount = $pd_total * $p->purchasing_discount_percentage / 100;

        if ($p->purchasing_tax) {
            $p->purchasing_tax = ($pd_total - $p->purchasing_discount) * 0.1;
            $p->purchasing_tax_type = 'in';
            $p->purchasing_total = ($pd_total - $p->purchasing_discount) + $p->purchasing_tax;
        }else{
            $p->purchasing_tax = 0;
            $p->purchasing_tax_type = '';
            $p->purchasing_total = $pd_total - $p->purchasing_discount;
        }

        $p->save();

        if (!$p) {
            return redirect()->back()->withInput()->withError('cannot create Retur');
        }else{
            return redirect('/purchasing')->with('success', 'Successfully create Retur');
        }
    }

    public function printPDF($id)
    {
        $purchasing = Purchasing::with('details.inventory.converts')->with('returs.details')->where('id',$id)->first();
        $purchasing_no = $purchasing->purchasing_no;   

        $inventories = InventoryCategory::with(['inventory'=> function($query) use ($purchasing_no){
                            $query->orderBy('inventory_code', 'asc');
                            $query->with(['purchasings'=>function($query) use($purchasing_no){
                                $query->where('purchasing_no', $purchasing_no);
                            }]);
                        }])->get();
        
        $detail = PurchasingDetail::with('inventory.category')->where('purchasing_no', $purchasing_no)->get();
        // return $detail;
        
        return view('purchasing.print', compact('purchasing','inventories'));
    }
}
