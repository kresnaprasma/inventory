<?php

namespace App\Http\Controllers;

use App\Cash;
use App\CashType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class CashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');

        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        $cos = Cash::orderBy('cash_date', 'asc')
                    ->whereDate('cash_date', '>=', $begin)
                    ->whereDate('cash_date','<=', $end)
                    ->get();
        
        return view('cashflow.index', compact('cos','begin','end'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cash_type = CashType::pluck('name','id');

        return view('cashflow.create', compact('cash_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cash_amount' => 'required',
            'cash_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $cash = new Cash();
        $cash->id = Uuid::uuid4()->getHex();
        $cash->cash_amount = $request->cash_amount;
        $cash->cash_date = date('Y-m-d H:i:s', strtotime($request->cash_date));
        $cash->cash_note = $request->cash_note;
        $cash->cash_type = $request->cash_type;
        $cash->user_id = auth()->user()->username;
        $cash->type_id = $request->type_id;
        $cash->save();

        if (!$cash) {
            return redirect()->back()->withInput()->withError('cannot create cash');
        }else{
            return redirect('/cash')->with('success', 'Successfully create Cash');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cash = Cash::find($id);

        $cash_type = CashOutletType::pluck('name','id');

        return view('cashflow.edit', compact('cash','cash_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'cash_amount' => 'required',
            'cash_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $cash = Cash::find($id);
        $cash->cash_amount = $request->cash_amount;
        $cash->cash_date = date('Y-m-d H:i:s', strtotime($request->cash_date));
        $cash->cash_note = $request->cash_note;
        $cash->cash_type = $request->cash_type;
        $cash->user_id = auth()->user()->username;
        $cash->type_id = $request->type_id;
        $cash->save();

        if (!$cash) {
            return redirect()->back()->withInput()->withError('cannot create cash');
        }else{
            return redirect('/cash')->with('success', 'Successfully create Cash');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
