<?php

namespace App\Http\Controllers;

use App\PurchasingOrder;
use App\PurchasingOrderDetail;
use App\Sales;
use App\SalesDetail;
use App\Inventory;
use App\InventoryCategory;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');

        if(!$begin && !$end)
        {
            // $now = date('Y-m-');
            // $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            // $begin = new \DateTime($now.'01');
            // $end = new \DateTime($now.$d1);
            $year = date('Y-01-01');
            $year_end  = date('Y-12-31');
            $begin = new \DateTime($year);
            $end = new \DateTime($year_end);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }


        if (empty($request->status)) {
            $sales = Sales::orderBy('sales_date', 'desc')
                                    ->whereDate('sales_date', '>=', $begin)
                                    ->whereDate('sales_date', '<=', $end)
                                    ->get();
        }else{
            $sales = Sales::orderBy('sales_date', 'desc')
                                    ->whereDate('sales_date', '>=', $begin)
                                    ->whereDate('sales_date','<=', $end)
                                    ->where('status',$request->status)
                                    ->get();
        }

        return view('sales.index', compact('sales','begin','end'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stocks = InventoryCategory::with(['inventory'=> function($query){
                        $query->orderBy('inventory_code','asc');
                    }])->get();

        $customer_list = Customer::pluck('name', 'customer_no')->toArray();

        return view('sales.create', compact('stocks', 'customer_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sales_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $sales_due_date = empty($request->sales_due_date) ? null : date('Y-m-d', strtotime($request->sales_due_date));

        $s = new Sales();
        $s->id = Uuid::uuid4()->getHex();
        $s->sales_no = $s->OfMaxno();
        $s->sales_date = date('Y-m-d', strtotime($request->sales_date));
        $s->sales_due_date = $sales_due_date;
        $s->customer_no = $request->customer_no;
        $s->user_id = auth()->user()->username;
        $s->sales_note = $request->sales_note;
        $s->draft = false;
        $s->status = "outstanding";
        $s->save();

        $this->salesDetail($request, $s);

        $sd_total = SalesDetail::where('sales_no', $s->sales_no)->sum('detail_total');

        $s = Sales::where('sales_no', $s->sales_no)->first();
        $s->sales_sub_total = $sd_total;
        $s->sales_discount_percentage =  empty($request->sales_discount_percentage) ? 0 : $request->sales_discount_percentage;
        $s->sales_discount = $sd_total * $request->sales_discount_percentage/100;

        if ($request->sales_tax !=0) {
            $s->sales_tax = ($sd_total - $s->sales_discount) * 0.1;
            $s->sales_tax_type = 'in';
        }else{
            $s->sales_tax = 0;
        }

        $s->sales_total = ($sd_total - $s->sales_discount) + $s->sales_tax;
        $s->save();

        if (!$s) {
            return redirect()->back()->withInput()->withError('cannot create Sales');
        }else{
            return redirect('sales')->with('success', 'Successfully create Sales');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales = Sales::find($id);

        return view('sales.show', compact('sales'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales = Sales::find($id);
        $stocks = InventoryCategory::with(['inventory'=> function($query){
                        $query->orderBy('inventory_code','asc');
                    }])->get();

        return view('sales.edit', compact('sales','stocks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'sales_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $s = Sales::find($id);
        $s->sales_date = date('Y-m-d', strtotime($request->sales_date));
        $s->sales_due_date = date('Y-m-d', strtotime($request->sales_due_date));
        $s->sales_extno = $request->sales_extno;
        $s->sales_ref = 0;
        $s->user_id = auth()->user()->username;
        $s->sales_note = $request->sales_note;
        $s->draft = false;

        if($s->status == 'outstanding'){
            $s->status = 'outstanding';
        }else{
            $s->status = 'complete';
        }        
        $s->save();

        $this->salesDetail($request, $s);

        $sd_total = SalesDetail::where('sales_no', $s->sales_no)->sum('detail_total');
        $s->sales_sub_total = $sd_total;
        $s->sales_discount_percentage =  $request->sales_discount_percentage;
        $s->sales_discount = $sd_total * $request->sales_discount_percentage/100;
        $s->sales_tax = ($sd_total - $s->sales_discount) * 0.1;
        $s->sales_tax_type = 'in';
        $s->sales_total = ($sd_total - $s->sales_discount) + $s->sales_tax;
        $s->save();

        if (!$s) {
            return redirect()->back()->withInput()->withError('cannot create sales');
        }else{
            return redirect('/sales')->with('success', 'Successfully create ales');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function salesDetail($request, $sales)
    {
        foreach ($request->detail_id as $key => $value) {
            if ($value != '') {
                $sd = SalesDetail::find($value);
                $sd->inventory_code = $request->inventory_code[$key];
                $sd->sales_no = $sales->sales_no;
                $sd->detail_qty = intval(str_replace(',','',$request->detail_qty[$key]));
                $sd->detail_unit = $request->detail_unit[$key];
                $sd->detail_amount = intval(str_replace(',','',$request->detail_amount[$key]));
                $sd->detail_total = intval(str_replace(',','',$request->detail_total[$key]));
                $sd->detail_date = null;
                $sd->detail_sku = ' ';
                $sd->save();
            }else{
                if ($request->detail_amount[$key] != null) {
                    $detail = [
                        'inventory_code' => $request->inventory_code[$key],
                        'sales_no' => $sales->sales_no,
                        'detail_qty' => intval(str_replace(',','',$request->detail_qty[$key])),
                        'detail_unit' => $request->detail_unit[$key],
                        'detail_amount' => intval(str_replace(',','',$request->detail_amount[$key])),
                        'detail_total' => intval(str_replace(',','',$request->detail_total[$key])),
                        'detail_date' => null,
                        'detail_sku' =>' ',
                    ];
                    SalesDetail::create($detail);
                }
            }
        }
    }

    public function salesSend($id)
    {
        $sales = Sales::find($id);
        
        if($sales->purchaseOrder->count() == 0){
            $p = new PurchasingOrder();
            $p->id = Uuid::uuid4()->getHex();
            $p->purchasing_order_no  = $p->OfMaxno();
            $p->purchasing_order_date = $sales->sales_date;
            $p->purchasing_order_due_date = $sales->sales_due_date;
            $p->purchasing_order_extno  = $sales->sales_extno;
            $p->purchasing_order_ref = $sales->sales_ref;
            $p->supplier_no = $sales->supplier_no;
            $p->user_id = auth()->user()->username;
            $p->purchasing_order_note = $sales->sales_note;
            $p->draft = false;
            $p->status = 'outstanding';
            $p->sales_no = $sales->sales_no;
            $p->save();

            foreach ($sales->details as $detail) {
                $pd = new PurchasingOrderDetail();
                $pd->inventory_code = $detail->inventory_code;
                $pd->purchasing_order_no = $p->purchasing_order_no;
                $pd->detail_qty = $detail->detail_qty;
                $pd->detail_unit = $detail->detail_unit;
                $pd->detail_amount = $detail->detail_amount;
                $pd->detail_total = $detail->detail_qty * $detail->detail_amount;
                $pd->detail_date = $p->purchasing_order_date;
                $pd->detail_sku = ' ';
                $pd->save();
            }
            
            return redirect('/sales/'.$id)->with('success', 'Successfully Send Stock to Outlet');
        }else{
            $sales_po = $sales->purchaseorder->first();
            // return $sales_po;
            $p = PurchasingOrder::find($sales_po->id);
            $p->purchasing_order_date = $sales->sales_date;
            $p->purchasing_order_due_date = $sales->sales_due_date;
            $p->purchasing_order_extno  = $sales->sales_extno;
            $p->purchasing_order_ref = $sales->sales_ref;
            $p->supplier_no = $sales->supplier_no;
            $p->user_id = auth()->user()->username;
            $p->purchasing_order_note = $sales->sales_note;
            $p->draft = false;
            $p->status = 'outstanding';
            $p->sales_no = $sales->sales_no;
            $p->save();

            foreach ($sales->details as $detail) {
                $pd = PurchasingOrderDetail::where('inventory_code', $detail->inventory_code)->first();
                // $pd->stock_code = $detail->stock_code;
                $pd->purchasing_order_no = $p->purchasing_order_no;
                $pd->detail_qty = $detail->detail_qty;
                $pd->detail_unit = $detail->detail_unit;
                $pd->detail_amount = $detail->detail_amount;
                $pd->detail_total = $detail->detail_qty * $detail->detail_amount;
                $pd->detail_date = $p->purchasing_order_date;
                $pd->detail_sku = ' ';
                $pd->save();
            }
            return redirect('/sales/'.$id)->with('success', 'Successfully Update Send Stock to Outlet');
        }
    }

    public function purchasingStore($request,$sales_no)
    {
        $p = new PurchasingOrder();
        $p->id = Uuid::uuid4()->getHex();
        $p->purchasing_order_no = $p->OfMaxno();
        $p->purchasing_order_date = date('Y-m-d', strtotime($request->sales_date));
        $p->purchasing_order_due_date = date('Y-m-d', strtotime($request->sales_due_date));
        $p->purchasing_order_extno = $request->sales_extno;
        $p->purchasing_order_ref = $request->sales_ref;
        $p->supplier_no = $request->supplier_no;
        $p->user_id = auth()->user()->username;
        $p->purchasing_order_note = $request->sales_note;
        $p->draft = false;
        $p->status = 'outstanding';
        $p->sales_no = $sales_no;
        $p->save();

        foreach ($request->detail_id as $key => $value) {
            if ($value != '') {
                $pd = PurchasingOrderDetail::find($value);
                $pd->inventory_code = $request->inventory_code[$key];
                $pd->purchasing_order_no = $p->purchasing_order_no;
                $pd->detail_qty = intval(str_replace(',','',$request->detail_qty[$key]));
                $pd->detail_unit = $request->detail_unit[$key];
                $pd->detail_amount = intval(str_replace(',','',$request->detail_amount[$key]));
                $pd->detail_total = intval(str_replace(',','',$request->detail_total[$key]));
                $pd->detail_date = $p->purchasing_order_date;
                $pd->detail_sku = ' ';
                $pd->save();
            }else{
                $detail = [
                    'inventory_code' => $request->inventory_code[$key],
                    'purchasing_order_no' => $p->purchasing_order_no,
                    'detail_qty' => intval(str_replace(',','',$request->detail_qty[$key])),
                    'detail_unit' => $request->detail_unit[$key],
                    'detail_amount' => intval(str_replace(',','',$request->detail_amount[$key])),
                    'detail_total' => intval(str_replace(',','',$request->detail_total[$key])),
                    'detail_date' => $p->purchasing_order_date,
                    'detail_sku' =>' ',
                ];
                PurchasingOrderDetail::create($detail);
            }
        }

        $pd_total = PurchasingOrderDetail::where('purchasing_order_no', $p->purchasing_order_no)->sum('detail_total');
        $p->purchasing_order_sub_total = $pd_total;
        $p->purchasing_order_discount_percentage =  empty($request->sales_discount_percentage) ? 0 : $request->sales_discount_percentage;
        $p->purchasing_order_discount = $pd_total * $request->sales_discount_percentage/100;
        $p->purchasing_order_tax = ($pd_total - $p->purchasing_order_discount) * 0.1;
        $p->purchasing_order_tax_type = 'in';
        $p->purchasing_order_total = ($pd_total - $p->purchasing_order_discount) + $p->purchasing_order_tax;
        $p->save();
    }

    public function printPDF($id)
    {
        $sales = Sales::find($id);

        return view('sales.print', compact('sales'));
    }
}
