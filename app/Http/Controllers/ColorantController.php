<?php

namespace App\Http\Controllers;

use App\Merk;
use App\Colorant;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;

class ColorantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colorant = Colorant::all();
        return view('inventory.colorant.index', compact('colorant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $merk_list = Merk::pluck('name', 'id');
        return view('inventory.colorant.create', compact('merk_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'colorant_code'=>'required|string|max:10',
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $colorant = new Colorant();
        $colorant->id = Uuid::uuid4()->getHex();
        $colorant->colorant_code = $request->input('colorant_code');
        $colorant->name = $request->input('name');
        $colorant->merk = $request->input('merk');
        $colorant->unit_stock = $request->input('unit_stock');
        $colorant->min_capacity = $request->input('min_capacity');
        $colorant->max_capacity = $request->input('max_capacity');
        $colorant->capacity = $request->input('capacity');
        $colorant->description = $request->input('description');
        // $supplier->user_id = auth()->user()->username;
        $colorant->save();

        if (!$colorant) {
            return redirect()->back()->withInput()->withError('cannot create colorant');
        }else{
            return redirect('/colorant')->with('success', 'Successfully create colorant');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
