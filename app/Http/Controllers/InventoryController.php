<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\InventoryCategory;
use App\InventoryPrice;
use App\InventoryConvert;
use App\InventoryQuantity;
use App\PurchasingDetail;
use App\Merk;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = empty($request->date)? date('m/d/Y') : date('m/d/Y', strtotime($request->date));
        $inventories = Inventory::orderBy('inventory_code','asc')->get();

        return view('inventory.index', compact('inventories','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category_list = InventoryCategory::pluck('name','id', 'asc')->toArray();
        $merk_list = Merk::pluck('name', 'alias')->toArray();

        $qty_min = '';
        $qty_max = '';
        $purchasing_price = 0;
        $purchasing_price_convert = 0;
        $purchasing_date = '';
        $purchasing_date_convert = '';

        $selling_price = 0;
        $selling_price_convert = 0;
        $selling_date = '';
        $selling_date_convert = '';

        $distribution_price = 0;
        $distribution_price_convert = 0;
        $distribution_date = '';
        $distribution_date_convert = '';
        
        
        return view('inventory.create', compact('category','category_list','qty_min','qty_max','purchasing_price','purchasing_date','selling_price','selling_date','purchasing_price_convert','purchasing_date_convert','selling_price_convert','selling_date_convert', 'distribution_price', 'distribution_price_convert', 'distribution_date', 'distribution_date_convert', 'merk_list', 'type_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
         $validator = Validator::make($request->all(), [
            'inventory_code' => 'required|string|unique:inventories',
            'inventory_name'=>'required|string',
            'stock_unit'=>'required|string',
            'convert_size'=>'numeric',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $qty_max = empty($request->qty_max) ? 0 : $request->qty_max;
        $qty_min = empty($request->qty_min) ? 0 : $request->qty_min;
        
        $invent = new Inventory();
        $invent->id = Uuid::uuid4()->getHex();
        $invent->inventory_code = $invent->OfMaxno($request->category_id);
        $invent->category_id = strtoupper($request->category_id);
        $invent->merk = $request->merk;
        $invent->type = $request->type;
        $invent->serries = $request->serries;
        $invent->inventory_name = $request->inventory_name;
        $invent->stock_minimum = $request->stock_minimum;
        $invent->stock_maximum = $request->stock_maximum;
        $invent->stock_location = $request->stock_location;
        $invent->stock_unit = strtolower($request->stock_unit);
        $invent->barcode = $request->barcode;
        $invent->stock_description = $request->stock_description;
        $invent->save();

        if($request->purchasing_price_unit != '' || $request->purchasing_price_unit != 0){
            InventoryPrice::create([
                'inventory_code' => $invent->inventory_code,
                'price'=> $request->purchasing_price_unit,
                'price_type'=>'purchasing',
                'price_unit'=>'retail',
                'price_date'=>$request->purchasing_date_unit,
            ]);
        }
        if($request->selling_price_unit != '' || $request->selling_price_unit != 0){
            InventoryPrice::create([
                'inventory_code' => $invent->inventory_code,
                'price'=> $request->selling_price_unit,
                'price_type'=>'selling_retail',
                'price_unit'=>'retail',
                'price_date'=>$request->selling_date_unit,
            ]);
        }
        if($request->distribution_price_unit != '' || $request->distribution_price_unit != 0){
            InventoryPrice::create([
                'inventory_code' => $invent->inventory_code,
                'price'=> $request->distribution_price_unit,
                'price_type'=>'selling_distribution',
                'price_unit'=>'retail',
                'price_date'=>$request->distribution_date_unit,
            ]);
        }

        if($request->purchasing_price_unit_convert != '' || $request->purchasing_price_unit_convert != 0){
            InventoryPrice::create([
                'inventory_code' => $invent->inventory_code,
                'price'=> $request->purchasing_price_unit_convert,
                'price_type'=>'purchasing',
                'price_unit'=>'pack',
                'price_date'=>$request->purchasing_date_unit_convert,
            ]);
        }
        if($request->selling_price_unit_convert != '' || $request->selling_price_unit_convert != 0){
            InventoryPrice::create([
                'inventory_code' => $invent->inventory_code,
                'price'=> $request->selling_price_unit_convert,
                'price_type'=>'selling_retail',
                'price_unit'=>'pack',
                'price_date'=>$request->selling_date_unit_convert,
            ]);
        }
        if($request->distribution_price_unit_convert != '' || $request->distribution_price_unit_convert != 0){
            InventoryPrice::create([
                'inventory_code' => $invent->inventory_code,
                'price'=> $request->distribution_price_unit_convert,
                'price_type'=>'selling_distribution',
                'price_unit'=>'pack',
                'price_date'=>$request->distribution_date_unit_convert,
            ]);
        }
        

        if($request->stock_minimum != ''){
            InventoryQuantity::create([
                'inventory_code'=>$invent->inventory_code,
                'qty_type'=>'min',
                'qty'=>$qty_min,
            ]);
        }elseif($request->stock_maximum != ''){
            InventoryQuantity::create([
                'inventory_code'=>$invent->inventory_code,
                'qty_type'=>'max',
                'qty'=>$qty_max,
            ]);
        }
        if($request->convert_unit != '' && $request->convert_size != ''){
            InventoryConvert::create([
                'inventory_code'=>$invent->inventory_code,
                'unit'=> strtolower($request->convert_unit),
                'size'=>$request->convert_size,
                'convert_type'=> '>'
            ]);
        }

        if (!$invent) {
            return redirect()->back()->withInput()->withError('cannot create inventory');
        }else{
            return redirect('/inventory/'.$invent->id.'/edit')->with('success', 'inventory has been created!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');

        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        $invent = Inventory::find($id);      
        $purchasing_price = '';
        $purchasing_date = '';
        $selling_price = '';
        $selling_date = '';
        $distribution_price = '';
        $distribution_date = '';

        $qty_min = '';
        $qty_max = '';

        $purchasing_price_convert = '';
        $purchasing_date_convert = '';
        $selling_price_convert = '';
        $selling_date_convert = '';
        $distribution_price_convert = '';
        $distribution_date_convert = '';

        $category_list = InventoryCategory::pluck('name','id')->toArray();
        $merk_list = Merk::pluck('name', 'alias')->toArray();

        return view('inventory.edit', compact('invent','category','category_list','purchasing_price','selling_price', 'distribution_price','purchasing_date','selling_date', 'distribution_date', 'qty_min','qty_max','begin','end','purchasing_price_convert','purchasing_date_convert','selling_price_convert','selling_date_convert', 'distribution_price_convert', 'distribution_date_convert', 'merk_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'inventory_name'=>'required|string',
            'stock_unit'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $qty_max = empty($request->qty_max) ? 0 : $request->qty_max;
        $qty_min = empty($request->qty_min) ? 0 : $request->qty_min;

        $invent = Inventory::find($id);
        $invent->inventory_name = $request->inventory_name;
        $invent->stock_minimum = $request->stock_minimum;
        $invent->stock_location = $request->stock_location;
        $invent->stock_unit = strtolower($request->stock_unit);
        $invent->barcode = $request->barcode;
        $invent->stock_description = $request->stock_description;
        // $stock->user_id = auth()->user()->username;
        $invent->save();

            // Purchasing stock_unit
            $this->stock_prices(
                $invent->stock_unit, 
                $invent->inventory_code,
                'purchasing', 
                $request->purchasing_date_unit,
                $request->purchasing_price_unit
             );
            // Purchasing unit converts
            $this->stock_prices(
                $request->convert_unit, 
                $invent->inventory_code,
                'purchasing', 
                $request->purchasing_date_unit_convert, 
                $request->purchasing_price_unit_convert);

            // Seeling stock_unit
            $this->stock_prices(
                $invent->stock_unit, 
                $invent->inventory_code,
                'selling', 
                $request->selling_date_unit, 
                $request->selling_price_unit);

            $this->stock_prices(
                $request->convert_unit, 
                $invent->inventory_code,
                'selling', 
                $request->selling_date_unit_convert, 
                $request->selling_price_unit_convert);



            // $purchase_price = StockPrice::where('price', $request->purchasing_price)
            //                                 ->where('price_date', $request->purchasing_date)
            //                                 ->where('price_type', 'purchasing')
            //                                 ->where('stock_code', $stock->stock_code)
            //                                 ->where('outlet_code', $request->outlet_code)
            //                                 ->first();
            // if($purchase_price){
            //     $purchase_price->price = $request->purchasing_price;
            //     $purchase_price->price_date = $request->purchasing_date;
            //     $purchase_price->save();
            // }else{
            //     if (!empty($request->purchasing_price)) {
            //         StockPrice::create([
            //             'stock_code' => $stock->stock_code,
            //             'price'=> $request->purchasing_price,
            //             'price_type'=>'purchasing',
            //             'price_date'=>$request->purchasing_date,
            //             'outlet_code'=>$request->outlet_code,
            //         ]);
            //     }
            // }

            // $selling_price = StockPrice::where('price', $request->selling_price)
            //                                 ->where('price_date', $request->selling_date)
            //                                 ->where('price_type', 'selling')
            //                                 ->where('stock_code', $stock->stock_code)
            //                                 ->first();
            // if($selling_price){
            //     $selling_price->price = $request->selling_price;
            //     $selling_price->price_date = $request->selling_date;
            //     $selling_price->save();
            // }else{
            //     if (!empty($request->selling_price)) {
            //         StockPrice::create([
            //             'stock_code' => $stock->stock_code,
            //             'price'=> $request->selling_price,
            //             'price_type'=>'selling',
            //             'price_date'=>$request->selling_date,
            //             'outlet_code'=>$request->outlet_code,
            //         ]);
            //     }
            // }

            if ($qty_min > 0) {
                $min = InventoryQuantity::where('inventory_code', $invent->inventory_code)
                                    ->where('qty_type', 'min')
                                    ->where('qty', $qty_min)
                                    ->first();

                if($min){
                    $min->qty = $qty_min;
                    $min->save();
                }else{
                    InventoryQuantity::create([
                        'inventory_code'=>$invent->inventory_code,
                        'qty_type'=>'min',
                        'qty'=>$qty_min
                    ]);
                }
            }

            if ($qty_max > 0) {
                $max = InventoryQuantity::where('inventory_code', $invent->inventory_code)
                                    ->where('qty_type', 'max')
                                    ->where('qty', $qty_max)
                                    ->first();

                if($max){
                    $max->qty = $qty_max;
                    $max->save();
                }else{
                    InventoryQuantity::create([
                        'inventory_code'=>$invent->inventory_code,
                        'qty_type'=>'max',
                        'qty'=>$qty_max
                    ]);
                }
            }

        if($invent->converts->count() > 0){
            $c = InventoryConvert::find($invent->converts[0]->id);
            $c->unit = strtolower($request->convert_unit);
            $c->size = $request->convert_size;
            $c->convert_type = '>';
            $c->save();   
        }else{
            InventoryConvert::create([
                'inventory_code'=>$invent->inventory_code,
                'unit'=> strtolower($request->convert_unit),
                'size'=>$request->convert_size,
                'convert_type'=> '>'
            ]);
            
        }

        if (!$invent) {
            return redirect()->back()->withInput()->withError('cannot create inventory');
        }else{
            return redirect('/inventory')->with('success', 'Stock has been updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stock_prices($price_unit,$inventory_code, $type, $date, $price)
    {
        $inventory_price = InventoryPrice::where('price', $price)
                ->where('price_date', $date)
                ->where('price_type', $type)
                ->where('inventory_code', $inventory_code)
                ->where('price_unit',$price_unit)
                ->first();
        
        if($inventory_price){
            $inventory_price->price = $price;
            $inventory_price->price_date = $date;
            $inventory_price->price_unit = $price_unit;
            $inventory_price->save();
        }else{
            if(!empty($price)){
                InventoryPrice::create([
                    'inventory_code' => $inventory_code,
                    'price'=> $price,
                    'price_type'=>$type,
                    'price_date'=>$date,
                    'price_unit'=>$price_unit,
                ]);
            }
        }
    }
}
