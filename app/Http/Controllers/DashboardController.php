<?php

namespace App\Http\Controllers;

use App\Purchasing;
use App\Sales;
use App\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }
    
        
        $total_purchasing = Purchasing::where('status','completed')
                                        ->whereDate('purchasing_date', '>=',$begin->format('Y-m-d'))
                                        ->whereDate('purchasing_date', '<=',$end->format('Y-m-d'))
                                        ->sum('purchasing_total');

        $total_purchasing_today = Purchasing::where('status','completed')
                                        ->whereDate('purchasing_date',$today)
                                        ->sum('purchasing_total');

        $total_purchasing_outstanding = Purchasing::where('status','outstanding')
                                        ->whereDate('purchasing_date', '>=',$begin->format('Y-m-d'))
                                        ->whereDate('purchasing_date', '<=',$end->format('Y-m-d'))
                                        ->sum('purchasing_total');

        $total_sales = Sales::whereDate('sales_date', '>=',$begin->format('Y-m-d'))
                        ->whereDate('sales_date', '<=',$end)
                        ->sum('sales_total');

        $total_sales_daily = Sales::whereDate('sales_date',date('Y-m-d'))->sum('sales_total');

        $total_visitor = Sales::whereDate('sales_date', '>=', $begin->format('Y-m-d'))
                ->whereDate('sales_date','<=',$end->format('Y-m-d'))
                ->sum('customer_no');

        // $total_cash_out = CashOutlet::where('cash_date','>=', $begin->format('Y-m-d'))
        //                     ->whereDate('cash_date','<=',$end->format('Y-m-d'))
        //                     ->whereDate('cash_type', 'out')
        //                     ->sum('cash_amount');

        // $total_cash_out_chart = CashOutletType::with(['cash'=>function($query) use($begin, $end) {
        //                             $query->whereDate('cash_date','>=', $begin->format('Y-m-d'));
        //                             $query->whereDate('cash_date','<=',$end->format('Y-m-d'));
        //                             $query->where('cash_type', 'out');
        //                         }])->get();

        // $opex = CashOutlet::whereDate('cash_date','>=', $begin->format('Y-m-d'))
        //                     ->whereDate('cash_date','<=',$end->format('Y-m-d'))
        //                     ->where('cash_type', 'out')
        //                     ->sum('cash_amount');

        $total_average_sales = ($total_sales == 0 || $total_visitor==0) ? 0 : $total_sales / $total_visitor;

        $percentage_purchasing = ($total_purchasing == 0) ? 0 : ($total_purchasing / $total_sales) * 100;
        $gross_profit = $total_sales - $total_purchasing;

        $percentage_gross_profit = ($total_sales == 0) ? 0 : ($gross_profit / $total_sales)  * 100;
        // $percentage_nett_profit = ($total_sales == 0) ? 0 : (($gross_profit - $opex) / $total_sales) * 100;
        
        // return $sales_price_category;
        return view('dashboard', compact('begin','end','total_purchasing','total_purchasing_outstanding','total_sales','total_sales_daily', 'total_cash_out','total_visitor','total_cash_out_chart','total_average_sales','percentage_purchasing','percentage_gross_profit','percentage_nett_profit','sales_price_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sumCash($data)
    {
        return $data;
        var_dump($data);
        $cash = [];
        $sum = 0;
        foreach ($data as $key => $value) {
            foreach ($value['cash'] as $key => $value2) {
                $sum += $value2['cash_amount'];
            }
        }

        return $sum;
    }
}
