<?php

namespace App\Http\Controllers;

use App\SalesOrder;
use App\SalesOrderDetail;
use App\InventoryCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class SalesOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = SalesOrder::orderBy('sales_order_no', 'desc')->get();

        return view('sales.order.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cat_am = InventoryCategory::with(['inventory'=> function($query){
                        $query->orderBy('inventory_code', 'asc');
                        $query->with('converts');
                    }])->where('id','AM')->first();
        
        $cat_ct = InventoryCategory::with(['inventory'=> function($query){
                        $query->limit(30);
                        $query->orderBy('inventory_code', 'asc');
                        $query->with('converts');
                    }])->where('id','CT')->first();

        $cat_at = InventoryCategory::with(['inventory'=> function($query){
                        $query->limit(30);
                        $query->offset(30);
                        $query->orderBy('inventory_code', 'asc');
                        $query->with('converts');
                    }])->where('id','AT')->first();

        return view('sales.order.create', compact('cat_am','cat_ct','cat_at'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sales_order_date'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $s = new SalesOrder();
        $s->id = Uuid::uuid4()->getHex();
        $s->sales_order_no = $s->Maxno();
        $s->sales_order_date = date('Y-m-d', strtotime($request->sales_order_date));
        $s->user_id = auth()->user()->username;
        $s->sales_order_note = $request->sales_order_note;
        $s->draft = false;
        $s->status = 'on progress';
        $s->save();

        foreach ($request->detail_qty as $key => $value) {
            if ($value != 0) {
                $so = new SalesOrderDetail();
                $so->sales_order_no = $s->sales_order_no;
                $so->inventory_code = $request->inventory_code[$key];;
                $so->detail_qty = $value;
                $so->detail_unit = $request->detail_unit[$key];
                $so->detail_date = $s->sales_order_date;
                $so->save();
            }
        }

        if (!$s) {
            return redirect()->back()->withInput()->withError('cannot create Sales');
        }else{
            return redirect('sales/order')->with('success', 'Successfully create Sales');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales = SalesOrder::find($id);
        $sales_order_no = $sales->sales_order_no;

        $inventory_category = InventoryCategory::with(['stocks'=> function($query)use($sales_order_no){
                            $query->orderBy('inventory_code', 'asc');
                            $query->with('converts');
                            $query->with(['sales_orders'=> function($query) use($sales_order_no){
                                $query->where('sales_order_no', $sales_order_no);
                            }]);
                        }])->get();

        $table_row = 30;

        return view('sales.order.show', compact('sales','inventory_category','table_row'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
