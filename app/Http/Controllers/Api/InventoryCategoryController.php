<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\InventoryCategoryPost;
use App\Inventory;
use App\InventoryCategory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class InventoryCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat = InventoryCategory::all();
        return $cat;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|string|max:2|unique:inventory_categories',
            'name'=>'required|string',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 

            return Response::json([
                'error'=>true,
                'message'=> $messages,
                'code'=> 500
            ], 404);
        }

        $category = new InventoryCategory();
        $category->id = strtoupper($request->id);
        $category->name = strtoupper($request->name);
        $category->save();

        if (!$category) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Inventory Category',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Inventory Category',
                'code'=>200,
                'data'=>$this->toJS($category)
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function toJS($category)
    {
        return [
            'category_id' => strtoupper($category['id']),
            'category_name' => strtoupper($category['name'])
        ];
    }
}
