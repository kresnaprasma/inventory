<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Inventory;
use App\InventoryDiscount;
use App\InventoryPrice;
use App\InventoryConvert;
use App\InventoryQuantity;
use App\PurchasingDetail;
use App\Type;
use App\Merk;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inventory::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function selector($id)
    {
        $type_list = Type::where('merk_id', $id)->pluck('name', 'alias');
        return json_encode($type_list);
    }

    public function selectoredit($id)
    {
        $type_edit = Type::where('name', $id)->pluck('name', 'alias');
        return json_encode($type_edit);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $price_type = $request->price;
        
        $date = empty($request->date) ? date('Y-m-d') : $request->date;

        $inventory = Inventory::with(['prices'=>function($query) use ($request, $date, $price_type){
                $query->whereDate('created_at','<=',$date);
                $query->orderBy('created_at','desc');
                $query->where('price_type', $price_type);
            }])
            ->with('converts')
            ->where('inventory_code',$id)->first();

            $inventory = Inventory::with('converts')
                        ->where('inventory_code',$id)
                        ->first();
                    
        if (!$inventory) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot get Inventory',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly get Inventory',
                'code'=>200,
                'data'=>$inventory
            ], 200);
        }
    }

    public function inventoryNo($id)
    {
        $inventory_no = Inventory::OfMaxno($id);

        if (!$inventory_no) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Inventory Category',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Inventory Category',
                'code'=>200,
                'data'=>$inventory_no
            ], 200);
        }
    }

    public function getConverter($id)
    {
        $convert = InventoryConvert::where('inventory_code', $id)->get();

        return $convert;
    }

    public function storeConverter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_code'=> 'required|string',
            'unit' => 'required|string',
            'size' => 'required|string',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 

            return Response::json([
                'error'=>true,
                'message'=> $messages,
                'code'=> 500
            ], 404);
        }

        $convert = new InventoryConvert();
        $convert->inventory_code = $request->inventory_code;
        $convert->unit = $request->unit;
        $convert->size = $request->size;
        $convert->convert_type = $request->convert_type;
        $convert->save();

        if (!$convert) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Inventory Convert',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Inventory Convert',
                'code'=>200,
                'data'=>$convert,
            ], 200);
        }
    }

    public function getPrice(Request $request,$id)
    {
        $price = InventoryPrice::where('inventory_code', $id)
                        ->orderBy('price_date','desc')
                        ->orderBy('price_unit','desc')
                        ->get();

        return $price;
    }

    public function storePrice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_code'=> 'required|string',
            'price' => 'required|string',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 

            return Response::json([
                'error'=>true,
                'message'=> $messages,
                'code'=> 500
            ], 404);
        }

        // 'stock_code','price','outlet_code'
        $price = new InventoryPrice();
        $price->inventory_code = $request->inventory_code;
        $price->price = $request->price;
        $price->price_unit = $request->price_unit;
        $price->price_date = $request->price_date;
        $price->save();

        if (!$price) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Inventory Price',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Inventory Price',
                'code'=>200,
                'data'=>$price,
            ], 200);
        }
    }

    public function getDiscount($id)
    {
        $disc = InventoryDiscount::where('inventory_code', $id)->get();

        return $disc;
    }

    public function storeDiscount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_code'=> 'required|string',
            'discount' => 'required|string',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 

            return Response::json([
                'error'=>true,
                'message'=> $messages,
                'code'=> 500
            ], 404);
        }

        $disc = new InventoryDiscount();
        $disc->inventory_code = $request->inventory_code;
        $disc->discount = $request->discount;
        $disc->save();

        if (!$disc) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Inventory Discount',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Inventory Discount',
                'code'=>200,
                'data'=>$disc,
            ], 200);
        }
    }

    public function getQuantity($id)
    {
        $qty = InventoryQuantity::where('inventory_code', $id)->get();

        return $qty;
    }

    public function storeQuantity(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_code'=> 'required|string',
            'outlet_code' => 'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 

            return Response::json([
                'error'=>true,
                'message'=> $messages,
                'code'=> 500
            ], 404);
        }
        
        if ($request->qty_min != '') {
            $quantity = new InventoryQuantity();
            $quantity->inventory_code = $request->inventory_code;
            $quantity->qty_type = 'min';
            $quantity->qty = $request->qty_min;
            $quantity->save();
        }

        if ($request->qty_max != '') {
            $quantity2 = new InventoryQuantity();
            $quantity2->inventory_code = $request->inventory_code;
            $quantity2->qty_type = 'max';
            $quantity2->qty = $request->qty_max;
            $quantity2->save();
        }

        if (!$quantity) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Inventory Quantity',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Inventory Quantity',
                'code'=>200,
                'data'=>$quantity,
            ], 200);
        }
    }

    public function chartPurchasingStock(Request $request)
    {   
        $begin = $request->input('begin');
        $end = $request->input('end');
        $inventory = $request->inventory;

        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        $inventory_code = PurchasingDetail::select(
                        'calendars.datefield as DATE',
                        DB::raw('IFNULL(SUM(CASE WHEN purchasing_details.inventory_code="'.$inventory.'" THEN detail_qty ELSE 0 END),0) as total_qty'))
                ->rightJoin('calendars', DB::raw("DATE_FORMAT(purchasing_details.detail_date, '%Y-%m-%d')"), '=', 'calendars.datefield')
                ->whereBetween('calendars.datefield', [$begin, $end])
                ->groupBy('calendars.datefield')
                ->get();

        return $inventory_code;
    }
}
