<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\PurchasingOrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PurchasingOrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pod = PurchasingOrderDetail::find($id);
        
        if (!$pod) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot delete Purchasing',
                'code'=> 500
            ], 500);
        }else{
            $pod->delete();
            return Response::json([
                'error'=>false,
                'message'=>'Successfully delete Purchasing',
                'data'=> $pod
            ], 200);
        }
    }
}
