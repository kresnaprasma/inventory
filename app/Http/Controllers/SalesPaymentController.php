<?php

namespace App\Http\Controllers;

use App\Purchasing;
use App\PurchasingPayment;
use App\Sales;
use App\SalesPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SalesPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $sales = Sales::find($id);

        $payment = SalesPayment::where('sales_no', $sales->sales_no)->get();

        if ($payment) {
            $amount_due = $sales->sales_total - $payment->sum('payment_amount');
        }else{
            $amount_due = 0;
        }

        return view('sales.payment.create', compact('sales','payment','amount_due'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_date'=>'required',
            'payment_amount'=>'required',
            'payment_note'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $sales = Sales::where('sales_no', $request->sales_no)->first();
        
        SalesPayment::create([
            'payment_date'=>date('Y-m-d', strtotime($request->payment_date)).' '.date('H:i:s'),
            'sales_no' => $request->sales_no,
            'payment_amount' => intval(str_replace(',', '', $request->payment_amount)),
            'payment_type'=>$request->payment_type,
            'payment_note' => $request->payment_note,
        ]);

        $payment = SalesPayment::where('sales_no', $sales->sales_no)->sum('payment_amount');
        if ($payment >= $sales->sales_total) {
            $sales->status = 'completed';
            $sales->save();
        }

        foreach ($sales->details as $sd) {
            $sd->detail_date = $sales->sales_date;
            $sd->save();
        }

        // $purchasing_no = $sales->purchasings()->first()->purchasing_no;

        // $this->purchasingPayment($request, $purchasing_no);

        return redirect('sales/'.$sales->id)->with('success', 'Successfully create Payment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function purchasingPayment($request, $purchasing_no)
    {
        $purchasing = Purchasing::where('purchasing_no', $purchasing_no)->first();

        PurchasingPayment::create([
            'payment_date'=>date('Y-m-d', strtotime($request->payment_date)).' '.date('H:i:s'),
            'purchasing_no' => $purchasing_no,
            'payment_amount' => intval(str_replace(',', '', $request->payment_amount)),
            'payment_type'=>$request->payment_type,
            'payment_note' => $request->payment_note,
        ]);

        $payment = PurchasingPayment::where('purchasing_no', $purchasing_no)->sum('payment_amount');
        if ($payment >= $purchasing->purchasing_total) {
            $purchasing->status = 'completed';
            $purchasing->save();
        }
    }
}
