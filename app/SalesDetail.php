<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
    protected $fillable = ['sales_no', 'inventory_code', 'detail_qty', 'detail_unit', 'detail_amount', 'detail_disc', 'detail_total', 'detail_date', 'detail_sku'];

    protected $table = 'sales_details';

    public function sales()
    {
    	return $this->hasMany('App\Sales', 'sales_no', 'sales_no');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code', 'inventory_code');
    }
}
