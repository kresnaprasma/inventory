<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryDiscount extends Model
{
    protected $fillable = ['inventory_code', 'category_id', 'discount_type', 'discount', 'discount_date'];

    public function category()
    {
    	return $this->hasMany('App\InventoryPriceCategory','category_id','id');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code', 'inventory_code');
    }
}
