<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryPicture extends Model
{
    protected $fillable = ['inventory_code','filename','original_name','filetype','filesize'];
    
    public $incrementing = false;
    
    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'inventory_code','inventory_code');
    }
}
