<?php

use App\Permission;
use App\Role;
use App\User;
use App\UserDetail;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();

        App\PermissionCategory::insert([
            ['name'=>'super','created_at' => $now, 'updated_at' => $now],
            ['name'=>'master','created_at' => $now, 'updated_at' => $now],
            ['name'=>'purchasing','created_at' => $now, 'updated_at' => $now],
            ['name'=>'stock','created_at' => $now, 'updated_at' => $now],
        ]);

        App\Permission::insert([
            ['name' => 'super','category_id'=>'1','description'=>'Full Access','created_at' => $now, 'updated_at' => $now],

            // Master User
        	['name' => 'user.read.modify','category_id'=>'2','description'=>'User (Read + modify)','created_at' => $now, 'updated_at' => $now],
            ['name' => 'user.readonly','category_id'=>'2','description'=>'User Read Only','created_at' => $now, 'updated_at' => $now],
            ['name' => 'User Manajemen(delete)','category_id'=>'2','description'=>'User delete','created_at' => $now, 'updated_at' => $now],

            // Stock
            ['name' => 'stock.item.read.modify','category_id'=>'4','description'=>'Stock Item (Read + modify)','created_at' => $now, 'updated_at' => $now],
            ['name' => 'stock.item.readonly','category_id'=>'4','description'=>'Stock Item Read Only','created_at' => $now, 'updated_at' => $now],
            ['name' => 'Stock.item.delete','category_id'=>'4','description'=>'Stock Item (delete)','created_at' => $now, 'updated_at' => $now],

            // Purchasing
            ['name' => 'purchasing.read.modify','category_id'=>'3','description'=>'Purchasing(Read + modify)','created_at' => $now, 'updated_at' => $now],
            ['name' => 'purchasing.readonly','category_id'=>'3','description'=>'Purchasing (Read Only)','created_at' => $now, 'updated_at' => $now],
            ['name' => 'purchasing.delete','category_id'=>'3','description'=>'Purchasing(delete)','created_at' => $now, 'updated_at' => $now],            
        ]);

        $super = User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'email' => 'admin@anekabangunan.com',
            'password' => bcrypt('123456789'),   
        ]);

        $super->assignPermission('super');
        
        $super2 = User::create([
            'name' => 'Kresna',
            'username' => 'kresna',
            'email' => 'kresnaprasmadewa@gmail.com',
            'password' => bcrypt('123456789'),
        ]);

        $super2->assignPermission('user.read.modify');
    }
}
