<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();

        App\InventoryCategory::insert([
            ['id'=>'CL', 'name'=>'COLORANT', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'AT', 'name'=>'ALAT TUKANG', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'KR', 'name'=>'KERAMIK', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'AM', 'name'=>'ALAT MANDI', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'KS', 'name'=>'KERAN & SELANG STAINLES', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'PN', 'name'=>'PINTU', 'created_at'=>$now, 'updated_at'=>$now],

        	['id'=>'SL', 'name'=>'SELOD', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'HN', 'name'=>'HANDEL', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'GR', 'name'=>'GRENDEL', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'LS', 'name'=>'PERALATAN LISTRIK', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'SE', 'name'=>'SELANG', 'created_at'=>$now, 'updated_at'=>$now],

        	['id'=>'OT', 'name'=>'OTHERS', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'PV', 'name'=>'PVC', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'TL', 'name'=>'TALANG', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'GT', 'name'=>'GENTENG', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'TP', 'name'=>'TERPAL', 'created_at'=>$now, 'updated_at'=>$now],

        	['id'=>'LA', 'name'=>'LAS', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'RO', 'name'=>'ROSTER & GLASSBLOCK', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'SM', 'name'=>'SEMEN & COMPOUND', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'LM', 'name'=>'LEM & AFDUNER', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'PK', 'name'=>'PAKU', 'created_at'=>$now, 'updated_at'=>$now],

        	['id'=>'SK', 'name'=>'SEKRUP', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'BT', 'name'=>'BAUTROOF', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'DN', 'name'=>'DINABOL', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'CT', 'name'=>'CAT', 'created_at'=>$now, 'updated_at'=>$now],
        	['id'=>'KM', 'name'=>'KUKU MACAN', 'created_at'=>$now, 'updated_at'=>$now],
        ]);
    }
}
