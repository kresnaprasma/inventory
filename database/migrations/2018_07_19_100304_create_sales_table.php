<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('sales_no')->unique();
            $table->string('sales_date');
            $table->string('sales_due_date');
            
            $table->string('customer_no');
            $table->foreign('customer_no')->references('customer_no')->on('customers')->onDelete('cascade');

            $table->string('user_id')->nullable();
            $table->foreign('user_id')->references('username')->on('users');

            $table->text('sales_note')->nullable();
            $table->boolean('draft')->default(true);
            $table->string('status');
            $table->double('sales_sub_total')->default(0);
            $table->double('sales_discount_percentage')->default(0);
            $table->double('sales_discount')->default(0);
            $table->double('sales_total')->default(0);
            
            $table->double('sales_tax')->default(0);
            $table->string('sales_tax_type')->nullable();
            $table->timestamps();
        });

        Schema::create('sales_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sales_no');
            $table->foreign('sales_no')->references('sales_no')->on('sales')->onDelete('cascade');

            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories');

            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->double('detail_amount')->default(0);
            $table->double('detail_disc')->default(0);
            $table->double('detail_total')->default(0);
            $table->datetime('detail_date');
            $table->string('detail_sku')->nullable();
            $table->timestamps();
        });

        Schema::create('sales_orders', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('sales_order_no')->unique();
            $table->datetime('sales_order_date')->nullable();
            $table->datetime('sales_order_due_date')->nullable();
            $table->string('sales_order_extno')->nullable();
            $table->string('sales_order_ref')->nullable();

            $table->string('sales_no')->nullable();
            $table->foreign('sales_no')->references('sales_no')->on('sales')->onDelete('cascade');

            $table->string('user_id')->nullable();
            $table->foreign('user_id')->references('username')->on('users');

            $table->text('sales_order_note')->nullable();
            $table->boolean('draft')->default(true);
            $table->string('status')->nullable();
            $table->double('sales_order_sub_total')->default(0);
            $table->double('sales_order_discount_percentage')->default(0);
            $table->double('sales_order_discount')->default(0);
            $table->double('sales_order_total')->default(0);
            
            $table->double('sales_order_tax')->default(0);
            $table->string('sales_order_tax_type')->nullable();

            $table->timestamps();
        });

        Schema::create('sales_order_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sales_order_no');
            $table->foreign('sales_order_no')->references('sales_order_no')->on('sales_orders')->onDelete('cascade');

            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories');

            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->double('detail_amount')->default(0);
            $table->double('detail_disc')->default(0);
            $table->double('detail_total')->default(0);
            $table->datetime('detail_date');
            $table->string('detail_sku')->nullable();
            $table->timestamps();
        });

        Schema::create('sales_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sales_no');
            $table->foreign('sales_no')->references('sales_no')->on('sales')->onDelete('cascade');
            
            $table->double('payment_amount');
            $table->datetime('payment_date');
            $table->string('payment_note');
            $table->enum('payment_type',['cash','bank']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
        Schema::dropIfExists('sales_details');
        Schema::dropIfExists('sales_orders');
        Schema::dropIfExists('sales_order_details');
        Schema::dropIfExists('sales_payment');
    }
}
