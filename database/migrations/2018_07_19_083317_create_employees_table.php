<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_positions', function(Blueprint $table){
            $table->string('id',32)->index();
            $table->primary('id');
            $table->string('position_no', 191)->unique();
            $table->string('name', 191);
            $table->timestamps();
        });

        Schema::create('employees', function (Blueprint $table) {
            $table->string('id',32)->index();
            $table->primary('id');
            $table->string('nik', 191)->unique();
            $table->string('name', 191);
            $table->string('alias', 191)->nullable();
            $table->string('ktp_no', 191)->nullable();
            $table->string('kk_no', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->text('address', 191)->nullable();
            $table->string('city', 191)->nullable();
            $table->string('kelurahan', 191)->nullable();
            $table->string('kecamatan', 191)->nullable();
            $table->string('province', 191)->nullable();
            $table->date('birthday', 191)->nullable();
            $table->string('birthplace', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->string('marrital', 191)->nullable();
            $table->enum('blood_type',['A', 'AB','B','O']);
            $table->string('zipcode', 191)->nullable();
            $table->enum('gender',['male', 'female']);

            $table->string('bank_account', 191)->nullable();
            $table->string('npwp', 191)->nullable();
            $table->string('bank_branch', 191)->nullable();
            $table->string('bank_name', 191)->nullable();
            
            $table->enum('job_status',['Active', 'Skorsing', 'Move', 'Retired', 'Fired', 'Training']);
            $table->date('job_start', 191)->nullable();
            $table->date('job_end', 191)->nullable();

            $table->string('bpjs_kesehatan_no', 191)->nullable();
            $table->string('bpjs_tenagakerja_no', 191)->nullable();

            $table->string('position_no', 191)->nullable();
            $table->foreign('position_no')->references('position_no')->on('employee_positions');

            $table->string('mother_name')->nullable();
            
            $table->boolean('is_user')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('employee_positions');
    }
}
