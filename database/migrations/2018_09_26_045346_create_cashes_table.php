<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('cashes', function (Blueprint $table) {
            $table->string('id');
            $table->double('cash_amount');
            $table->datetime('cash_date');
            $table->text('cash_note');
            $table->enum('cash_type', ['in', 'out']);

            $table->string('user_id');
            $table->foreign('user_id')->references('username')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_types');
        Schema::dropIfExists('cashes');
    }
}
