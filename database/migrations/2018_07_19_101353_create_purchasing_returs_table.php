<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasingRetursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasing_returs', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('retur_no')->unique();

            $table->string('purchasing_no');
            $table->foreign('purchasing_no')->references('purchasing_no')->on('purchasings')->onDelete('cascade');
            
            $table->datetime('retur_date')->nullable();
            $table->string('user_id')->nullable();
            $table->foreign('user_id')->references('username')->on('users');
            
            $table->text('retur_note')->nullable();
            $table->boolean('draft')->default(true);
            $table->timestamps();
        });

        Schema::create('purchasing_retur_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('retur_no');
            $table->foreign('retur_no')->references('retur_no')->on('purchasing_returs')->onDelete('cascade');
            
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories');
            
            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->datetime('detail_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasing_returs');
        Schema::dropIfExists('purchasing_retur_details');
    }
}
