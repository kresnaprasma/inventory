<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasings', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('purchasing_no')->unique();
            $table->datetime('purchasing_date')->nullable();
            $table->datetime('purchasing_due_date')->nullable();
            $table->string('purchasing_extno')->nullable();
            $table->string('purchasing_ref')->nullable();
            
            $table->string('supplier_no')->nullable();
            $table->foreign('supplier_no')->references('supplier_no')->on('suppliers');

            $table->string('user_id')->nullable();
            $table->foreign('user_id')->references('username')->on('users');

            $table->text('purchasing_note')->nullable();
            $table->boolean('draft')->default(true);
            $table->string('status');
            $table->double('purchasing_sub_total')->default(0);
            $table->double('purchasing_discount_percentage')->default(0);
            $table->double('purchasing_discount')->default(0);
            $table->double('purchasing_total')->default(0);
            
            $table->double('purchasing_tax')->default(0);
            $table->string('purchasing_tax_type')->nullable();

            $table->timestamps();
        });

        Schema::create('purchasing_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('purchasing_no');
            $table->foreign('purchasing_no')->references('purchasing_no')->on('purchasings')->onDelete('cascade');

            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories');

            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->double('retur_qty')->default(0);
            $table->string('retur_unit')->nullable();
            $table->double('detail_amount')->default(0);
            $table->double('detail_disc')->default(0);
            $table->double('detail_total')->default(0);
            $table->datetime('detail_date');
            $table->string('detail_sku');
            $table->timestamps();
        });

        Schema::create('purchasing_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('purchasing_no');
            $table->foreign('purchasing_no')->references('purchasing_no')->on('purchasings')->onDelete('cascade');
            
            $table->double('payment_amount');
            $table->datetime('payment_date');
            $table->string('payment_note');
            $table->enum('payment_type',['cash','bank']);
            $table->timestamps();
        });

        Schema::create('purchasing_orders', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('purchasing_order_no')->unique();
            $table->datetime('purchasing_order_date')->nullable();
            $table->datetime('purchasing_order_due_date')->nullable();
            $table->string('purchasing_order_extno')->nullable();
            $table->string('purchasing_order_ref')->nullable();
            
            $table->string('supplier_no')->nullable();

            $table->string('user_id')->nullable();
            $table->foreign('user_id')->references('username')->on('users');

            $table->text('purchasing_order_note')->nullable();
            $table->boolean('draft')->default(true);
            $table->string('status');
            $table->double('purchasing_order_sub_total')->default(0);
            $table->double('purchasing_order_discount_percentage')->default(0);
            $table->double('purchasing_order_discount')->default(0);
            $table->double('purchasing_order_total')->default(0);
            
            $table->double('purchasing_order_tax')->default(0);
            $table->string('purchasing_order_tax_type')->nullable();

            $table->timestamps();
        });

        Schema::create('purchasing_order_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('purchasing_order_no');
            $table->foreign('purchasing_order_no')->references('purchasing_order_no')->on('purchasing_orders')->onDelete('cascade');

            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories');

            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->double('detail_amount')->default(0);
            $table->double('detail_disc')->default(0);
            $table->double('detail_total')->default(0);
            $table->datetime('detail_date');
            $table->string('detail_sku');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasings');
        Schema::dropIfExists('purchasing_details');
        Schema::dropIfExists('purchasing_payments');
        Schema::dropIfExists('purchasing_orders');
        Schema::dropIfExists('purchasing_order_details');
    }
}
