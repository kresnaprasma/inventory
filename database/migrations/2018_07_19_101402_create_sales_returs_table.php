<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesRetursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_returs', function (Blueprint $table) {
            $table->string('id')->primary();

            $table->string('retur_no')->unique();
            
            $table->string('sales_no');
            $table->foreign('sales_no')->references('sales_no')->on('sales')->onDelete('cascade');
            
            $table->datetime('retur_date')->nullable();
            
            $table->string('user_id')->nullable();
            $table->foreign('user_id')->references('username')->on('users');
            
            $table->text('retur_note')->nullable();
            $table->boolean('draft')->default(true);
            $table->timestamps();
        });

        Schema::create('sales_retur_details', function (Blueprint $table) {
            $table->increments('id');

            $table->string('retur_no');
            $table->foreign('retur_no')->references('retur_no')->on('sales_returs')->onDelete('cascade');
            
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories');
            
            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->datetime('detail_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_returs');
        Schema::dropIfExists('sales_retur_details');
    }
}
