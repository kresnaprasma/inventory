<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('bank_code')->unique();
            $table->string('name', 50)->unique();
            $table->string('alias', 50)->nullable();
            $table->text('address')->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->timestamps();
        });

        Schema::create('suppliers', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('supplier_no', 50)->unique();
            $table->string('name', 50)->nullable();
            $table->string('email', 50)->unique();
            $table->text('address', 100)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('npwp', 50)->nullable();
            $table->string('pic_name', 100)->nullable();
            $table->string('pic_phone', 20)->nullable();
            $table->string('account_name', 50)->nullable();
            $table->string('account_no', 100)->nullable();

            $table->string('bank_id')->nullable();
            $table->foreign('bank_id')->references('id')->on('banks');
            
            $table->string('bank_branch', 100)->nullable();
            $table->string('user_id', 50)->nullable();
            $table->foreign('user_id')->references('username')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
        Schema::dropIfExists('suppliers');
    }
}
