<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_pictures', function (Blueprint $table) {
            $table->string('id',32)->index();
            $table->primary('id');
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories')->onDelete('cascade');
            $table->string('filename');
            $table->string('original_name');
            $table->string('filetype');
            $table->string('filesize');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_pictures');
    }
}
