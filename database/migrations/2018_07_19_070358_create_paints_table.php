<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colorants', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('colorant_code')->unique();
            $table->string('name')->nullable();
            $table->string('merk')->nullable();
            $table->string('unit_stock')->nullable();
            $table->float('capacity')->nullable();
            $table->float('min_capacity')->nullable();
            $table->float('max_capacity')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('paints', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('paint_code')->unique();
            $table->string('paint_name');
            $table->string('merk');
            $table->string('type');
            $table->string('base_paint');
            $table->float('capacity')->nullable(); 
            $table->string('stock_unit')->nullable();
            $table->string('barcode')->nullable();
            $table->string('paint_description')->nullable();
            $table->timestamps();
        });

        Schema::create('paint_details', function (Blueprint $table) {
            $table->increments('id'); 

            $table->string('paint_code');
            $table->foreign('paint_code')->references('paint_code')->on('paints')->onDelete('cascade');

            $table->string('colorant_code');
            $table->foreign('colorant_code')->references('colorant_code')->on('colorants');

            $table->double('detail_qty')->default(0);
            $table->string('detail_unit')->nullable();
            $table->double('detail_amount')->default(0);
            $table->double('detail_disc')->default(0);
            $table->double('detail_total')->default(0);
            $table->datetime('detail_date');
            $table->string('detail_sku')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colorants');
        Schema::dropIfExists('paints');
        Schema::dropIfExists('paint_details');
    }
}
