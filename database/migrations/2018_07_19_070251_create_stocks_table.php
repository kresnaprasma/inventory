<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('merks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('types', function (Blueprint $table) {
            $table->increments('id', 191);
            $table->string('alias', 191);
            $table->string('name', 191);

            $table->string('merk_id', 191);
            $table->foreign('merk_id')->references('alias')->on('merks');

            $table->timestamps();
        });

        Schema::create('inventory_categories', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('inventory_locations', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->timestamps();
        });
        
        Schema::create('inventories', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('inventory_code')->unique();

            $table->string('category_id');
            $table->foreign('category_id')->references('id')->on('inventory_categories');

            $table->string('merk');
            $table->string('type')->nullable();
            $table->string('serries')->nullable();

            $table->string('inventory_name');
            $table->string('stock_location')->nullable();
            $table->double('stock_minimum')->nullable();
            $table->double('stock_maximum')->nullable();
            $table->string('stock_unit')->nullable();
            $table->text('stock_description')->nullable();
            $table->string('barcode')->nullable();
            $table->timestamps();
        });

        Schema::create('inventory_converts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories')->onDelete('cascade')->onUpdate('cascade');
            $table->string('unit')->nullable();
            $table->double('size')->nullable();
            $table->enum('convert_type',['<','>']);
            $table->timestamps();
        });

        Schema::create('inventory_quantities', function(Blueprint $table){
            $table->increments('id');
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('qty_type', ['min', 'max']);
            $table->double('qty');
            $table->timestamps();
        });

        Schema::create('inventory_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories')->onDelete('cascade')->onUpdate('cascade');
            $table->double('price')->nullable();
            $table->enum('price_type', ['purchasing','selling_retail', 'selling_distribution']);
            $table->enum('price_unit', ['retail', 'pack']);
            $table->datetime('price_date')->nullable();
            $table->timestamps();
        });

        Schema::create('inventory_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inventory_code');
            $table->foreign('inventory_code')->references('inventory_code')->on('inventories')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('discount_type', ['purchasing','selling_retail', 'selling_distribution']);
            $table->double('discount')->nullable();
            $table->datetime('discount_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merks');
        Schema::dropIfExists('types');
        Schema::dropIfExists('inventories');
        Schema::dropIfExists('inventory_categories');
        Schema::dropIfExists('inventory_locations');
        Schema::dropIfExists('inventory_quantities');
        Schema::dropIfExists('inventory_prices');
        Schema::dropIfExists('inventory_discounts');
    }
}
