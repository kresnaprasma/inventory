<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// Route::middleware(['auth'])->group(function () {
//    Route::get('/', function () {
//    	return view('layouts.admin.admin');
//	 	});
// });

Route::middleware(['auth'])->group(function () {
	Route::get('/', 'DashboardController@index');
	// Route::get('/', function() {
	// 	return view('layouts.admin.admin');
	// });
});

Route::group([
	'prefix' =>'/master',
	'middleware'=>'auth'
], function(){

	Route::post('user/delete', ['as'=>'master.user.delete', 'uses'=>'UserController@delete']);
	Route::get('user/password/{id}',['as'=>'master.user.password.edit','uses'=>'UserController@editPassword']);
	Route::patch('user/password/{id}',['as'=>'master.user.password.update','uses'=>'UserController@updatePassword']);
	Route::resource('user', 'UserController',['as'=>'master']);

	Route::post('permission/delete', ['as'=>'master.permission.delete', 'uses'=>'PermissionController@delete']);
	Route::resource('permission', 'PermissionController',['as'=>'master']);

	Route::post('bank/delete', ['as'=>'master.bank.delete', 'uses'=>'BankController@delete']);
	Route::resource('bank', 'BankController',['as'=>'master']);

	Route::post('supplier/delete', ['as'=>'master.supplier.delete', 'uses'=>'SupplierController@delete']);
	Route::resource('supplier', 'SupplierController', ['as'=>'master']);

	Route::post('customer/delete', ['as'=>'master.customer.delete', 'uses'=>'CustomerController@delete']);
	Route::resource('customer', 'CustomerController', ['as'=>'master']);

	Route::post('employee/delete', ['as'=>'master.employee.delete', 'uses'=>'EmployeeController@delete']);
	Route::resource('employee', 'EmployeeController', ['as'=>'master']);
});

Route::group([
	'middleware' => 'auth'
], function() {
	Route::resource('colorant', 'ColorantController', ['as'=>'inventory']);
	Route::resource('paint', 'PaintController', ['as'=>'inventory']);

	Route::resource('inventory', 'InventoryController');
	Route::resource('inventorycategory', 'InventoryCategoryController', ['as'=>'master']);

	Route::post('merk/delete', ['as'=>'master.merk.delete', 'uses'=>'MerkController@delete']);
	Route::resource('merk', 'MerkController', ['as'=>'master']);

	Route::post('type/delete', ['as'=>'master.type.delete', 'uses'=>'TypeController@delete']);
	Route::resource('type', 'TypeController', ['as'=>'master']);
});



Route::group([
	'middleware'=>'auth',
], function(){
	Route::resource('cash', 'CashController');
});


// Purchasing
Route::resource('purchasing/order','PurchasingOrderController')->names('purchasing.order');
Route::get('purchasing/print/{id}', ['as'=>'purchasing.print', 'uses'=>'PurchasingController@printPDF']);
Route::resource('purchasing', 'PurchasingController');
Route::group([
	'prefix'=>'purchasing',
	// 'middleware'=>'auth',
], function(){
	Route::get('/payment/{id}', ['as'=>'purchasing.payment.create', 'uses'=>'PurchasingPaymentController@create']);
	Route::post('/payment', ['as'=>'purchasing.payment.store', 'uses'=>'PurchasingPaymentController@store']);
	Route::get('/retur/{id}',['as'=>'purchasing.retur.create','uses'=>'PurchasingReturController@create']);
	Route::post('/retur',['as'=>'purchasing.retur.store','uses'=>'PurchasingController@storeRetur']);
});


//Sales
Route::group([
	'prefix'=>'sales',
	// 'middleware'=>'auth',
], function(){
	Route::get('/send/{id}', ['as'=>'sales.send.create', 'uses'=>'SalesController@salesSend']);

	Route::get('/payment/{id}', ['as'=>'sales.payment.create', 'uses'=>'SalesPaymentController@create']);
	Route::post('/payment', ['as'=>'sales.payment.store', 'uses'=>'SalesPaymentController@store']);

	Route::resource('order', 'SalesOrderController',['as'=>'sales']);
});
Route::get('sales/print/{id}', ['as'=>'sales.print', 'uses'=>'SalesController@printPDF']);
Route::resource('sales', 'SalesController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
