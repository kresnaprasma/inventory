<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
	'prefix'=>'web',
	// 'middleware'=>'auth',
], function(){

	Route::post('inventory/category', ['as'=>'api.web.inventory.category.store','uses'=>'Api\InventoryCategoryController@store']);
	Route::get('inventory/no/{id}', ['as'=>'api.web.inventory.get.no','uses'=>'Api\InventoryController@inventoryNo']);

	Route::get('inventory/type/{id}', ['as'=>'type.selector', 'uses'=>'Api\InventoryController@selector']);
	Route::get('inventory/type/edit/{id}', ['as'=>'type.selectoredit', 'uses'=>'Api\InventoryController@selectoredit']);
	// Convert
	Route::get('inventory/convert/{inventory}', ['as'=>'api.web.inventory.convert.get','uses'=>'Api\InventoryController@getConverter']);
	Route::post('inventory/convert', ['as'=>'api.web.inventory.convert.store','uses'=>'Api\InventoryController@storeConverter']);

	// Price
	Route::get('inventory/price/{inventory}', ['as'=>'api.web.inventory.price.get','uses'=>'Api\InventoryController@getPrice']);
	Route::post('inventory/price', ['as'=>'api.web.inventory.price.store','uses'=>'Api\InventoryController@storePrice']);

	// Discount
	Route::get('inventory/discount/{inventory}', ['as'=>'api.web.inventory.discount.get','uses'=>'Api\InventoryController@getDiscount']);
	Route::post('inventory/discount', ['as'=>'api.web.inventory.discount.store','uses'=>'Api\InventoryController@storeDiscount']);

	//Quantity
	Route::get('inventory/quantity/{inventory}', ['as'=>'api.web.inventory.quantity.get','uses'=>'Api\InventoryController@getQuantity']);
	Route::post('inventory/quantity', ['as'=>'api.web.inventory.quantity.store','uses'=>'Api\InventoryController@storeQuantity']);

	Route::get('inventory/chart/purchasing', ['as'=>'api.web.inventory.chart.purchasing','uses'=>'Api\InventoryController@chartPurchasingStock']);

	// CONVERT PRICE
	Route::get('inventory/convert-price/{inventory}',['as'=>'api.web.inventory.convert.price.get','uses'=>'Api\InventoryController@getConvertPrice']);
	
	Route::resource('inventory', 'Api\InventoryController',['as'=>'api.web']);

	Route::get('paint/type/{id}', ['as'=>'paint.type.selector', 'uses'=>'Api\PaintController@selector']);
	Route::get('paint/type/edit/{id}', ['as'=>'paint.type.selectoredit', 'uses'=>'Api\PaintController@selectoredit']);
});